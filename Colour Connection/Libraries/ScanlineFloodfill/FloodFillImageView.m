//
//  FloodFillImageView.m
//  ImageFloodFilleDemo
//
//  Created by chintan on 11/07/13.
//  Copyright (c) 2013 ZWT. All rights reserved.
//https://github.com/Chintan-Dave/UIImageScanlineFloodfill
//
#import "FloodFillImageView.h"
#import "CCUndoManager.h"
#import "ImageManager.h"
#import <AudioToolbox/AudioToolbox.h>

@interface FloodFillImageView()

@property (strong, nonatomic) NSMutableArray<UIImage*> *history;

@end

@implementation FloodFillImageView
//@dynamic originalImage;

@synthesize tolorance,newcolor, isColourPickingOn, isActive;


//- (UIImage *) originalImage {
//    return objc_getAssociatedObject(self, @selector(originalImage));
//}
//
//-(void) setOriginalImage:(UIImage *)originalImage{
//    objc_setAssociatedObject(self, @selector(originalImage), originalImage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.history = [NSMutableArray new];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.history = [NSMutableArray new];
    }
    return self;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isActive){
        return;
    }
    
    isActive = TRUE;
    
    //Get touch Point
    CGPoint tpoint = [[[event allTouches] anyObject] locationInView:self];
    
    CGFloat imageScale = self.image.size.width / self.bounds.size.width ;
    
    //Convert Touch Point to pixel of Image
    //This code will be according to your need
    tpoint.x = (tpoint.x * imageScale); // x 2
    tpoint.y = (tpoint.y * imageScale);
    
    //Call function to flood fill and get new image with filled color
    
    if ([self isColourPickingOn] == NO) {
        if ([[ImageManager sharedManager] canDrawAtPoint:tpoint]) {
            
            UIColor *colour = [self.image colorAtPoint:tpoint];
            
            if (![colour isEqual:newcolor]) {
                if (self.history.count < 10) {
                    [self.history addObject:self.image];
                }
                else{
                    [self.history removeObjectAtIndex:0];
                    [self.history addObject:self.image];
                }
                
                
                
                UIImage *image1 = [self.image floodFillFromPoint:tpoint withColor:newcolor andTolerance:tolorance];
                [self setImage:image1];
                isActive = FALSE;
            } else {
                isActive = FALSE;
                //NSLog(@"Same colour detected.");
            }
        } else {
            isActive = FALSE;
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }
    } else {
        UIColor *colour = [self.image colorAtPoint:tpoint];
        isActive = FALSE;
        self.completion(colour);
    }
}

- (void)undoAction {
    if (!self.history.count) return;

    self.image = self.history.lastObject;
    [self.history removeLastObject];
//    if([[CCUndoManager sharedManager] hasActions] == NO){
//        return;
//    }
//
//    dispatch_async(dispatch_get_main_queue(), ^(void)
//                   {
//                       UIImage *image1 = [self.image ccUndo];
//                       [self setImage:image1];
//                   });
//
}

@end
