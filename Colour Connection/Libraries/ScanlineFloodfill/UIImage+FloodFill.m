//
//  UIImage+FloodFill.m
//  ImageFloodFilleDemo
//
//  Created by chintan on 15/07/13.
//  Copyright (c) 2013 ZWT. All rights reserved.
//https://github.com/Chintan-Dave/UIImageScanlineFloodfill

#import "UIImage+FloodFill.h"
#import <UIKit/UIKit.h>
#import "CCUndoManager.h"
#import "ImageManager.h"

#define DEBUG_ANTIALIASING 0
#define ORIGINAL_DATA ((unsigned char *)[[ImageManager sharedManager] imageData].bytes)
#define IS_SAME_COLOR (0xffffffff == originalColor)
//compareColor(ocolor, color, tolerance)

@implementation UIImage (FloodFill)

/*
 startPoint : Point from where you want to color. Generaly this is touch point.
 This is important because color at start point will be replaced with other.
 
 newColor   : This color will be apply at point where the match on startPoint color found.
 
 tolerance  : If Tolerance is 0 than it will search for exact match of color 
 other wise it will take range according to tolerance value.
 
 If You dont want to use tolerance and want to incress performance Than you can change
 compareColor(ocolor, color, tolerance) with just ocolor==color which reduse function call.
 */


- (UIImage *) floodFillFromPoint:(CGPoint)startPoint withColor:(UIColor *)newColor andTolerance:(int)tolerance
{
    return [self floodFillFromPoint:startPoint withColor:newColor andTolerance:tolerance useAntiAlias:NO];
}


-(UIColor *) colorAtPoint:(CGPoint) startPoint{
    @autoreleasepool{
        
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        
        CGImageRef imageRef = [self CGImage];
        
        NSUInteger width = CGImageGetWidth(imageRef);
        NSUInteger height = CGImageGetHeight(imageRef);
        
        ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
        
        CGFloat ratio = width / height;
        startPoint = CGPointMake(startPoint.x * ratio, startPoint.y * ratio);
        
        ////NSLog(@"Start point: %@", NSStringFromCGPoint(startPoint));
        
        //        ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
        
        unsigned char *imageData = malloc(height * width * 4);
        //        unsigned char *imageData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
        
        NSUInteger bytesPerPixel = 4;       //CGImageGetBitsPerPixel(imageRef) / 8;
        NSUInteger bytesPerRow = width * 4;      //CGImageGetBytesPerRow(imageRef);
        NSUInteger bitsPerComponent = 8;    //CGImageGetBitsPerComponent(imageRef);
        
        ////NSLog(@"bytesPerPixel: %lu", (unsigned long)bytesPerPixel);
        ////NSLog(@"bytesPerRow: %lu", (unsigned long)bytesPerRow);
        ////NSLog(@"bitsPerComponent: %lu", (unsigned long)bitsPerComponent);
        
        CGBitmapInfo bitmapInfo = bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;    //CGImageGetBitmapInfo(imageRef);
        
        
        
        if (kCGImageAlphaLast == (uint32_t)bitmapInfo || kCGImageAlphaFirst == (uint32_t)bitmapInfo) {
            bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;
        }
        
        //        CGContextRef context = CGBitmapContextCreate (imageData, width, height, 8, width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
        
        CGContextRef context = CGBitmapContextCreate(imageData,
                                                     width,
                                                     height,
                                                     bitsPerComponent,
                                                     bytesPerRow,
                                                     colorSpace,
                                                     bitmapInfo);
        CGColorSpaceRelease(colorSpace);
        
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
        
        
        //Get color at start point
        unsigned int byteIndex = (bytesPerRow * roundf(startPoint.y)) + roundf(startPoint.x) * bytesPerPixel;
        unsigned int ocolor = getColorCode(byteIndex, imageData);
        
        int redX   = ((0xff000000 & ocolor) >> 24);
        int greenX = ((0x00ff0000 & ocolor) >> 16);
        int blueX = ((0x0000ff00 & ocolor) >> 8);
        int alphaX =  (0x000000ff & ocolor);
        
        UIColor *colour = [UIColor colorWithRed:redX/255.0 green:greenX/255.0 blue:blueX/255.0 alpha:alphaX/255.0];
        
        CGContextRelease(context);
        
        free(imageData);
        
        return  colour;
    }}

- (UIImage *) floodFillFromPoint:(CGPoint)startPoint withColor:(UIColor *)newColor andTolerance:(int)tolerance useAntiAlias:(BOOL)antiAlias
{
    @autoreleasepool{
        @try
        {
            /*
             First We create rowData from UIImage.
             We require this conversation so that we can use detail at pixel like color at pixel.
             You can get some discussion about this topic here:
             http://stackoverflow.com/questions/448125/how-to-get-pixel-data-from-a-uiimage-cocoa-touch-or-cgimage-core-graphics
             */
            
            
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            
            CGImageRef imageRef = [self CGImage];
            
            NSUInteger width = CGImageGetWidth(imageRef);
            NSUInteger height = CGImageGetHeight(imageRef);
            
            ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
            
            CGFloat ratio = width / height;
            startPoint = CGPointMake(startPoint.x * ratio, startPoint.y * ratio);
            
            unsigned char *imageData = malloc(height * width * 4);
            
            NSUInteger bytesPerPixel = 4;
            NSUInteger bytesPerRow = width * 4;
            NSUInteger bitsPerComponent = 8;
            
            CGBitmapInfo bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;
            
            
            if (kCGImageAlphaLast == (uint32_t)bitmapInfo || kCGImageAlphaFirst == (uint32_t)bitmapInfo) {
                bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;
            }
            
            CGContextRef context = CGBitmapContextCreate(imageData,
                                                         width,
                                                         height,
                                                         bitsPerComponent,
                                                         bytesPerRow,
                                                         colorSpace,
                                                         bitmapInfo);
            CGColorSpaceRelease(colorSpace);
            
            CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);

            //Get color at start point
            unsigned int byteIndex = (bytesPerRow * roundf(startPoint.y)) + roundf(startPoint.x) * bytesPerPixel;
            
            
            
            unsigned int ocolor = getColorCode(byteIndex, imageData);
            
            if (compareColor(ocolor, 0, 0)) {
                
                CGContextRelease(context);
                free(imageData);
                
                return nil;
            }
            
            int redX   = ((0xff000000 & ocolor) >> 24);
            int greenX = ((0x00ff0000 & ocolor) >> 16);
            int blueX = ((0x0000ff00 & ocolor) >> 8);
            int alphaX =  (0x000000ff & ocolor);
            
            UIColor *prevColour = [UIColor colorWithRed:redX/255.0 green:greenX/255.0 blue:blueX/255.0 alpha:alphaX/255.0];
            
            [[CCUndoManager sharedManager] recordAction:prevColour withPoint:startPoint];
            
            
            //Convert newColor to RGBA value so we can save it to image.
            int newRed, newGreen, newBlue, newAlpha;
            
            const CGFloat *components = CGColorGetComponents(newColor.CGColor);
            
            /*
             If you are not getting why I use CGColorGetNumberOfComponents than read following link:
             http://stackoverflow.com/questions/9238743/is-there-an-issue-with-cgcolorgetcomponents
             */
            
            if(CGColorGetNumberOfComponents(newColor.CGColor) == 2)
            {
                newRed   = newGreen = newBlue = components[0] * 255;
                newAlpha = components[1] * 255;
            }
            else if (CGColorGetNumberOfComponents(newColor.CGColor) == 4)
            {
                if ((bitmapInfo&kCGBitmapByteOrderMask) == kCGBitmapByteOrder32Little)
                {
                    newRed   = components[2] * 255;
                    newGreen = components[1] * 255;
                    newBlue  = components[0] * 255;
                    newAlpha = 255;
                }
                else
                {
                    newRed   = components[0] * 255;
                    newGreen = components[1] * 255;
                    newBlue  = components[2] * 255;
                    newAlpha = 255;
                }
            }
            
            unsigned int ncolor = (newRed << 24) | (newGreen << 16) | (newBlue << 8) | newAlpha;
            
            /*
             We are using stack to store point.
             Stack is implemented by LinkList.
             To incress speed I have used NSMutableData insted of NSMutableArray.
             To see Detail Of This implementation visit following leink.
             http://iwantmyreal.name/blog/2012/09/29/a-faster-array-in-objective-c/
             */
            
            
            
            LinkedListStack *points = [[LinkedListStack alloc] initWithCapacity:500 incrementSize:500 andMultiplier:height];
            LinkedListStack *antiAliasingPoints = [[LinkedListStack alloc] initWithCapacity:500 incrementSize:500 andMultiplier:height];
            
            int x = roundf(startPoint.x);
            int y = roundf(startPoint.y);
            
            //add starting point where user touch in stack
            [points pushFrontX:x andY:y];
            
            /*
             This algorithem is prety simple though it llook odd in Objective C syntex.
             To get familer with this algorithm visit following link.
             http://lodev.org/cgtutor/floodfill.html
             You can read hole artical for knowledge.
             If you are familer with flood fill than got to Scanline Floodfill Algorithm With Stack (floodFillScanlineStack)
             */
            
            unsigned int color;
            unsigned int originalColor;
            BOOL spanLeft,spanRight;
            BOOL colorFilled = false;
            
            // LOOP 1
            
            /*
             Run Loop Till we have pixel in stack
             - it means stack has no more point and area is filled with newColor
             */
            
            while ([points popFront:&x andY:&y] != INVALID_NODE_CONTENT) {
                /*
                 Get value from Stack.
                 - If you have look care fully we have already done this in while condition.  [points popFront:&x andY:&y] will take point from stack and store it in x and y.
                 */

                /*
                 Find highest value of Y with old color (We refer old color to color which is at starting point).
                 - Here highest word may miss lead. here highest means upper side of image. so we can say “first pixel in column x with old color”. every time we start from upper side and go (or come) down. To do this we use following code.
                 */
                
                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                
                color = getColorCode(byteIndex, imageData);
                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                
                //COMPARE 1
                
                while(y >= 0 && IS_SAME_COLOR) {
                    y--;
                    if(y >= 0) {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    }
                }
                
                // Add the top most point on the antialiasing list
                if(y >= 0 && !IS_SAME_COLOR) {
                    [antiAliasingPoints pushFrontX:x andY:y];
                    }
                y++;
                
                spanLeft = spanRight = NO;
                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                color = getColorCode(byteIndex, imageData);
                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                
                // LOOP 2
                
                /*
                 Run Loop Till Y reach to end of image or current pixel color is not same as old color.
                 - so we start from up side so we will stop when we reach till down. Following while condition willl do it.
                 */
                
                while (y < height && IS_SAME_COLOR && ncolor != color) {
                    
                    /*
                     We have three condition here First will stop if we reach at last pixel of image.
                     second will stop if we reach in color area where we don’t need to feel color (color other than old color). 
                     and third will stop if the color is alredy replaced with newColor.
                     */
                    
                    //Replace Old Color with new color
                    colorFilled = true;
                    imageData[byteIndex + 0] = newRed;
                    imageData[byteIndex + 1] = newGreen;
                    imageData[byteIndex + 2] = newBlue;
                    imageData[byteIndex + 3] = newAlpha;
                    
                    /*
                     A. Check color of pixel nere current (x+1 & x-1)
                     B. Add value to stack if require.
                     C. Increase Y by 1
                     - I have merged this three step because it is very tide with each other. Which pixel will color is depend on what we add in stack so now  we will see how to add pixel in code.
                     */
                    if(x > 0) {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x - 1) * bytesPerPixel;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                        
                        if(!spanLeft && x > 0 && IS_SAME_COLOR) {
                            [points pushFrontX:(x - 1) andY:y];
                            spanLeft = YES;
                        } else if(spanLeft && x > 0 && !IS_SAME_COLOR) {
                            spanLeft = NO;
                        }
                        
                        // we can't go left. Add the point on the antialiasing list
                        if(!spanLeft && x > 0 && !IS_SAME_COLOR && !compareColor(ncolor, color, tolerance)) {
                            [antiAliasingPoints pushFrontX:(x - 1) andY:y];
                        }
                    }
                    
                    if(x < width - 1) {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x + 1) * bytesPerPixel;;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                        
                        if(!spanRight && IS_SAME_COLOR) {
                            [points pushFrontX:(x + 1) andY:y];
                            spanRight = YES;
                        } else if(spanRight && !IS_SAME_COLOR) {
                            spanRight = NO;
                        }
                        
                        // we can't go right. Add the point on the antialiasing list
                        if(!spanRight && !IS_SAME_COLOR && !compareColor(ncolor, color, tolerance)) {
                            [antiAliasingPoints pushFrontX:(x + 1) andY:y];
                        }
                    }
                    
                    y++;
                    
                    if(y < height) {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    }
                    
                    /*
                     Ok I agree that it is confusing but if you understand algorithm well and when u see this code in xcode instead of here than it will make more sense.
                     */
                }
                
                if (y <height) {
                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    // Add the bottom point on the antialiasing list
                    if (!IS_SAME_COLOR)
                        [antiAliasingPoints pushFrontX:x andY:y];
                }
            }
            
            // For each point marked
            // perform antialiasing on the same pixel, plus the top,left,bottom and right pixel
            unsigned int antialiasColor = getColorCodeFromUIColor(newColor,bitmapInfo&kCGBitmapByteOrderMask );
            int red1   = ((0xff000000 & antialiasColor) >> 24);
            int green1 = ((0x00ff0000 & antialiasColor) >> 16);
            int blue1  = ((0x0000ff00 & antialiasColor) >> 8);
            int alpha1 =  (0x000000ff & antialiasColor);
            
            // LOOP 3
            if (!colorFilled)
                return self;
            
            while ([antiAliasingPoints popFront:&x andY:&y] != INVALID_NODE_CONTENT)
            {
                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                color = getColorCode(byteIndex, imageData);
                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                
                BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                
                if (!compareColor(ncolor, color, 0) && validLength)
                {
                    color = 0x000000ff;
                    
                    int red2   = ((0xff000000 & color) >> 24);
                    int green2 = ((0x00ff0000 & color) >> 16);
                    int blue2 = ((0x0000ff00 & color) >> 8);
                    int alpha2 =  (0x000000ff & color);
                    
                    if (antiAlias) {
                        imageData[byteIndex + 0] = (red1 + red2) / 2;
                        imageData[byteIndex + 1] = (green1 + green2) / 2;
                        imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                        imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                    } else {
                        imageData[byteIndex + 0] = red2;
                        imageData[byteIndex + 1] = green2;
                        imageData[byteIndex + 2] = blue2;
                        imageData[byteIndex + 3] = alpha2;
                    }
                    
#if DEBUG_ANTIALIASING
                    imageData[byteIndex + 0] = 0;
                    imageData[byteIndex + 1] = 0;
                    imageData[byteIndex + 2] = 255;
                    imageData[byteIndex + 3] = 255;
#endif
                }
                
                // left
                if (x>0)
                {
                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x - 1) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            imageData[byteIndex + 0] = (red1 + red2) / 2;
                            imageData[byteIndex + 1] = (green1 + green2) / 2;
                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                }
                if (x<width)
                {
                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x + 1) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            imageData[byteIndex + 0] = (red1 + red2) / 2;
                            imageData[byteIndex + 1] = (green1 + green2) / 2;
                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                    
                }
                
                if (y>0)
                {
                    byteIndex = (bytesPerRow * roundf(y - 1)) + roundf(x) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            imageData[byteIndex + 0] = (red1 + red2) / 2;
                            imageData[byteIndex + 1] = (green1 + green2) / 2;
                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                }
                
                if (y<height)
                {
                    byteIndex = (bytesPerRow * roundf(y + 1)) + roundf(x) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    //App crached becaused original data was empty
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            
                            imageData[byteIndex + 0] = (red1 + red2) / 2;
                            imageData[byteIndex + 1] = (green1 + green2) / 2;
                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                            
                            
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
ю                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                    
                }
            }
            
            /*
             
             Convert bitmap image back to UIImage
             
             - This is pretty simple one we have filled image in context so now we make UIImage from it.
             
             */
            
            points = nil;
            antiAliasingPoints = nil;
            
            
            CGImageRef newCGImage = CGBitmapContextCreateImage(context);
            
            UIImage *result = [UIImage imageWithCGImage:newCGImage scale:[self scale] orientation:UIImageOrientationUp];
            
            CGImageRelease(newCGImage);
            
            CGContextRelease(context);
            
            free(imageData);
            
            return result;
        }
        @catch (NSException *exception)
        {
            ////NSLog(@"Exception : %@", exception);
        }
        
    }
}


//- (UIImage *) ccUndo
//{
//    @autoreleasepool{
//        
//        NSDictionary *dict = [[CCUndoManager sharedManager] performUndo];
//        
//        CGPoint startPoint = [[dict objectForKey:@"point"] CGPointValue];
//        UIColor *newColor = [dict objectForKey:@"colour"];
//        int tolerance = 0;
//        BOOL antiAlias = YES;
//        
//        @try
//        {
//            /*
//             First We create rowData from UIImage.
//             We require this conversation so that we can use detail at pixel like color at pixel.
//             You can get some discussion about this topic here:
//             http://stackoverflow.com/questions/448125/how-to-get-pixel-data-from-a-uiimage-cocoa-touch-or-cgimage-core-graphics
//             */
//            
//            
//            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//            
//            CGImageRef imageRef = [self CGImage];
//            
//            NSUInteger width = CGImageGetWidth(imageRef);
//            NSUInteger height = CGImageGetHeight(imageRef);
//            
//            ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
//            
//            CGFloat ratio = width / height;
//            startPoint = CGPointMake(startPoint.x * ratio, startPoint.y * ratio);
//            
//            ////NSLog(@"Start point: %@", NSStringFromCGPoint(startPoint));
//            
//            //        ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
//            
//            unsigned char *imageData = malloc(height * width * 4);
//            //        unsigned char *imageData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
//            
//            NSUInteger bytesPerPixel = 4;       //CGImageGetBitsPerPixel(imageRef) / 8;
//            NSUInteger bytesPerRow = width * 4;      //CGImageGetBytesPerRow(imageRef);
//            NSUInteger bitsPerComponent = 8;    //CGImageGetBitsPerComponent(imageRef);
//            
//            ////NSLog(@"bytesPerPixel: %lu", (unsigned long)bytesPerPixel);
//            ////NSLog(@"bytesPerRow: %lu", (unsigned long)bytesPerRow);
//            ////NSLog(@"bitsPerComponent: %lu", (unsigned long)bitsPerComponent);
//            
//            CGBitmapInfo bitmapInfo = bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;    //CGImageGetBitmapInfo(imageRef);
//            
//            
//            
//            if (kCGImageAlphaLast == (uint32_t)bitmapInfo || kCGImageAlphaFirst == (uint32_t)bitmapInfo) {
//                bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;
//            }
//            
//            //        CGContextRef context = CGBitmapContextCreate (imageData, width, height, 8, width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
//            
//            CGContextRef context = CGBitmapContextCreate(imageData,
//                                                         width,
//                                                         height,
//                                                         bitsPerComponent,
//                                                         bytesPerRow,
//                                                         colorSpace,
//                                                         bitmapInfo);
//            CGColorSpaceRelease(colorSpace);
//            
//            CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
//            
//            
//            
//            //        unsigned long length = strlen(imageData);
//            
//            //Get color at start point
//            unsigned int byteIndex = (bytesPerRow * roundf(startPoint.y)) + roundf(startPoint.x) * bytesPerPixel;
//            
//            //        if (length < 4) {
//            //            return nil;
//            //        }
//            
//            unsigned int ocolor = getColorCode(byteIndex, imageData);
//            
//            if (compareColor(ocolor, 0, 0)) {
//                
//                CGContextRelease(context);
//                free(imageData);
//                
//                return nil;
//            }
//            
//            int redX   = ((0xff000000 & ocolor) >> 24);
//            int greenX = ((0x00ff0000 & ocolor) >> 16);
//            int blueX = ((0x0000ff00 & ocolor) >> 8);
//            int alphaX =  (0x000000ff & ocolor);
//            
//            //        UIColor *prevColour = [UIColor colorWithRed:redX/255.0 green:greenX/255.0 blue:blueX/255.0 alpha:alphaX/255.0];
//            //        [[CCUndoManager sharedManager] recordAction:prevColour withPoint:startPoint];
//            
//            
//            //Convert newColor to RGBA value so we can save it to image.
//            int newRed, newGreen, newBlue, newAlpha;
//            
//            const CGFloat *components = CGColorGetComponents(newColor.CGColor);
//            
//            /*
//             If you are not getting why I use CGColorGetNumberOfComponents than read following link:
//             http://stackoverflow.com/questions/9238743/is-there-an-issue-with-cgcolorgetcomponents
//             */
//            
//            if(CGColorGetNumberOfComponents(newColor.CGColor) == 2)
//            {
//                newRed   = newGreen = newBlue = components[0] * 255;
//                newAlpha = components[1] * 255;
//            }
//            else if (CGColorGetNumberOfComponents(newColor.CGColor) == 4)
//            {
//                if ((bitmapInfo&kCGBitmapByteOrderMask) == kCGBitmapByteOrder32Little)
//                {
//                    newRed   = components[2] * 255;
//                    newGreen = components[1] * 255;
//                    newBlue  = components[0] * 255;
//                    newAlpha = 255;
//                }
//                else
//                {
//                    newRed   = components[0] * 255;
//                    newGreen = components[1] * 255;
//                    newBlue  = components[2] * 255;
//                    newAlpha = 255;
//                }
//            }
//            
//            unsigned int ncolor = (newRed << 24) | (newGreen << 16) | (newBlue << 8) | newAlpha;
//            
//            /*
//             We are using stack to store point.
//             Stack is implemented by LinkList.
//             To incress speed I have used NSMutableData insted of NSMutableArray.
//             To see Detail Of This implementation visit following leink.
//             http://iwantmyreal.name/blog/2012/09/29/a-faster-array-in-objective-c/
//             */
//            
//            
//            
//            LinkedListStack *points = [[LinkedListStack alloc] initWithCapacity:500 incrementSize:500 andMultiplier:height];
//            LinkedListStack *antiAliasingPoints = [[LinkedListStack alloc] initWithCapacity:500 incrementSize:500 andMultiplier:height];
//            
//            int x = roundf(startPoint.x);
//            int y = roundf(startPoint.y);
//            
//            //add starting point where user touch in stack
//            [points pushFrontX:x andY:y];
//            
//            /*
//             This algorithem is prety simple though it llook odd in Objective C syntex.
//             To get familer with this algorithm visit following link.
//             http://lodev.org/cgtutor/floodfill.html
//             You can read hole artical for knowledge.
//             If you are familer with flood fill than got to Scanline Floodfill Algorithm With Stack (floodFillScanlineStack)
//             */
//            
//            unsigned int color;
//            unsigned int originalColor;
//            BOOL spanLeft,spanRight;
//            
//            // LOOP 1
//            
//            /*
//             Run Loop Till we have pixel in stack
//             - it means stack has no more point and area is filled with newColor
//             */
//            
//            while ([points popFront:&x andY:&y] != INVALID_NODE_CONTENT)
//            {
//                
//                /*
//                 Get value from Stack.
//                 - If you have look care fully we have already done this in while condition.  [points popFront:&x andY:&y] will take point from stack and store it in x and y.
//                 */
//                
//                
//                /*
//                 Find highest value of Y with old color (We refer old color to color which is at starting point).
//                 - Here highest word may miss lead. here highest means upper side of image. so we can say “first pixel in column x with old color”. every time we start from upper side and go (or come) down. To do this we use following code.
//                 */
//                
//                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
//                
//                color = getColorCode(byteIndex, imageData);
//                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                
//                //COMPARE 1
//                
//                while(y >= 0 && IS_SAME_COLOR)
//                {
//                    y--;
//                    
//                    if(y >= 0)
//                    {
//                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
//                        
//                        color = getColorCode(byteIndex, imageData);
//                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                        
//                    }
//                }
//                
//                // Add the top most point on the antialiasing list
//                if(y >= 0 && !IS_SAME_COLOR)
//                    if(y >= 0 && !IS_SAME_COLOR)
//                    {
//                        [antiAliasingPoints pushFrontX:x andY:y];
//                    }
//                
//                y++;
//                
//                spanLeft = spanRight = NO;
//                
//                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
//                
//                color = getColorCode(byteIndex, imageData);
//                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                
//                // LOOP 2
//                
//                /*
//                 Run Loop Till Y reach to end of image or current pixel color is not same as old color.
//                 - so we start from up side so we will stop when we reach till down. Following while condition willl do it.
//                 */
//                
//                while (y < height && IS_SAME_COLOR && ncolor != color)
//                {
//                    
//                    /*
//                     
//                     We have three condition here First will stop if we reach at last pixel of image.
//                     second will stop if we reach in color area where we don’t need to feel color (color other than old color).
//                     and third will stop if the color is alredy replaced with newColor.
//                     
//                     */
//                    
//                    
//                    //Replace Old Color with new color
//                    
//                    imageData[byteIndex + 0] = newRed;
//                    imageData[byteIndex + 1] = newGreen;
//                    imageData[byteIndex + 2] = newBlue;
//                    imageData[byteIndex + 3] = newAlpha;
//                    
//                    /*
//                     
//                     A. Check color of pixel nere current (x+1 & x-1)
//                     
//                     B. Add value to stack if require.
//                     
//                     C. Increase Y by 1
//                     
//                     - I have merged this three step because it is very tide with each other. Which pixel will color is depend on what we add in stack so now  we will see how to add pixel in code.
//                     
//                     */
//                    
//                    if(x > 0)
//                    {
//                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x - 1) * bytesPerPixel;
//                        
//                        color = getColorCode(byteIndex, imageData);
//                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                        
//                        if(!spanLeft && x > 0 && IS_SAME_COLOR)
//                        {
//                            [points pushFrontX:(x - 1) andY:y];
//                            
//                            spanLeft = YES;
//                        }
//                        else if(spanLeft && x > 0 && !IS_SAME_COLOR)
//                        {
//                            spanLeft = NO;
//                        }
//                        
//                        // we can't go left. Add the point on the antialiasing list
//                        if(!spanLeft && x > 0 && !IS_SAME_COLOR && !compareColor(ncolor, color, tolerance))
//                        {
//                            [antiAliasingPoints pushFrontX:(x - 1) andY:y];
//                        }
//                    }
//                    
//                    if(x < width - 1)
//                    {
//                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x + 1) * bytesPerPixel;;
//                        
//                        color = getColorCode(byteIndex, imageData);
//                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                        
//                        if(!spanRight && IS_SAME_COLOR)
//                        {
//                            [points pushFrontX:(x + 1) andY:y];
//                            
//                            spanRight = YES;
//                        }
//                        else if(spanRight && !IS_SAME_COLOR)
//                        {
//                            spanRight = NO;
//                        }
//                        
//                        // we can't go right. Add the point on the antialiasing list
//                        if(!spanRight && !IS_SAME_COLOR && !compareColor(ncolor, color, tolerance))
//                        {
//                            [antiAliasingPoints pushFrontX:(x + 1) andY:y];
//                        }
//                    }
//                    
//                    y++;
//                    
//                    if(y < height)
//                    {
//                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
//                        
//                        color = getColorCode(byteIndex, imageData);
//                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                    }
//                    
//                    /*
//                     
//                     Ok I agree that it is confusing but if you understand algorithm well and when u see this code in xcode instead of here than it will make more sense.
//                     
//                     */
//                    
//                }
//                
//                if (y<height)
//                {
//                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
//                    color = getColorCode(byteIndex, imageData);
//                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                    
//                    // Add the bottom point on the antialiasing list
//                    if (!IS_SAME_COLOR)
//                        [antiAliasingPoints pushFrontX:x andY:y];
//                }
//            }
//            
//            // For each point marked
//            // perform antialiasing on the same pixel, plus the top,left,bottom and right pixel
//            unsigned int antialiasColor = getColorCodeFromUIColor(newColor,bitmapInfo&kCGBitmapByteOrderMask );
//            int red1   = ((0xff000000 & antialiasColor) >> 24);
//            int green1 = ((0x00ff0000 & antialiasColor) >> 16);
//            int blue1  = ((0x0000ff00 & antialiasColor) >> 8);
//            int alpha1 =  (0x000000ff & antialiasColor);
//            
//            // LOOP 3
//            while ([antiAliasingPoints popFront:&x andY:&y] != INVALID_NODE_CONTENT)
//            {
//                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
//                color = getColorCode(byteIndex, imageData);
//                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                
//                if (!compareColor(ncolor, color, 0))
//                {
//                    color = 0x000000ff;
//                    
//                    int red2   = ((0xff000000 & color) >> 24);
//                    int green2 = ((0x00ff0000 & color) >> 16);
//                    int blue2 = ((0x0000ff00 & color) >> 8);
//                    int alpha2 =  (0x000000ff & color);
//                    
//                    if (antiAlias) {
//                        imageData[byteIndex + 0] = (red1 + red2) / 2;
//                        imageData[byteIndex + 1] = (green1 + green2) / 2;
//                        imageData[byteIndex + 2] = (blue1 + blue2) / 2;
//                        imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
//                    } else {
//                        imageData[byteIndex + 0] = red2;
//                        imageData[byteIndex + 1] = green2;
//                        imageData[byteIndex + 2] = blue2;
//                        imageData[byteIndex + 3] = alpha2;
//                    }
//                    
//#if DEBUG_ANTIALIASING
//                    imageData[byteIndex + 0] = 0;
//                    imageData[byteIndex + 1] = 0;
//                    imageData[byteIndex + 2] = 255;
//                    imageData[byteIndex + 3] = 255;
//#endif
//                }
//                
//                // left
//                if (x>0)
//                {
//                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x - 1) * bytesPerPixel;
//                    color = getColorCode(byteIndex, imageData);
//                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                    
//                    if (!compareColor(ncolor, color, 0))
//                    {
//                        color = 0x000000ff;
//                        
//                        int red2   = ((0xff000000 & color) >> 24);
//                        int green2 = ((0x00ff0000 & color) >> 16);
//                        int blue2 = ((0x0000ff00 & color) >> 8);
//                        int alpha2 =  (0x000000ff & color);
//                        
//                        if (antiAlias) {
//                            imageData[byteIndex + 0] = (red1 + red2) / 2;
//                            imageData[byteIndex + 1] = (green1 + green2) / 2;
//                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
//                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
//                        } else {
//                            imageData[byteIndex + 0] = red2;
//                            imageData[byteIndex + 1] = green2;
//                            imageData[byteIndex + 2] = blue2;
//                            imageData[byteIndex + 3] = alpha2;
//                        }
//                        
//#if DEBUG_ANTIALIASING
//                        imageData[byteIndex + 0] = 0;
//                        imageData[byteIndex + 1] = 0;
//                        imageData[byteIndex + 2] = 255;
//                        imageData[byteIndex + 3] = 255;
//#endif
//                    }
//                }
//                if (x<width)
//                {
//                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x + 1) * bytesPerPixel;
//                    color = getColorCode(byteIndex, imageData);
//                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                    
//                    if (!compareColor(ncolor, color, 0))
//                    {
//                        
//                        color = 0x000000ff;
//                        
//                        int red2   = ((0xff000000 & color) >> 24);
//                        int green2 = ((0x00ff0000 & color) >> 16);
//                        int blue2 = ((0x0000ff00 & color) >> 8);
//                        int alpha2 =  (0x000000ff & color);
//                        
//                        if (antiAlias) {
//                            imageData[byteIndex + 0] = (red1 + red2) / 2;
//                            imageData[byteIndex + 1] = (green1 + green2) / 2;
//                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
//                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
//                        } else {
//                            imageData[byteIndex + 0] = red2;
//                            imageData[byteIndex + 1] = green2;
//                            imageData[byteIndex + 2] = blue2;
//                            imageData[byteIndex + 3] = alpha2;
//                        }
//                        
//#if DEBUG_ANTIALIASING
//                        imageData[byteIndex + 0] = 0;
//                        imageData[byteIndex + 1] = 0;
//                        imageData[byteIndex + 2] = 255;
//                        imageData[byteIndex + 3] = 255;
//#endif
//                    }
//                    
//                }
//                
//                if (y>0)
//                {
//                    byteIndex = (bytesPerRow * roundf(y - 1)) + roundf(x) * bytesPerPixel;
//                    color = getColorCode(byteIndex, imageData);
//                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                    
//                    if (!compareColor(ncolor, color, 0))
//                    {
//                        
//                        color = 0x000000ff;
//                        
//                        int red2   = ((0xff000000 & color) >> 24);
//                        int green2 = ((0x00ff0000 & color) >> 16);
//                        int blue2 = ((0x0000ff00 & color) >> 8);
//                        int alpha2 =  (0x000000ff & color);
//                        
//                        if (antiAlias) {
//                            imageData[byteIndex + 0] = (red1 + red2) / 2;
//                            imageData[byteIndex + 1] = (green1 + green2) / 2;
//                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
//                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
//                        } else {
//                            imageData[byteIndex + 0] = red2;
//                            imageData[byteIndex + 1] = green2;
//                            imageData[byteIndex + 2] = blue2;
//                            imageData[byteIndex + 3] = alpha2;
//                        }
//                        
//#if DEBUG_ANTIALIASING
//                        imageData[byteIndex + 0] = 0;
//                        imageData[byteIndex + 1] = 0;
//                        imageData[byteIndex + 2] = 255;
//                        imageData[byteIndex + 3] = 255;
//#endif
//                    }
//                }
//                
//                if (y<height)
//                {
//                    byteIndex = (bytesPerRow * roundf(y + 1)) + roundf(x) * bytesPerPixel;
//                    color = getColorCode(byteIndex, imageData);
//                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
//                    
//                    if (!compareColor(ncolor, color, 0))
//                    {
//                        color = 0x000000ff;
//                        
//                        int red2   = ((0xff000000 & color) >> 24);
//                        int green2 = ((0x00ff0000 & color) >> 16);
//                        int blue2 = ((0x0000ff00 & color) >> 8);
//                        int alpha2 =  (0x000000ff & color);
//                        
//                        if (antiAlias) {
//                            
//                            imageData[byteIndex + 0] = (red1 + red2) / 2;
//                            imageData[byteIndex + 1] = (green1 + green2) / 2;
//                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
//                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
//                            
//                            
//                        } else {
//                            imageData[byteIndex + 0] = red2;
//                            imageData[byteIndex + 1] = green2;
//                            imageData[byteIndex + 2] = blue2;
//                            imageData[byteIndex + 3] = alpha2;
//                        }
//                        
//#if DEBUG_ANTIALIASING
//                        imageData[byteIndex + 0] = 0;
//                        imageData[byteIndex + 1] = 0;
//                        imageData[byteIndex + 2] = 255;
//                        imageData[byteIndex + 3] = 255;
//#endif
//                    }
//                    
//                }
//            }
//            
//            /*
//             
//             Convert bitmap image back to UIImage
//             
//             - This is pretty simple one we have filled image in context so now we make UIImage from it.
//             
//             */
//            
//            CGImageRef newCGImage = CGBitmapContextCreateImage(context);
//            
//            UIImage *result = [UIImage imageWithCGImage:newCGImage scale:[self scale] orientation:UIImageOrientationUp];
//            
//            CGImageRelease(newCGImage);
//            
//            CGContextRelease(context);
//            
//            free(imageData);
//            
//            return result;
//        }
//        @catch (NSException *exception)
//        {
//            ////NSLog(@"Exception : %@", exception);
//        }
//    }
//}

- (UIImage *) ccUndo
{
    NSDictionary *dict = [[CCUndoManager sharedManager] performUndo];
    
    CGPoint startPoint = [[dict objectForKey:@"point"] CGPointValue];
    UIColor *newColor = [dict objectForKey:@"colour"];
    
    //    NSLog(@"New Colour: %@", newColor);
    
    int tolerance = 0;
    BOOL antiAlias = YES;
    BOOL isPreviousColourWhite = isWhiteF(newColor);
    
    @autoreleasepool{
        @try
        {
            /*
             First We create rowData from UIImage.
             We require this conversation so that we can use detail at pixel like color at pixel.
             You can get some discussion about this topic here:
             http://stackoverflow.com/questions/448125/how-to-get-pixel-data-from-a-uiimage-cocoa-touch-or-cgimage-core-graphics
             */
            
            
            CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
            
            CGImageRef imageRef = [self CGImage];
            
            NSUInteger width = CGImageGetWidth(imageRef);
            NSUInteger height = CGImageGetHeight(imageRef);
            
            ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
            
            CGFloat ratio = width / height;
            startPoint = CGPointMake(startPoint.x * ratio, startPoint.y * ratio);
            
            ////NSLog(@"Start point: %@", NSStringFromCGPoint(startPoint));
            
            //        ////NSLog(@"Width: %lu, Height: %lu", (unsigned long)width, (unsigned long)height);
            
            unsigned char *imageData = malloc(height * width * 4);
            //        unsigned char *imageData = (unsigned char*) calloc(height * width * 4, sizeof(unsigned char));
            
            NSUInteger bytesPerPixel = 4;       //CGImageGetBitsPerPixel(imageRef) / 8;
            NSUInteger bytesPerRow = width * 4;      //CGImageGetBytesPerRow(imageRef);
            NSUInteger bitsPerComponent = 8;    //CGImageGetBitsPerComponent(imageRef);
            
            //NSLog(@"");
            //NSLog(@"-ENHANCED--");
            //NSLog(@"bytesPerPixel: %lu", (unsigned long)bytesPerPixel);
            //NSLog(@"bytesPerRow: %lu", (unsigned long)bytesPerRow);
            //NSLog(@"bitsPerComponent: %lu", (unsigned long)bitsPerComponent);
            
            
            //NSLog(@"--IMAGE INFO--");
            //NSLog(@"Bytes per pixel: %lu", CGImageGetBitsPerPixel(imageRef) / 8);
            //NSLog(@"BytesPerRow: %lu", CGImageGetBytesPerRow(imageRef));
            //NSLog(@"BitsPerComponent: %zul", CGImageGetBitsPerComponent(imageRef));
            //NSLog(@"bitmap info: %ul", CGImageGetBitmapInfo(imageRef));
            //NSLog(@"");
            
            CGBitmapInfo bitmapInfo = bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;    //CGImageGetBitmapInfo(imageRef);
            
            
            
            if (kCGImageAlphaLast == (uint32_t)bitmapInfo || kCGImageAlphaFirst == (uint32_t)bitmapInfo) {
                bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;
            }
            
            //        CGContextRef context = CGBitmapContextCreate (imageData, width, height, 8, width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
            
            CGContextRef context = CGBitmapContextCreate(imageData,
                                                         width,
                                                         height,
                                                         bitsPerComponent,
                                                         bytesPerRow,
                                                         colorSpace,
                                                         bitmapInfo);
            CGColorSpaceRelease(colorSpace);
            
            CGContextDrawImage(context, CGRectMake(0, 0, width, height), imageRef);
            
            
            
            unsigned long length = strlen(imageData);
            
            
            //NSLog(@"---- ---- ---- ---- ---- ----");
            //NSLog(@"data size X: %d", sizeof(imageData));
            //NSLog(@"Data length X: %ul", length);
            //NSLog(@"---- ---- ---- ---- ---- ----");
            
            //
            //            if (length < 4) {
            //                return nil;
            //            }
            
            //Get color at start point
            unsigned int byteIndex = (bytesPerRow * roundf(startPoint.y)) + roundf(startPoint.x) * bytesPerPixel;
            
            
            
            unsigned int ocolor = getColorCode(byteIndex, imageData);
            
            if (compareColor(ocolor, 0, 0)) {
                
                CGContextRelease(context);
                free(imageData);
                
                return nil;
            }
            
            int redX   = ((0xff000000 & ocolor) >> 24);
            int greenX = ((0x00ff0000 & ocolor) >> 16);
            int blueX = ((0x0000ff00 & ocolor) >> 8);
            int alphaX =  (0x000000ff & ocolor);
            
            //            UIColor *prevColour = [UIColor colorWithRed:redX/255.0 green:greenX/255.0 blue:blueX/255.0 alpha:alphaX/255.0];
            //            [[CCUndoManager sharedManager] recordAction:prevColour withPoint:startPoint];
            
            
            //Convert newColor to RGBA value so we can save it to image.
            int newRed, newGreen, newBlue, newAlpha;
            
            const CGFloat *components = CGColorGetComponents(newColor.CGColor);
            
            /*
             If you are not getting why I use CGColorGetNumberOfComponents than read following link:
             http://stackoverflow.com/questions/9238743/is-there-an-issue-with-cgcolorgetcomponents
             */
            
            if(CGColorGetNumberOfComponents(newColor.CGColor) == 2)
            {
                newRed   = newGreen = newBlue = components[0] * 255;
                newAlpha = components[1] * 255;
            }
            else if (CGColorGetNumberOfComponents(newColor.CGColor) == 4)
            {
                if ((bitmapInfo&kCGBitmapByteOrderMask) == kCGBitmapByteOrder32Little)
                {
                    newRed   = components[2] * 255;
                    newGreen = components[1] * 255;
                    newBlue  = components[0] * 255;
                    newAlpha = 255;
                }
                else
                {
                    newRed   = components[0] * 255;
                    newGreen = components[1] * 255;
                    newBlue  = components[2] * 255;
                    newAlpha = 255;
                }
            }
            
            unsigned int ncolor = (newRed << 24) | (newGreen << 16) | (newBlue << 8) | newAlpha;
            
            /*
             We are using stack to store point.
             Stack is implemented by LinkList.
             To incress speed I have used NSMutableData insted of NSMutableArray.
             To see Detail Of This implementation visit following leink.
             http://iwantmyreal.name/blog/2012/09/29/a-faster-array-in-objective-c/
             */
            
            
            
            LinkedListStack *points = [[LinkedListStack alloc] initWithCapacity:500 incrementSize:500 andMultiplier:height];
            LinkedListStack *antiAliasingPoints = [[LinkedListStack alloc] initWithCapacity:500 incrementSize:500 andMultiplier:height];
            
            int x = roundf(startPoint.x);
            int y = roundf(startPoint.y);
            
            //add starting point where user touch in stack
            [points pushFrontX:x andY:y];
            
            /*
             This algorithem is prety simple though it llook odd in Objective C syntex.
             To get familer with this algorithm visit following link.
             http://lodev.org/cgtutor/floodfill.html
             You can read hole artical for knowledge.
             If you are familer with flood fill than got to Scanline Floodfill Algorithm With Stack (floodFillScanlineStack)
             */
            
            unsigned int color;
            unsigned int originalColor;
            BOOL spanLeft,spanRight;
            
            // LOOP 1
            
            /*
             Run Loop Till we have pixel in stack
             - it means stack has no more point and area is filled with newColor
             */
            
            while ([points popFront:&x andY:&y] != INVALID_NODE_CONTENT)
            {
                
                /*
                 Get value from Stack.
                 - If you have look care fully we have already done this in while condition.  [points popFront:&x andY:&y] will take point from stack and store it in x and y.
                 */
                
                
                /*
                 Find highest value of Y with old color (We refer old color to color which is at starting point).
                 - Here highest word may miss lead. here highest means upper side of image. so we can say “first pixel in column x with old color”. every time we start from upper side and go (or come) down. To do this we use following code.
                 */
                
                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                
                color = getColorCode(byteIndex, imageData);
                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                
                //COMPARE 1
                
                while(y >= 0 && IS_SAME_COLOR)
                {
                    y--;
                    
                    if(y >= 0)
                    {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                        
                    }
                }
                
                // Add the top most point on the antialiasing list
                if(y >= 0 && !IS_SAME_COLOR)
                    if(y >= 0 && !IS_SAME_COLOR)
                    {
                        [antiAliasingPoints pushFrontX:x andY:y];
                    }
                
                y++;
                
                spanLeft = spanRight = NO;
                
                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                
                color = getColorCode(byteIndex, imageData);
                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                
                // LOOP 2
                
                /*
                 Run Loop Till Y reach to end of image or current pixel color is not same as old color.
                 - so we start from up side so we will stop when we reach till down. Following while condition willl do it.
                 */
                
                while (y < height && IS_SAME_COLOR && ncolor != color)
                {
                    
                    /*
                     
                     We have three condition here First will stop if we reach at last pixel of image.
                     second will stop if we reach in color area where we don’t need to feel color (color other than old color).
                     and third will stop if the color is alredy replaced with newColor.
                     
                     */
                    
                    
                    //Replace Old Color with new color
                    
                    imageData[byteIndex + 0] = newRed;
                    imageData[byteIndex + 1] = newGreen;
                    imageData[byteIndex + 2] = newBlue;
                    imageData[byteIndex + 3] = newAlpha;
                    
                    /*
                     
                     A. Check color of pixel nere current (x+1 & x-1)
                     
                     B. Add value to stack if require.
                     
                     C. Increase Y by 1
                     
                     - I have merged this three step because it is very tide with each other. Which pixel will color is depend on what we add in stack so now  we will see how to add pixel in code.
                     
                     */
                    
                    if(x > 0)
                    {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x - 1) * bytesPerPixel;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                        
                        if(!spanLeft && x > 0 && IS_SAME_COLOR)
                        {
                            [points pushFrontX:(x - 1) andY:y];
                            
                            spanLeft = YES;
                        }
                        else if(spanLeft && x > 0 && !IS_SAME_COLOR)
                        {
                            spanLeft = NO;
                        }
                        
                        // we can't go left. Add the point on the antialiasing list
                        if(!spanLeft && x > 0 && !IS_SAME_COLOR && !compareColor(ncolor, color, tolerance))
                        {
                            [antiAliasingPoints pushFrontX:(x - 1) andY:y];
                        }
                    }
                    
                    if(x < width - 1)
                    {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x + 1) * bytesPerPixel;;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                        
                        if(!spanRight && IS_SAME_COLOR)
                        {
                            [points pushFrontX:(x + 1) andY:y];
                            
                            spanRight = YES;
                        }
                        else if(spanRight && !IS_SAME_COLOR)
                        {
                            spanRight = NO;
                        }
                        
                        // we can't go right. Add the point on the antialiasing list
                        if(!spanRight && !IS_SAME_COLOR && !compareColor(ncolor, color, tolerance))
                        {
                            [antiAliasingPoints pushFrontX:(x + 1) andY:y];
                        }
                    }
                    
                    y++;
                    
                    if(y < height)
                    {
                        byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                        
                        color = getColorCode(byteIndex, imageData);
                        originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    }
                    
                    /*
                     
                     Ok I agree that it is confusing but if you understand algorithm well and when u see this code in xcode instead of here than it will make more sense.
                     
                     */
                    
                }
                
                if (y<height)
                {
                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    // Add the bottom point on the antialiasing list
                    if (!IS_SAME_COLOR)
                        [antiAliasingPoints pushFrontX:x andY:y];
                }
            }
            
            // For each point marked
            // perform antialiasing on the same pixel, plus the top,left,bottom and right pixel
            unsigned int antialiasColor = getColorCodeFromUIColor(newColor,bitmapInfo&kCGBitmapByteOrderMask );
            int red1   = ((0xff000000 & antialiasColor) >> 24);
            int green1 = ((0x00ff0000 & antialiasColor) >> 16);
            int blue1  = ((0x0000ff00 & antialiasColor) >> 8);
            int alpha1 =  (0x000000ff & antialiasColor);
            
            // LOOP 3
            while ([antiAliasingPoints popFront:&x andY:&y] != INVALID_NODE_CONTENT)
            {
                byteIndex = (bytesPerRow * roundf(y)) + roundf(x) * bytesPerPixel;
                color = getColorCode(byteIndex, imageData);
                originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                
                BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                
                if (!compareColor(ncolor, color, 0) && validLength)
                {
                    color = 0x000000ff;
                    
                    int red2   = ((0xff000000 & color) >> 24);
                    int green2 = ((0x00ff0000 & color) >> 16);
                    int blue2 = ((0x0000ff00 & color) >> 8);
                    int alpha2 =  (0x000000ff & color);
                    
                    if (antiAlias) {
                        
                        if (isPreviousColourWhite) {
                            
                            imageData[byteIndex + 0] = 0;
                            imageData[byteIndex + 1] = 0;
                            imageData[byteIndex + 2] = 0;
                            imageData[byteIndex + 3] = 255;
                            
                        }else{
                            
                            imageData[byteIndex + 0] = (red1 + red2) / 2;
                            imageData[byteIndex + 1] = (green1 + green2) / 2;
                            imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                            imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                            
                        }
                        
                    } else {
                        imageData[byteIndex + 0] = red2;
                        imageData[byteIndex + 1] = green2;
                        imageData[byteIndex + 2] = blue2;
                        imageData[byteIndex + 3] = alpha2;
                    }
                    
#if DEBUG_ANTIALIASING
                    imageData[byteIndex + 0] = 0;
                    imageData[byteIndex + 1] = 0;
                    imageData[byteIndex + 2] = 255;
                    imageData[byteIndex + 3] = 255;
#endif
                }
                
                // left
                if (x>0)
                {
                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x - 1) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            
                            if (isPreviousColourWhite) {
                                
                                imageData[byteIndex + 0] = 0;
                                imageData[byteIndex + 1] = 0;
                                imageData[byteIndex + 2] = 0;
                                imageData[byteIndex + 3] = 255;
                                
                            }else{
                                
                                imageData[byteIndex + 0] = (red1 + red2) / 2;
                                imageData[byteIndex + 1] = (green1 + green2) / 2;
                                imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                                imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                                
                            }
                            
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                }
                if (x<width)
                {
                    byteIndex = (bytesPerRow * roundf(y)) + roundf(x + 1) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            
                            if (isPreviousColourWhite) {
                                
                                imageData[byteIndex + 0] = 0;
                                imageData[byteIndex + 1] = 0;
                                imageData[byteIndex + 2] = 0;
                                imageData[byteIndex + 3] = 255;
                                
                            }else{
                                
                                imageData[byteIndex + 0] = (red1 + red2) / 2;
                                imageData[byteIndex + 1] = (green1 + green2) / 2;
                                imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                                imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                                
                            }
                            
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                    
                }
                
                if (y>0)
                {
                    byteIndex = (bytesPerRow * roundf(y - 1)) + roundf(x) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            
                            if (isPreviousColourWhite) {
                                
                                imageData[byteIndex + 0] = 0;
                                imageData[byteIndex + 1] = 0;
                                imageData[byteIndex + 2] = 0;
                                imageData[byteIndex + 3] = 255;
                                
                            }else{
                                
                                imageData[byteIndex + 0] = (red1 + red2) / 2;
                                imageData[byteIndex + 1] = (green1 + green2) / 2;
                                imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                                imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                                
                            }
                            
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                }
                
                if (y<height)
                {
                    byteIndex = (bytesPerRow * roundf(y + 1)) + roundf(x) * bytesPerPixel;
                    color = getColorCode(byteIndex, imageData);
                    //App crached becaused original data was empty
                    originalColor = getColorCode(byteIndex, ORIGINAL_DATA);
                    
                    BOOL validLength = (byteIndex <= [[ImageManager sharedManager] imageData].length);
                    
                    if (!compareColor(ncolor, color, 0) && validLength)
                    {
                        color = 0x000000ff;
                        
                        int red2   = ((0xff000000 & color) >> 24);
                        int green2 = ((0x00ff0000 & color) >> 16);
                        int blue2 = ((0x0000ff00 & color) >> 8);
                        int alpha2 =  (0x000000ff & color);
                        
                        if (antiAlias) {
                            
                            if (isPreviousColourWhite) {
                                
                                imageData[byteIndex + 0] = 0;
                                imageData[byteIndex + 1] = 0;
                                imageData[byteIndex + 2] = 0;
                                imageData[byteIndex + 3] = 255;
                                
                            }else{
                                
                                imageData[byteIndex + 0] = (red1 + red2) / 2;
                                imageData[byteIndex + 1] = (green1 + green2) / 2;
                                imageData[byteIndex + 2] = (blue1 + blue2) / 2;
                                imageData[byteIndex + 3] = (alpha1 + alpha2) / 2;
                                
                            }
                            
                        } else {
                            imageData[byteIndex + 0] = red2;
                            imageData[byteIndex + 1] = green2;
                            imageData[byteIndex + 2] = blue2;
                            imageData[byteIndex + 3] = alpha2;
                        }
                        
#if DEBUG_ANTIALIASING
                        imageData[byteIndex + 0] = 0;
                        imageData[byteIndex + 1] = 0;
                        imageData[byteIndex + 2] = 255;
                        imageData[byteIndex + 3] = 255;
#endif
                    }
                    
                }
            }
            
            /*
             
             Convert bitmap image back to UIImage
             
             - This is pretty simple one we have filled image in context so now we make UIImage from it.
             
             */
            
            points = nil;
            antiAliasingPoints = nil;
            
            
            CGImageRef newCGImage = CGBitmapContextCreateImage(context);
            
            UIImage *result = [UIImage imageWithCGImage:newCGImage scale:[self scale] orientation:UIImageOrientationUp];
            
            CGImageRelease(newCGImage);
            
            CGContextRelease(context);
            
            free(imageData);
            
            return result;
        }
        @catch (NSException *exception)
        {
            ////NSLog(@"Exception : %@", exception);
        }
        
    }
}


/*
 I have used pure C function because it is said than C function is faster than Objective - C method in call.
 This two function are called most of time so it require that calling this work in speed.
 I have not verified this performance so I like to here comment on this.
 */
/*
 This function extract color from image and convert it to integer represent.
 
 Converting to integer make comperation easy.
 */
unsigned int getColorCode (unsigned int byteIndex, unsigned char *imageData)
{
    //byteIndex
    
    if(byteIndex > [[ImageManager sharedManager] imageData].length){
        
        unsigned int red   = 0;
        unsigned int green = 0;
        unsigned int blue  = 0;
        unsigned int alpha = 0;
        
        return (red << 24) | (green << 16) | (blue << 8) | alpha;
        
    }else{
        
        unsigned int red   = imageData[byteIndex];
        unsigned int green = imageData[byteIndex + 1];
        unsigned int blue  = imageData[byteIndex + 2];
        unsigned int alpha = imageData[byteIndex + 3];
        
        return (red << 24) | (green << 16) | (blue << 8) | alpha;
        
    }
    
    
}

/*
 This function compare two color with counting tolerance value.
 
 If color is between tolerance rancge than it return true other wise false.
 */
bool compareColor (unsigned int color1, unsigned int color2, int tolorance)
{
//    if(color1 == color2)
//        return true;
    return color1 == color2;
    
    int red1   = ((0xff000000 & color1) >> 24);
    int green1 = ((0x00ff0000 & color1) >> 16);
    int blue1  = ((0x0000ff00 & color1) >> 8);
    int alpha1 =  (0x000000ff & color1);
    
    int red2   = ((0xff000000 & color2) >> 24);
    int green2 = ((0x00ff0000 & color2) >> 16);
    int blue2  = ((0x0000ff00 & color2) >> 8);
    int alpha2 =  (0x000000ff & color2);
    
    int diffRed   = abs(red2   - red1);
    int diffGreen = abs(green2 - green1);
    int diffBlue  = abs(blue2  - blue1);
    int diffAlpha = abs(alpha2 - alpha1);
    
    if( diffRed   > tolorance ||
       diffGreen > tolorance ||
       diffBlue  > tolorance ||
       diffAlpha > tolorance  )
    {
        return false;
    }
    
    return true;
}

unsigned int getColorCodeFromUIColor(UIColor *color, CGBitmapInfo orderMask)
{
    //Convert newColor to RGBA value so we can save it to image.
    int newRed, newGreen, newBlue, newAlpha;
    
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    /*
     If you are not getting why I use CGColorGetNumberOfComponents than read following link:
     http://stackoverflow.com/questions/9238743/is-there-an-issue-with-cgcolorgetcomponents
     */
    
    if(CGColorGetNumberOfComponents(color.CGColor) == 2)
    {
        newRed   = newGreen = newBlue = components[0] * 255;
        newAlpha = components[1] * 255;
    }
    else if (CGColorGetNumberOfComponents(color.CGColor) == 4)
    {
        if (orderMask == kCGBitmapByteOrder32Little)
        {
            newRed   = components[2] * 255;
            newGreen = components[1] * 255;
            newBlue  = components[0] * 255;
            newAlpha = 255;
        }
        else
        {
            newRed   = components[0] * 255;
            newGreen = components[1] * 255;
            newBlue  = components[2] * 255;
            newAlpha = 255;
        }
    }
    else
    {
        newRed   = newGreen = newBlue = 0;
        newAlpha = 255;
    }
    
    unsigned int ncolor = (newRed << 24) | (newGreen << 16) | (newBlue << 8) | newAlpha;
    
    return ncolor;
}


bool isWhiteF(UIColor *colour){
    
    const CGFloat* components = CGColorGetComponents(colour.CGColor);
    return (components[0] == 1.0) && (components[1] == 1.0) && (components[2] == 1.0);
    
}



@end
