//
//  UIImage+FloodFill.h
//  ImageFloodFilleDemo
//
//  Created by chintan on 15/07/13.
//  Copyright (c) 2013 ZWT. All rights reserved.
//https://github.com/Chintan-Dave/UIImageScanlineFloodfill

#import <UIKit/UIKit.h>
#import "LinkedListStack.h"

@interface UIImage (FloodFill)

- (UIImage *) floodFillFromPoint:(CGPoint)startPoint withColor:(UIColor *)newColor andTolerance:(int)tolerance;
- (UIImage *) ccUndo;
- (UIImage *) floodFillFromPoint:(CGPoint)startPoint withColor:(UIColor *)newColor andTolerance:(int)tolerance useAntiAlias:(BOOL)antiAlias;
- (UIColor *) colorAtPoint:(CGPoint) startPoint;

@end
