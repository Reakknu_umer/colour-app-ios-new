//
//  ImageManager.h
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 3/5/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface ImageManager : NSObject{
    NSData *imageData;
    CGSize imageSize;
    NSUInteger bytesPerPixel;
    NSUInteger bytesPerRow;
    NSUInteger bitsPerComponent;
}

@property (nonatomic, retain) NSData *imageData;
@property (nonatomic, assign) CGSize imageSize;
@property (nonatomic, assign) NSUInteger bytesPerPixel;
@property (nonatomic, assign) NSUInteger bytesPerRow;
@property (nonatomic, assign) NSUInteger bitsPerComponent;

+ (id)sharedManager;

- (void)configure:(UIImage *) image;
- (NSUInteger) byteIndexForPoint:(CGPoint) startPoint;
- (BOOL) canDrawAtPoint:(CGPoint) point;
- (BOOL) canDrawAtByteIndex:(NSUInteger)byteIndex;


@end
