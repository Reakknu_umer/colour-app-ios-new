//
//  CCUndoManager.m
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 2/22/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//

#import "CCUndoManager.h"

@implementation CCUndoManager

@synthesize actions;

#pragma mark Singleton Methods

+ (id)sharedManager {
    static CCUndoManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        actions = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

-(void) clear; {
    [actions removeAllObjects];
}

-(BOOL) hasActions;{
    
    return [actions count] != 0;
    
}

-(void) recordAction:(UIColor *) colour withPoint:(CGPoint) point;{
    
    NSDictionary *dict = @{
                           @"colour" : colour,
                           @"point" : [NSValue valueWithCGPoint:point]
                           };
    
    [actions addObject:dict];
    
    //    const CGFloat* components = CGColorGetComponents(colour.CGColor);
    //    
    //    NSLog(@"Colour SAVED, R:%f G:%f B:%f A:%f at: %@", components[0], components[1], components[2], CGColorGetAlpha(colour.CGColor), NSStringFromCGPoint(point));
    
}

-(NSDictionary *) performUndo{
    
    NSDictionary *dict = [actions lastObject];
    
    CGPoint startPoint = [[dict objectForKey:@"point"] CGPointValue];
    UIColor *newColor = [dict objectForKey:@"colour"];
    
    //    const CGFloat* components = CGColorGetComponents(newColor.CGColor);
    //    
    //    NSLog(@"Colour POPED, R:%f G:%f B:%f A:%f at: %@", components[0], components[1], components[2], CGColorGetAlpha(newColor.CGColor), NSStringFromCGPoint(startPoint));
    
    [actions removeObject:dict];
    
    return  dict;
}


- (void) revealColour:(UIColor *) colour{
    
    
    
}


@end
