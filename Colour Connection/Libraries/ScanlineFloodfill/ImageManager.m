//
//  ImageManager.m
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 3/5/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//

#import "ImageManager.h"

@implementation ImageManager
@synthesize imageData, imageSize, bytesPerRow, bytesPerPixel, bitsPerComponent;

#pragma mark Singleton Methods

+ (id)sharedManager {
    static ImageManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        imageData = [[NSData alloc] init];
        imageSize = CGSizeZero;
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (void)configure:(UIImage *) image;{
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
    }
    
    CGImageRef imageRef = [image CGImage];
    imageSize = CGSizeMake(CGImageGetWidth(imageRef), CGImageGetHeight(imageRef));
    
    bytesPerPixel = 4;       //CGImageGetBitsPerPixel(imageRef) / 8;
    bytesPerRow = imageSize.width * 4;      //CGImageGetBytesPerRow(imageRef);
    bitsPerComponent =  8;    //CGImageGetBitsPerComponent(imageRef);
    
    CGBitmapInfo bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;    //CGImageGetBitmapInfo(imageRef);
    
    if (kCGImageAlphaLast == (uint32_t)bitmapInfo || kCGImageAlphaFirst == (uint32_t)bitmapInfo) {
        bitmapInfo = (uint32_t)kCGImageAlphaPremultipliedLast;
    }
    
    void *bitmapData = malloc(imageSize.width * imageSize.height * 4);
    
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Error: Memory not allocated!");
        CGColorSpaceRelease(colorSpace);
    }
    
    //    CGContextRef context = CGBitmapContextCreate (bitmapData, imageSize.width, imageSize.height, 8, imageSize.width * 4, colorSpace, kCGImageAlphaPremultipliedFirst);
    CGContextRef context = CGBitmapContextCreate (bitmapData, imageSize.width, imageSize.height, bitsPerComponent, bytesPerRow, colorSpace, bitmapInfo);
    CGColorSpaceRelease(colorSpace);
    
    if (context == NULL)
    {
        fprintf (stderr, "Error: Context not created!");
        free (bitmapData);
    }
    
    CGRect rect = (CGRect){.size = imageSize};
    CGContextDrawImage(context, rect, imageRef);
    Byte *byteData = CGBitmapContextGetData (context);
    CGContextRelease(context);
    
    imageData = [NSData dataWithBytes:byteData length:(imageSize.width * imageSize.height * 4)];
    free(bitmapData);
    
}

- (NSUInteger) byteIndexForPoint:(CGPoint) startPoint; {
    
    return (bytesPerRow * roundf(startPoint.y)) + roundf(startPoint.x) * bytesPerPixel;
    
}

- (BOOL) canDrawAtPoint:(CGPoint)point {
    
    //NSLog(@"Data length: %ul", imageData.length);
    
    if(imageData == nil){
        return NO;
    }
    
    unsigned int byteIndex = (unsigned int)[self byteIndexForPoint:point];
    unsigned int red   = ((unsigned char *)imageData.bytes)[byteIndex];
    unsigned int green = ((unsigned char *)imageData.bytes)[byteIndex + 1];
    unsigned int blue  = ((unsigned char *)imageData.bytes)[byteIndex + 2];
    
    //    NSLog(@"Can draw ? R:%d G:%d B:%d ", red, green,blue);
    
    BOOL isBlack = (red == 0) && (green == 0) && (blue == 0);
    
    return !isBlack;
    
}

- (BOOL) canDrawAtByteIndex:(NSUInteger)byteIndex {
    
    
    if(imageData == nil){
        return NO;
    }
    
    unsigned int red   = ((unsigned char *)imageData.bytes)[byteIndex];
    unsigned int green = ((unsigned char *)imageData.bytes)[byteIndex + 1];
    unsigned int blue  = ((unsigned char *)imageData.bytes)[byteIndex + 2];
    
    BOOL isBlack = (red == 0) && (green == 0) && (blue == 0);
    
    return !isBlack;
    
}

@end
