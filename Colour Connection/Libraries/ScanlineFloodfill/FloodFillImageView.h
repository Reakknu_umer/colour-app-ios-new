//
//  FloodFillImageView.h
//  ImageFloodFilleDemo
//
//  Created by chintan on 11/07/13.
//  Copyright (c) 2013 ZWT. All rights reserved.
//https://github.com/Chintan-Dave/UIImageScanlineFloodfill

#import <UIKit/UIKit.h>
#import "UIImage+FloodFill.h"
#import <objc/runtime.h>

typedef void(^ColorPickerCompletion)(UIColor *);

@interface FloodFillImageView : UIImageView
@property int tolorance;
@property (nonatomic, assign) BOOL isColourPickingOn;
@property (strong, nonatomic)   UIColor *newcolor;
@property (nonatomic, strong) ColorPickerCompletion completion;
@property (nonatomic, assign) BOOL isActive;
//@property (nonatomic, strong)   UIImage *originalImage;

-(void) undoAction;

@end
