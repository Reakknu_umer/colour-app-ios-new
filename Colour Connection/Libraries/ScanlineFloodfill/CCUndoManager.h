//
//  CCUndoManager.h
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 2/22/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CCUndoManager : NSObject{
    NSMutableArray *actions;
}

@property (nonatomic, retain) NSMutableArray *actions;

+ (id)sharedManager;

-(void) clear;
-(BOOL) hasActions;
-(void) recordAction:(UIColor *) colour withPoint:(CGPoint) point;
-(NSDictionary *) performUndo;

@end
