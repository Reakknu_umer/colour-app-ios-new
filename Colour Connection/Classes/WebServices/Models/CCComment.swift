//
//  CCComment.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCComment: CCModelBase {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var commentId:Int!
    var comment = ""
    var userId:Int!
    var userName = ""
    var userProfilePicture:URL!
    var createdTime:String!
    
    //MARK: Computed Properties    
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        commentId = json["comment_id"].intValue
        comment = json["comment"].stringValue
        userId = json["user_id"].intValue
        userName = json["username"].stringValue
        userProfilePicture = json["photo"].URL ?? URL(string: "")
        createdTime = Helper.localizeDate(json["created_at"].stringValue)

}
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
