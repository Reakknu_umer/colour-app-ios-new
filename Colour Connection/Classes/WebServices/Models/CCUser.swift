//
//  CCUser.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCUser: CCModelBase {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var userId:Int!
    var username = ""
    var userProfilePicture:URL!
    var isFollowing = false
    var email = ""
    
    //MARK: Computed Properties    
    //MARK: Initializers
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        userId = json["user_id"].intValue
        username = json["username"].stringValue
        userProfilePicture = json["photo"].URL ?? URL(string: "")
        email = json["email"].stringValue
        isFollowing = json["is_follow"].boolValue
        
        /*
         
         email = "thilina3001@gmail.com";
         followers = 2;
         followings = 30;
         "is_admin" = 0;
         "package_expire" = "";
         "payload_token" = ae427d560cba1c4f9a2e33467f6ababf;
         photo = "http://coloring.sandbox2.elegant-media.com/images/profile.jpg";
         posts =         (
         );
         token = 3L7GhWi3d5lmwRngvxnU0MLMzDU8o1Ve;
         "user_id" = 13;
         username = thilina3001;
         
         */
        
    }
    
    //    required init(json: SwiftyJSON.JSON) {
    //        super.init(json: json)
    //        
    //        if json["id"].string != nil {
    //            userId = json["id"].stringValue
    //        }else{
    //            userId = json["user_id"].stringValue
    //        }
    //        
    //        username = json["user_name"].stringValue
    //        firstName = json["first_name"].stringValue
    //        lastName = json["last_name"].stringValue
    //        introduction = json["introduction"].stringValue
    //        
    //        profileType = ProfileType(string:  json["profile_status"].stringValue)!
    //        
    //        if let profileUrl = json["profile_image"].URL {
    //            profilePictureUrl = profileUrl
    //        }else{
    //            profilePictureUrl = json["images"]["profile_image"]["http_url"].URL
    //        }
    //        
    //        if let profileUrl = json["cover_image"].URL {
    //            coverImageUrl = json["cover_image"].URL
    //        }else{
    //            coverImageUrl = json["images"]["cover_image"]["http_url"].URL
    //        }
    //        
    //        email = json["email"].stringValue
    //        designation = json["cur_designation"].string
    //        workPlace = json["cur_working_at"].string
    //        country = json["country"].string
    //        connectionCount = json["connection_count"].intValue
    //        mutualConnections = json["mutual_connection_count"].stringValue
    //    }
    //    
    func encodeWithCoder(_ aCoder: NSCoder) {
        //print("encodeWithCoder")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(userProfilePicture, forKey: "userProfilePicture")
        aCoder.encode(isFollowing, forKey: "isFollowing")
        aCoder.encode(email, forKey: "email")
    }
    
    init(coder aDecoder: NSCoder!) {
        
        super.init(json: JSON(["":""]))
        
        if let userId = aDecoder.decodeObject(forKey: "userId") as? Int {
           self.userId = userId
        }
//        userId = aDecoder.decodeInteger(forKey: "userId")
        username = aDecoder.decodeObject(forKey: "username") as! String
        userProfilePicture = aDecoder.decodeObject(forKey: "userProfilePicture") as? URL ?? URL(string: "")
        isFollowing = aDecoder.decodeBool(forKey: "isFollowing")
        email = aDecoder.decodeObject(forKey: "email") as! String
        
    }
    //
    //    //MARK: Segue
    //    //MARK: IBAction
    //    //MARK: - Private Functions
    //    
    func isCurrentUser()->Bool {
        //print("current user: \(UserSession.sharedSession.currentUser!.userId)")
        //print("user: \(userId)")
        return (UserSession.sharedSession.currentUser!.userId == userId)
    }
    
    //    func persistUser(){?
    //    }
    
    //

//
    //    //MARK: Helpers
    //    
    //    override func isEqual(object: AnyObject?) -> Bool {
    //        if let rhs = object as? User {
    //            return userId == rhs.userId
    //        }
    //        return false
    //    }
    //    
    //    override var hashValue: Int {
    //        return userId.hashValue
    //    }
    
}
