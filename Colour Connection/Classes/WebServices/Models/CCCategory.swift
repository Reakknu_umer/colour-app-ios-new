//
//  CCCategory.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCCategory: CCModelBase {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var categoryId:Int!
    var categoryName = ""
    var templates = [CCTemplate]()
    
    //MARK: Computed Properties    
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        categoryId = json["category_id"].intValue
        categoryName = json["category_name"].stringValue
        templates = CCTemplate.objectArray(json["templates"])
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
