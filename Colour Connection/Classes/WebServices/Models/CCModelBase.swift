//
//  CCModelBase.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCModelBase: NSObject {
    
    //MARK: Enums
    required init(json:SwiftyJSON.JSON){
        
    }
    
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
    class func objectArray<T:CCModelBase>(_ json:SwiftyJSON.JSON)->[T]{
        
        var objects = [T]()
        
        for (_,subJson):(String, JSON) in json {
            objects.append(T(json: subJson))
        }
        
        return objects
    }
    
}
