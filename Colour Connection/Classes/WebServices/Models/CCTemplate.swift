//
//  CCTemplate.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCTemplate: CCModelBase {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var templateId:Int!
    var categoryId:Int!
    var templateName = ""
    var imageName = ""
    var imageUrl:URL!
    var thumbImageUrl:URL!
    var svgUrl:URL!
    var isSubOnly = false
    
    //MARK: Computed Properties    
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        templateId = json["template_id"].intValue
        categoryId = json["category_id"].intValue
        templateName = json["template_name"].stringValue
        imageName = json["image_name"].stringValue
        imageUrl = json["image_png_url"].URL ?? URL(string : "")
        thumbImageUrl = json["thumb_image"].URL ?? URL(string : "")
        svgUrl = json["image_url"].URL ?? URL(string : "")
        isSubOnly = json["sub_only"].boolValue
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func isSaved()->Bool{
        
        return SavedTemplate.isTemplateExist(templateId)
        
    }
    
    func savedTemplate()->SavedTemplate?{
        return SavedTemplate.template(templateId)
    }
    
    //MARK: Helpers
    
}
