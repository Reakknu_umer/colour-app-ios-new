//
//  CCProfile.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCProfile: CCUser {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var followingCount = 0
    var followersCount = 0
    var posts = [CCPost]()
    
    var packageExpirationDate:Date!
    
    //MARK: Computed Properties    
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        followersCount = json["followers"].intValue
        followingCount = json["followings"].intValue
        followersCount = json["followers"].intValue
        
        posts = CCPost.objectArray(json["posts"])
        posts = posts.map({ (post) -> CCPost in
            post.userId = self.userId
            return post
        })
        
        /*
         
         email = "thilina3001@gmail.com";
         followers = 2;
         followings = 30;
         "is_admin" = 0;
         "package_expire" = "";
         "payload_token" = ae427d560cba1c4f9a2e33467f6ababf;
         photo = "http://coloring.sandbox2.elegant-media.com/images/profile.jpg";
         posts =         (
         );
         token = 3L7GhWi3d5lmwRngvxnU0MLMzDU8o1Ve;
         "user_id" = 13;
         username = thilina3001;
         
         */
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
