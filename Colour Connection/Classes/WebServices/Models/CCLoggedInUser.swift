//
//  LoggedInUser.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCLoggedInUser: CCUser {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var isAdmin = false
    var payloadToken = ""
    var packageExpirationDate:Date?
    var isMonthlyActive = false
    var isYearlyActive = false
    
    var isSubscribed:Bool {
        
        get {
            return packageExpirationDate != nil
        }
        
    }
    
    var isSubscriptionExpired:Bool {
        
        get{
            
            if packageExpirationDate == nil {
                return true
            }else{
                
                return Date().compare(packageExpirationDate!) == ComparisonResult.orderedDescending
                
            }

}
        
    }
    
    var isSubscriptionValid:Bool{
        
        get{
            return isSubscribed && !isSubscriptionExpired
        }
        
    }

//MARK: Computed Properties    
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        isAdmin = json["is_admin"].boolValue
        payloadToken = json["payload_token"].stringValue
        
        if let packageExpireTimeStamp = json["expire_date"].double {
            
            if packageExpireTimeStamp > 0 {
                packageExpirationDate = Date(timeIntervalSince1970: packageExpireTimeStamp)
            }
            
        }
        
    }
    
    override func encodeWithCoder(_ aCoder: NSCoder) {
        
        super.encodeWithCoder(aCoder)
        aCoder.encode(isAdmin, forKey: "isAdmin")
        aCoder.encode(isMonthlyActive, forKey: "isMonthlyActive")
        aCoder.encode(isYearlyActive, forKey: "isYearlyActive")
        aCoder.encode(payloadToken, forKey: "payloadToken")
        aCoder.encode(packageExpirationDate, forKey: "packageExpirationDate")
    }
    
    override init(coder aDecoder: NSCoder!) {
        
        super.init(coder: aDecoder)
        isAdmin = aDecoder.decodeBool(forKey: "isAdmin")
        isMonthlyActive = aDecoder.decodeBool(forKey: "isMonthlyActive")
        isYearlyActive = aDecoder.decodeBool(forKey: "isYearlyActive")
        payloadToken = aDecoder.decodeObject(forKey: "payloadToken") as! String
        packageExpirationDate = aDecoder.decodeObject(forKey: "packageExpirationDate") as? Date
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func setAsCurrentUser(){
        UserSession.sharedSession.currentUser = self
    }
    
    //MARK: Helpers
    
}

/*
 
 "user_id": 1,
 "username": "John",
 "email": "john@gmail.com",
 "token": "BbIUwypFBGtVpDr6kRTQ27uagatriMeE",
 "package_expire": "21-06-2017 09:30:00",
 "is_admin": 0,
 "payload_token":"10e76104dbc867234c0bb420f6979db8"
 "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
 
 */
