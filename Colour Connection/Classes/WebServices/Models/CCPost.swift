//
//  CCPost.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CCPost: CCModelBase {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var postId:Int!
    var postName = ""
    var userId:Int!
    var userName = ""
    var postImage:URL!
    var postThumbImage:URL!
    var likeCount = 0
    var commentCount = 0
    var templateId = 0
    var templateName = ""
    var isLiked = false
    var isFavourite = false
    var createdDate:String!
    var createdDateObject:Date!
    var comments = [CCComment]()
    
    //MARK: Computed Properties    
    //MARK: Initializers

required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        postId = json["post_id"].intValue
        postName = json["post_name"].stringValue
        userId = json["user_id"].intValue
        userName = json["username"].stringValue
    postImage = json["post_image"].URL ?? URL(string : "")
        postThumbImage = json["post_thumb_image"].URL ?? URL(string : "")
        likeCount = json["likes"].intValue
        commentCount = json["comments"].intValue //commentCount
        isLiked = json["liked"].boolValue
        isFavourite = json["is_favourite"].boolValue
        createdDate = Helper.localizeDate(json["created_at"].stringValue)
        createdDateObject = createdDate.date("dd/MM/yyyy") ?? Date()
        templateId = json["template_id"].intValue
        templateName = json["template_name"].stringValue
        
        comments = CCComment.objectArray(json["comments_list"])
        
        if postThumbImage == nil {
            postThumbImage = postImage
        }
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions

//MARK: Helpers
    
    override func isEqual(_ object: Any?) -> Bool {
        if let rhs = object as? CCPost {
            return postId == rhs.postId
        }
        return false
    }
    
    override var hashValue: Int {
        return postId.hashValue
    }
    
    func compare(_ post:CCPost)->ComparisonResult{
        
        return createdDateObject.compare(post.createdDateObject)
        
    }
    
    class func merge(_ newPosts:[CCPost], existingPosts:[CCPost])->[CCPost]{
        
        var set  = Set(existingPosts)
        
        //add missing posts to the set
        
        for p in newPosts {
            
            if set.contains(p) {
                
                if let index = set.index(of: p) {
                    
                    let object = set[index]
                    
                    object.isLiked = p.isLiked
                    object.isFavourite = p.isFavourite
                    object.likeCount = p.likeCount
                    object.commentCount = p.commentCount
                    object.comments = p.comments
                    
                }
                
            }else{
                
                set.insert(p)
                
            }
            
        }
        
        let modifiedPosts = Array(set)
        
        return modifiedPosts.sorted { (post1, post2) -> Bool in
            return post1.postId > post2.postId
        }
        
    }
    
}
