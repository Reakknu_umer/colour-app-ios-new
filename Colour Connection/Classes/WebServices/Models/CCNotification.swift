//
//  CCNotification.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import SwiftyJSON

class CCNotification: CCModelBase {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    
    var notificationId:Int!
    var notificationText = ""
    var userId:Int!
    var username = ""
    var postImage:URL!
    var postThumbImage:URL!
    var postId = 0
    var postName = ""
    
    var profilePicture:URL!
    
    //MARK: Computed Properties    
    //MARK: Initializers
    
    required init(json: SwiftyJSON.JSON) {
        super.init(json: json)
        
        postId = json["post_id"].intValue
        postName = json["post_name"].stringValue
        
        notificationId = json["notification_id"].intValue
        notificationText = json["description"].stringValue
        postImage = json["photo"].URL ?? URL(string: "")
        
        userId = json["user_id"].intValue
        username = json["username"].stringValue
        
        if let thumbUrl = json["post_thumb_image"].URL {
            postThumbImage = thumbUrl
        }else{
            postThumbImage = json["photo"].URL ?? URL(string: "")
        }
        
        /*
         
         "result":[{
         "notification_id": 1,
         "description": "Nice post",
         "user_id": 1,
         "username": "Petter",
         "post_image": "http:\/\/www.coloring.com\/images/posts/15246632.jpg",
         "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnail/15246632.jpg",
         "post_id": 1,
         "post_name": "New Image",
         }]"
         
         */
        
        /*
         "result":[{
         "notification_id": 1,
         "description": "is following you",
         "user_id": 1,
         "username": "Petter",
         "photo": "http:\/\/www.coloring.com\/images/profile/15246632.jpg",
         }]"
         */
        
        /*
         
         {
         description = "is following you";
         "notification_id" = 76;
         photo = "http://coloring.sandbox2.elegant-media.com/images/profile.jpg";
         "user_id" = 13;
         username = thilina3001;
         }

*/
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
