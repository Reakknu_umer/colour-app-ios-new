//
//  WebServiceAPI.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

enum WSEndPoint:String{
    
    //Artworks
    
    case getTemplate = "/getTemplate"
    case listArtIdeas = "/listArtIdeas"
    case listCategories = "/listCategories"
    case listTemplates = "/listTemplates"
    case listTestTemplates = "/listTestTemplates"
    case uploadPost = "/uploadPost"
    
    //Gallery
    
    case addComment = "/addComment"
    case addFavouritePost = "/addFavouritePost"
    case deletePost = "/deletePost"
    case exploreInspire = "/inspire"
    case exploreNewPosts = "/newPosts"
    case exploreTimeline = "/timeline"
    case favouritePosts = "/favouritePosts"
    case followUser = "/followUser"
    case likePost = "/likePost"
    case listComments = "/listComments"
    case listFollowers = "/followersList"
    case listFollowings = "/followingList"
    case otherUserProfile = "/otherUserProfile"
    case searchUsers = "/searchUsers"
    
    //Notifications
    
    case listUnreadComments = "/newComments"
    case listUnreadFollowings = "/listNewFollowings"
    case listUnreadLikes = "/listLikes"
    case notificationCount = "/notificationCount"
    case readComments = "/readComments"
    case readFollowings = "/readFollowings"
    case readLikes = "/readLikes"
    
    //user
    case profilePictureUpdate = "/profilePictureUpdate"
    case subscribePackage = "/subscribePackage"
    case subscriptionExpireDate = "/subscriptionExpireDate"
    case userForgotPassword = "/userForgotPassword"
    case userLogin = "/userLogin"
    case userLogout = "/userLogout"
    case userRegistration = "/userRegister"
    case userProfile = "/userProfile"
    case updateProfile = "/updateProfile"
    
    func urlString()->String{
        return "\(Config.baseUrl())\(self.rawValue)"
    }
    
}

private var Manager : Alamofire.SessionManager = {
    // Create the server trust policies
    let serverTrustPolicies: [String: ServerTrustPolicy] = [
        "coloring.sandbox2.elegant-media.com": .disableEvaluation
    ]
    // Create custom manager
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
    let man = Alamofire.SessionManager(
        configuration: URLSessionConfiguration.default,
        serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
    )
    return man
}()

class WebServiceAPI: NSObject {
    
    ///Artworks
    
    class func template(_ templateId:Int, success:@escaping ((CCTemplate)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["template_id":String(templateId)] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.getTemplate.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    //                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "pagination":
                     {
                     "total": 24,
                     "current_page": 1,
                     "last_page": 3,
                     "per_page": 8,
                     "prev_page_url": null,
                     "prev_page_url": "http:\/\/www.coloring.com\/api/v1/listArtIdeas?page=2",
                     "from": 1,
                     "to": 1,
                     }
                     
                     */
                    
                    let template = CCTemplate(json: json["result"])
                    //                    let posts = CCPost.objectArray(json["posts"]) as [CCPost]
                    success(template)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func listArtIdeas(_ templateId:Int, success:@escaping (([CCPost])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["template_id":String(templateId)] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.listArtIdeas.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "pagination":
                     {
                     "total": 24,
                     "current_page": 1,
                     "last_page": 3,
                     "per_page": 8,
                     "prev_page_url": null,
                     "prev_page_url": "http:\/\/www.coloring.com\/api/v1/listArtIdeas?page=2",
                     "from": 1,
                     "to": 1,
                     }
                     
                     */
                    
                    let posts = CCPost.objectArray(json["result"]["posts"]) as [CCPost]
                    success(posts)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func listCategories(_ success:@escaping (([CCCategory])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["api_key":Config.apiKey()]
        
        //print(params)
        
        
        Manager.request(WSEndPoint.listCategories.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    //saro
                    let categories = CCCategory.objectArray(json["result"]) as [CCCategory]
                    success(categories)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func listTemplates(_ categoryId:String, success:@escaping (([CCTemplate])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["category_id":categoryId] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.listTemplates.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    let templates = CCTemplate.objectArray(json["result"]) as [CCTemplate]
                    success(templates)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func listTestTemplates(_ success:@escaping (([CCTemplate])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.listTestTemplates.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    let templates = CCTemplate.objectArray(json["result"]) as [CCTemplate]
                    success(templates)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func uploadPost(_ postName:String, image:UIImage, templateId:String, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        let imageData:Data! = UIImagePNGRepresentation(image)
        
        //print("size of image in MB: %f ", (Double(imageData.length) / 1024.0) / 1024.0)
        
        let params = ["post_name" : postName, "template_id" : templateId] + self.defaultDictionary()

        
    
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in params {

                multipartFormData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)! , withName: key)
                
            }
            multipartFormData.append(imageData , withName: "post_image", fileName: "art.png", mimeType: "image/png")
        }, to: WSEndPoint.uploadPost.urlString() ,  method: .post, headers : nil,  encodingCompletion: { encodingResult in
            
            switch encodingResult {
                
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    SVProgressHUD.dismiss()
                    
                    if let value = response.result.value {
                        
                        if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                            
                            
                            success()
                            
                        }else{
                            failure()
                        }
                        
                    }else{
                        
                        failure()
                        
                    }
                    
                }
                
            case .failure(let encodingError):
                
                DispatchQueue.main.async {
                    Helper.showErrorMessage("Failed")
                    }
                
                failure()
                }
            }
        )
  
        
    }
    
    //Gallery
    
    class func addComment(_ postId:Int, comment:String, success:@escaping ((CCPost)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId),"comment":comment] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.addComment.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    let post = CCPost(json: json["result"])
                    success(post)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func deletePost(_ postId:Int, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId)] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.deletePost.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func exploreInspire(_ pageIndex:Int, success:@escaping (([CCPost], Int)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        var params = ["api_key":Config.apiKey(), "page": "\(pageIndex)"]
        
        if UserSession.sharedSession.isValid {
            params["token"] = UserSession.sharedSession.accessToken
        }
        
        print(params)
        Manager.request(WSEndPoint.exploreInspire.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                print(response.request)  // original URL request
                print(response.response) // URL response
                print(response.data)     // server data
                print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "pagination":
                     {
                     "total": 24,
                     "current_page": 1,
                     "last_page": 3,
                     "per_page": 8,
                     "prev_page_url": null,
                     "prev_page_url": "http:\/\/www.coloring.com\/api/v1/listArtIdeas?page=2",
                     "from": 1,
                     "to": 1,
                     },
                     
                     result =     {
                     pagination =         {
                     "current_page"
                     
                     */
                    
                    let posts = CCPost.objectArray(json["result"]["posts"]) as [CCPost]
                    let currentPage = json["result"]["pagination"]["current_page"].intValue
                    
                    success(posts, currentPage)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func exploreNewPosts(_ pageIndex:Int, success:@escaping (([CCPost],Int)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["api_key":Config.apiKey(), "page": "\(pageIndex)"]
        
        //print(params)
        Manager.request(WSEndPoint.exploreNewPosts.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "posts":[{
                     "post_id": 1,
                     "user_id": 8,
                     "post_image": "http:\/\/www.coloring.com\/images/posts/f6979db8.svg",
                     "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnails/f6979db8.svg",
                     "likes": 5,
                     "liked": 1,
                     "comments": 8,
                     }],
                     "last_seen": "15-12-2016 09:00:00",
                     */
                    
                    let posts = CCPost.objectArray(json["result"]["posts"]) as [CCPost]
                    let currentPage = json["result"]["pagination"]["current_page"].intValue
                    
                    success(posts, currentPage)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func exploreTimeline(_ pageIndex:Int, success:@escaping (([CCPost], Int)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = defaultDictionary() + ["page": "\(pageIndex)"]
        
        //print(params)
        Manager.request(WSEndPoint.exploreTimeline.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "pagination":
                     {
                     "total": 24,
                     "current_page": 1,
                     "last_page": 3,
                     "per_page": 8,
                     "prev_page_url": null,
                     "prev_page_url": "http:\/\/www.coloring.com\/api/v1/listArtIdeas?page=2",
                     "from": 1,
                     "to": 1,
                     },
                     */
                    
                    let posts = CCPost.objectArray(json["result"]["posts"]) as [CCPost]
                    let currentPage = json["result"]["pagination"]["current_page"].intValue
                    
                    success(posts, currentPage)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func favouritePosts(_ success:@escaping (([CCPost])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.favouritePosts.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "pagination":
                     {
                     "total": 24,
                     "current_page": 1,
                     "last_page": 3,
                     "per_page": 8,
                     "prev_page_url": null,
                     "prev_page_url": "http:\/\/www.coloring.com\/api/v1/listArtIdeas?page=2",
                     "from": 1,
                     "to": 1,
                     },
                     */
                    
                    let posts = CCPost.objectArray(json["result"]["posts"]) as [CCPost]
                    success(posts)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func followUser(_ userId:Int, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["follower_id":String(userId),"is_follow":"0"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.followUser.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func unfollowUser(_ userId:Int, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["follower_id":String(userId),"is_follow":"1"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.followUser.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func likePost(_ postId:Int, success:@escaping ((Int)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId), "like" :"1"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.likePost.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    let likeCount = json["result"].intValue
                    success(likeCount)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func favouritePost(_ postId:Int, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId),"favourite":"1"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.addFavouritePost.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func unfavouritePost(_ postId:Int, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId),"favourite":"0"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.addFavouritePost.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func unlikePost(_ postId:Int, success:@escaping ((Int)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId), "like" :"0"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.likePost.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    let likeCount = json["result"].intValue
                    success(likeCount)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func listComments(_ postId:Int, success:@escaping (([CCComment], CCPost, CCTemplate)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["post_id":String(postId)] + self.defaultDictionary()
        
        //let params = ["post_id":String(postId)] + self.defaultDictionary()
        //BbIUwypFBGtVpDr6kRTQ27uagatriMeE
        
        //print(params)
        Manager.request(WSEndPoint.listComments.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //                print(response.request)  // original URL request
                //                print(response.response) // URL response
                //                print(response.data)     // server data
                //                print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    //comments_list
                    
                    let comments = CCComment.objectArray(json["result"]["comments_list"]) as [CCComment]
                    let post = CCPost(json: json["result"])
                    let template = CCTemplate(json: json["result"])
                    
                    success(comments, post, template)
                    
                }else{
                    failure()
                }
                
        }
        //            .responseString { response in
        //                if let error = response.result.error {
        //                    print(error)
        //                }
        //                if let value = response.result.value {
        //                    print(value)
        //                }
        //        }
        
    }
    
    class func listFollowers(_ otherUserId:String?, success:@escaping (([CCUser])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        var params = self.defaultDictionary()
        
        if otherUserId != nil {
            params["other_user_id"] = otherUserId!
        }
        
        //print(params)
        Manager.request(WSEndPoint.listFollowers.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":[{
                     "user_id": 1,
                     "username": "Petter",
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     "is_follow": 0
                     }]"
                     
                     */
                    
                    let followers = CCUser.objectArray(json["result"]) as [CCUser]
                    success(followers)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func listFollowings(_ otherUserId:String?, success:@escaping (([CCUser])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        var params = self.defaultDictionary()
        
        if otherUserId != nil {
            params["other_user_id"] = otherUserId!
        }
        
        //print(params)
        Manager.request(WSEndPoint.listFollowings.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":[{
                     "user_id": 1,
                     "username": "Petter",
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     "is_follow": 0
                     }]"
                     
                     */
                    
                    let followings = CCUser.objectArray(json["result"]) as [CCUser]
                    success(followings)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func otherUserProfile(_ userId:String, success:@escaping ((CCProfile)->()), failure: @escaping (()->()), canDisturbUser:Bool){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        if canDisturbUser {
            SVProgressHUD.show()
        }
        
        let params = ["friend_id":userId] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.otherUserProfile.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: canDisturbUser) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     {
                     "user_id": 1,
                     "username": "David",
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     "followings": 5,
                     "followers": 10,
                     "is_follow": 0,
                     "posts":[{
                     "post_id": 1,
                     "post_image": "http:\/\/www.coloring.com\/images/posts/f6979db8.svg",
                     "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnails/f6979db8.svg",
                     "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnails/f6979db8.svg",
                     "likes": 5,
                     "liked": 1,
                     "comments": 6,
                     }],
                     },
                     */
                    
                    let profile = CCProfile(json: json["result"])
                    success(profile)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func searchUsers(_ key:String, success:@escaping (([CCUser])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["key":key] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.searchUsers.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    let userResult = CCUser.objectArray(json["result"]) as [CCUser]
                    success(userResult)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    //http://coloring.sandbox2.elegant-media.com/api/v1/subscribePackage/
    
    //Subscription
    
    class func subscribeForMonth(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["type":"1"] + self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.subscribePackage.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    if let packageExpireTimeStamp = json["result"]["expire_date"].double {
                        
                        if packageExpireTimeStamp == 0 {
                            UserSession.sharedSession.currentUser!.packageExpirationDate = nil
                        }else{
                            UserSession.sharedSession.currentUser!.packageExpirationDate = Date(timeIntervalSince1970: packageExpireTimeStamp)
                        }
                        
                    }else{
                        UserSession.sharedSession.currentUser!.packageExpirationDate = nil
                    }
                    
                    UserSession.sharedSession.currentUser?.isMonthlyActive = json["result"]["is_monthly_enable"].boolValue
                    UserSession.sharedSession.currentUser?.isYearlyActive = json["result"]["is_yearly_enable"].boolValue
                    
                    UserSession.sharedSession.currentUser?.setAsCurrentUser()
                    
                    success()
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func subscribeForYear(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["type":"2"] + self.defaultDictionary()
        
        //print(params)
        
        Manager.request(WSEndPoint.subscribePackage.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    if let packageExpireTimeStamp = json["result"]["expire_date"].double {
                        
                        if packageExpireTimeStamp == 0 {
                            UserSession.sharedSession.currentUser!.packageExpirationDate = nil
                        }else{
                            UserSession.sharedSession.currentUser!.packageExpirationDate = Date(timeIntervalSince1970: packageExpireTimeStamp)
                        }
                        
                    }else{
                        UserSession.sharedSession.currentUser!.packageExpirationDate = nil
                    }
                    
                    UserSession.sharedSession.currentUser?.isMonthlyActive = json["result"]["is_monthly_enable"].boolValue
                    UserSession.sharedSession.currentUser?.isYearlyActive = json["result"]["is_yearly_enable"].boolValue
                    
                    UserSession.sharedSession.currentUser?.setAsCurrentUser()
                    
                    success()
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    //Notifications
    
    class func unreadCommentNotifications(_ success:@escaping (([CCNotification])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.listUnreadComments.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":[{
                     "notification_id": 1,
                     "description": "Nice post",
                     "user_id": 1,
                     "username": "Petter",
                     "post_image": "http:\/\/www.coloring.com\/images/posts/15246632.jpg",
                     "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnail/15246632.jpg",
                     "post_id": 1,
                     "post_name": "New Image",
                     }]"
                     
                     */
                    
                    let notifications = CCNotification.objectArray(json["result"]) as [CCNotification]
                    success(notifications)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func unreadFollowingNotifications(_ success:@escaping (([CCNotification])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.listUnreadFollowings.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "result":[{
                     "notification_id": 1,
                     "description": "is following you",
                     "user_id": 1,
                     "username": "Petter",
                     "photo": "http:\/\/www.coloring.com\/images/profile/15246632.jpg",
                     }]"
                     */
                    
                    let notifications = CCNotification.objectArray(json["result"]) as [CCNotification]
                    success(notifications)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func unreadLikeNotifications(_ success:@escaping (([CCNotification])->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.listUnreadLikes.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "result":[{
                     "notification_id": 1,
                     "description": "liked your work",
                     "user_id": 1,
                     "username": "Petter",
                     "post_image": "http:\/\/www.coloring.com\/images/posts/15246632.jpg",
                     "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnail/15246632.jpg",
                     "post_id": 1,
                     "post_name": "New Image",
                     }]"
                     */
                    
                    let notifications = CCNotification.objectArray(json["result"]) as [CCNotification]
                    success(notifications)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func notificationCount(_ success:@escaping (((Int,Int,Int))->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.notificationCount.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":{
                     "likes": 1,
                     "comments": 3,
                     "notifications": 1,
                     }
                     
                     comments = 0;
                     likes = 0;
                     notifications = 2;
                     
                     */
                    
                    let (likeCount,commentCount,notificationCount) = (json["result"]["likes"].intValue, json["result"]["comments"].intValue, json["result"]["notifications"].intValue)
                    success(likeCount, commentCount, notificationCount)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func readCommentNotifications(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.readComments.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func readFollowingNotifciations(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.readFollowings.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func readLikeNotifications(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        
        Manager.request(WSEndPoint.readLikes.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    //Users
    
    class func profilePictureUpdate(_ imageData:Data, success:@escaping ((URL?)->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        //print("size of image in MB: %f ", (Double(imageData.length) / 1024.0) / 1024.0)
        
        let params = self.defaultDictionary()
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            
            for (key, value) in params {
                
                
                multipartFormData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)

                
            }
            multipartFormData.append(imageData, withName: "photo", fileName: "profilePicture.png", mimeType: "image/png")

        }, to: WSEndPoint.profilePictureUpdate.urlString() ,  method: .post, headers : nil,  encodingCompletion: { encodingResult in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    SVProgressHUD.dismiss()
                    if let value = response.result.value {
                        if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                            let imageUrl = json["result"]["photo"].URL
                            success(imageUrl)
                        }else{
                            failure()
                        }
                        
                    }else{
                        failure()
                    }
                }
                
            case .failure(let encodingError):
                failure()
            }
        }
        )
        
    }
    
    class func subscriptionExpireDate(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        //        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.subscriptionExpireDate.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "result":{
                     "package_expire_date":"22-01-2017 06:17:00",
                     "is_expired":0,
                     }
                     */
                    
                    //print(json["package_expire_date"].stringValue.date("dd-MM-yyyy HH:mm:ss") ?? NSDate())
                    //print(json["is_monthly_enable"].boolValue)
                    //print(json["is_yearly_enable"].boolValue)
                    
                    if let packageExpireTimeStamp = json["result"]["expire_date"].double {
                        
                        if packageExpireTimeStamp == 0 {
                            UserSession.sharedSession.currentUser!.packageExpirationDate = nil
                        }else{
                            UserSession.sharedSession.currentUser!.packageExpirationDate = Date(timeIntervalSince1970: packageExpireTimeStamp)
                        }
                        
                    }else{
                        UserSession.sharedSession.currentUser!.packageExpirationDate = nil
                    }
                    
                    UserSession.sharedSession.currentUser?.isMonthlyActive = json["result"]["is_monthly_enable"].boolValue
                    UserSession.sharedSession.currentUser?.isYearlyActive = json["result"]["is_yearly_enable"].boolValue
                    
                    UserSession.sharedSession.currentUser?.setAsCurrentUser()
                    
                    success()
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func userForgotPassword(_ email:String, success:@escaping (()->()), failure: @escaping ((String)->())){
        
        if !WebServiceAPI.hasInternet() {
            failure("No Internet Connection.")
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["email":email, "api_key" : Config.apiKey()]
        
        //print(params)
        Manager.request(WSEndPoint.userForgotPassword.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                var errorMessage = "Something went wrong."
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: false) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     Your code goes here
                     */
                    
                    success()
                }else{
                    
                    if let jsonResponse = response.result.value {
                        let json = JSON(jsonResponse)
                        errorMessage = json["error"].stringValue
                        
                    }
                    
                    failure(errorMessage)
                }
                
        }
        
    }
    
    class func userLogin(_ email:String, password:String, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["email":email,"password":password, "device_id": UIDevice.current.identifierForVendor!.uuidString, "device_type" : "iOS", "device_push_token" : UserSession.sharedSession.pushToken,"api_key":Config.apiKey()]
        
        print(params)
        Manager.request(WSEndPoint.userLogin.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: false) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     "result":
                     {
                     "user_id": 1,
                     "username": "John",
                     "email": "john@gmail.com",
                     "token": "BbIUwypFBGtVpDr6kRTQ27uagatriMeE",
                     "package_expire": "21-06-2017 09:30:00",
                     "is_admin": 0,
                     "payload_token":"10e76104dbc867234c0bb420f6979db8"
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     }
                     */
                    
                    //print(json["result"])
                    
                    UserSession.sharedSession.accessToken = json["result"]["token"].stringValue
                    UserSession.sharedSession.currentUser = CCLoggedInUser(json: json["result"])
                    
                    success()
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func userLogout(_ success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.userLogout.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    
                    SVProgressHUD.dismiss()
                    
                    UserSession.sharedSession.invalidateSession()
                    
                    success()
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func userRegistration(_ username:String, email:String, password:String, success:@escaping (()->()), failure: @escaping ((String)->())){
        
        if !WebServiceAPI.hasInternet() {
            failure("No Internet Connection.")
            return
        }
        
        SVProgressHUD.show()
        
        let params = ["username" : username, "email":email,"password":password, "device_id": UIDevice.current.identifierForVendor!.uuidString, "device_type" : "iOS", "api_key" : Config.apiKey(), "device_push_token" : UserSession.sharedSession.pushToken]
        
        //print(params)
        Manager.request(WSEndPoint.userRegistration.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                var errorMessage = "Something went wrong."
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: false) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":
                     {
                     "user_id": 1,
                     "username": "John",
                     "email": "john@gmail.com",
                     "token": "BbIUwypFBGtVpDr6kRTQ27uagatriMeE",
                     "package_expire": "",
                     "is_admin": 0,
                     "payload_token":"10e76104dbc867234c0bb420f6979db8"
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     },
                     "er
                     
                     */
                    
                    //                    UserSession.sharedSession.accessToken = json["result"]["token"].stringValue
                    //                    UserSession.sharedSession.currentUser = CCLoggedInUser(json: json["result"])
                    
                    success()
                    
                }else{
                    
                    if let jsonResponse = response.result.value {
                        let json = JSON(jsonResponse)
                        errorMessage = json["error"].stringValue
                        
                    }
                    
                    failure(errorMessage)
                }
                
        }
        
    }
    
    class func userProfile(_ success:@escaping ((CCProfile)->()), failure: @escaping (()->()), canDisturbUser:Bool){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        if canDisturbUser {
            SVProgressHUD.show()
        }
        
        let params = self.defaultDictionary()
        
        //print(params)
        Manager.request(WSEndPoint.userProfile.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: canDisturbUser) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":
                     {
                     "user_id": 1,
                     "username": "John",
                     "email": "john@gmail.com",
                     "token": "BbIUwypFBGtVpDr6kRTQ27uagatriMeE",
                     "package_expire": "21-01-2017 09:15:00",
                     "is_admin": 0,
                     "payload_token":"10e76104dbc867234c0bb420f6979db8"
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     "posts":[{
                     "post_id": 1,
                     "post_image": "http:\/\/www.coloring.com\/images/posts/f6979db8.svg",
                     "post_thumb_image": "http:\/\/www.coloring.com\/images/posts/thumbnails/f6979db8.svg",
                     "likes": 5,
                     "liked": 1,
                     "comments": 6,
                     }],
                     
                     */
                    
                    let profile = CCProfile(json: json["result"])
                    success(profile)
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    class func userProfileUpdate(_ username:String, email:String, currentPassword:String!, newPassword:String?, success:@escaping (()->()), failure: @escaping (()->())){
        
        if !WebServiceAPI.hasInternet() {
            failure()
            return
        }
        
        SVProgressHUD.show()
        
        var params = self.defaultDictionary()
        
        params["username"] = username
        params["email"] = email
        params["current_password"] = currentPassword
        
        if newPassword != nil {
            params["new_password"] = newPassword!
        }
        
        //print(params)
        Manager.request(WSEndPoint.updateProfile.urlString(), method: .post, parameters: params, encoding: URLEncoding.default, headers: nil)
            .responseJSON { response in
                
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let json = Helper.jsonResponse(response, shouldShowAlert: true) {
                    SVProgressHUD.dismiss()
                    
                    /*
                     
                     "result":{
                     "user_id": 1,
                     "username": "John",
                     "email": "john@gmail.com",
                     "token": "BbIUwypFBGtVpDr6kRTQ27uagatriMeE",
                     "package_expire": "2017-06-21",
                     "is_admin": 0,
                     "payload_token":"10e76104dbc867234c0bb420f6979db8"
                     "photo": "http:\/\/www.coloring.com\/images/profile/profile.jpg",
                     }
                     
                     */
                    
                    UserSession.sharedSession.currentUser = CCLoggedInUser(json: json["result"])
                    success()
                    
                }else{
                    failure()
                }
                
        }
        
    }
    
    //Helper
    
    class func defaultDictionary()->[String:String]{
        return ["api_key":Config.apiKey(), "token" : UserSession.sharedSession.accessToken]
    }
    
    class func hasInternet()->Bool{
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.reachability.isReachable
        
    }
    
}
