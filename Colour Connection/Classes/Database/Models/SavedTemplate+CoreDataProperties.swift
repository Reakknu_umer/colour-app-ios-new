//
//  SavedTemplate+CoreDataProperties.swift
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 2/21/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension SavedTemplate {
    
    @NSManaged var categoryId: NSNumber?
    @NSManaged var templateId: NSNumber?
    @NSManaged var templateName: String?
    @NSManaged var imageUrlString: String?
    @NSManaged var createTime: Date?
    
}
