//
//  SavedTemplate.swift
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 2/21/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//

import Foundation
import CoreData

class SavedTemplate: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    var filePath:String{
        
        get{
            return "\(FileManager.documentsDir())/\(categoryId!)_\(templateId!).svg"
            //alex
           // return "\(FileManager.documentsDir())/\(categoryId!)_\(templateId!).png"
            
        }
        
    }
    
    var originalFilePath:String{
        
        get{
            
        //    return "\(FileManager.documentsDir())/\(categoryId!)_\(templateId!)_original.png"
            //alex
             return "\(FileManager.documentsDir())/\(categoryId!)_\(templateId!)_original.svg"
        }
        
    }
    
    var image:UIImage? {
        
        get {
            
            print("File path: \(filePath)")
            
            if let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) {
                return UIImage(data: data)
            }
            
            return nil
        }
        
    }
    

    
    var originalImage:UIImage? {
        
        get {
            
            print("File path: \(originalFilePath)")
            
            if let data = try? Data(contentsOf: URL(fileURLWithPath: originalFilePath)) {
                return UIImage(data: data)
            }
            
            return nil
        }
        
    }
    
    func isFileExist()-> Bool{
        
        let condition1 = FileManager.default.fileExists(atPath: originalFilePath)
        let condition2 = FileManager.default.fileExists(atPath: originalFilePath)
        
        return (condition1 && condition2)
        
    }
    
    func replaceImage(_ image:UIImage){
        
        let imageData = UIImagePNGRepresentation(image)
        try? imageData?.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        
    }
    
    func prepareForDownload(){
        
        if !isFileExist(){
            return
        }
        
        let fileManager = FileManager.default
        
        do {
            
            try fileManager.removeItem(atPath: originalFilePath)
            try fileManager.removeItem(atPath: filePath)
            
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        
    }
    
    func removeSavedTemplate(){
        
        Database.sharedInstance.managedObjectContext.delete(self)
        Database.sharedInstance.saveContext()
        
    }
    
    func clearTemaplete()->UIImage?{
        
        let data = try? Data(contentsOf: URL(fileURLWithPath: originalFilePath))
        try? data?.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        return image
    }
    
    func discardFiles(){
        
        let fileManager = FileManager()
        
        try? fileManager.removeItem(atPath: filePath)
        try? fileManager.removeItem(atPath: originalFilePath)
        
    }
    
    class func allTemapltes()->[SavedTemplate]{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedTemplate")
        
        do{
            let sectionSortDescriptor = NSSortDescriptor(key: "createTime", ascending: false)
            let sortDescriptors = [sectionSortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            
            let templates = try Database.sharedInstance.managedObjectContext.fetch(fetchRequest) as! [SavedTemplate]
            
            return templates
            
        }catch{
            
            return [SavedTemplate]()
            
        }
        
    }
    
    class func addNewSavedTemplate(_ categoryId:Int, templateId:Int, templateName:String, imageUrl:URL)->SavedTemplate{
        
        if let existingObject = self.template(templateId) {
            return existingObject
        }
        
        let template = NSEntityDescription.insertNewObject(forEntityName: "SavedTemplate", into: Database.sharedInstance.managedObjectContext) as! SavedTemplate
        
        template.categoryId = categoryId as NSNumber?
        template.templateId = templateId as NSNumber?
        template.imageUrlString = imageUrl.absoluteString
        template.templateName = templateName
        template.createTime = Date()
        
        Database.sharedInstance.saveContext()
        
        return template
        
    }
    
    class func addNewSavedTemplate(_ template:CCTemplate)->SavedTemplate{
        
        if let existingObject = self.template(template.templateId) {
            return existingObject
        }
        
        let savedTemplate = NSEntityDescription.insertNewObject(forEntityName: "SavedTemplate", into: Database.sharedInstance.managedObjectContext) as! SavedTemplate
        
        savedTemplate.categoryId = template.categoryId as NSNumber?
        savedTemplate.templateId = template.templateId as NSNumber?
      //  savedTemplate.imageUrlString = template.imageUrl.absoluteString
        //Alex
        savedTemplate.imageUrlString = template.svgUrl.absoluteString
        savedTemplate.templateName = template.templateName
        savedTemplate.createTime = Date()
        
        Database.sharedInstance.saveContext()
        
        return savedTemplate
        
    }
    
    class func template(_ templateId:Int)->SavedTemplate?{
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedTemplate")
        fetchRequest.predicate = NSPredicate(format: "templateId = %d", templateId)
        
        do{
            
            let templates = try Database.sharedInstance.managedObjectContext.fetch(fetchRequest) as! [SavedTemplate]
            return templates.first
            
        }catch{
            
            return nil
            
        }

}
    
    class func isTemplateExist(_ templateId:Int)->Bool{
        
        return (template(templateId) != nil)
        
    }
    
}
