//
//  HelperExtenison.swift
//  ProGlobe
//
//  Created  on 8/1/16.
//  at Eight25Media
//

import UIKit

//Dictionary

func +<Key, Value> (lhs: [Key: Value], rhs: [Key: Value]) -> [Key: Value] {
    var result = lhs
    rhs.forEach{ result[$0] = $1 }
    return result
}

private let minimumHitArea = CGSize(width: 44, height: 44)

extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(top)
            } else if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}

extension UIButton {
    open override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // if the button is hidden/disabled/transparent it can't be hit
        if self.isHidden || !self.isUserInteractionEnabled || self.alpha < 0.01 { return nil }
        
        // increase the hit frame to be at least as big as `minimumHitArea`
        let buttonSize = self.bounds.size
        let widthToAdd = max(minimumHitArea.width - buttonSize.width, 0)
        let heightToAdd = max(minimumHitArea.height - buttonSize.height, 0)
        let largerFrame = self.bounds.insetBy(dx: -widthToAdd / 2, dy: -heightToAdd / 2)
        
        // perform hit test on larger frame
        return (largerFrame.contains(point)) ? self : nil
    }
}

extension String {
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
}

extension UIViewController {
    
    func initWithStoryboardId(){
        
    }
    
}

class PGTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5);
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
}

extension UIColor {
    
    convenience init(hex: Int, alpha: Double = 1.0) {
        self.init(red: CGFloat((hex>>16)&0xFF)/255.0, green:CGFloat((hex>>8)&0xFF)/255.0, blue: CGFloat((hex)&0xFF)/255.0, alpha:  CGFloat(255 * alpha) / 255)
    }
    
}

extension String {
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    mutating func deleteCharactersInRange(_ range: NSRange) {
        let mutableSelf = NSMutableString(string: self)
        mutableSelf.deleteCharacters(in: range)
        self = mutableSelf as String
    }
    
}

extension RangeReplaceableCollection where Iterator.Element : Equatable {
    // Remove first collection element that is equal to the given `object`:
    mutating func removeObject(_ object : Iterator.Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
}

extension Array {
    var last: Element {
        return self[self.endIndex - 1]
    }
}

extension Date{
    
    func timeAgoShort()->String{
        
        let seconds:Int = Int(Date().timeIntervalSince(self))
        
        if (seconds < 5){
            return "just now";
        }else if (seconds < 60){
            return "\(seconds) secs ago";
        }
        else if (seconds < 3600) {
            
            let minutes = seconds/60
            
            if minutes > 1 {
                return "\(minutes) mins ago";
            }else{
                return "1 min ago";
            }
            
        }
        else if (seconds < 86400) {
            
            let hours = seconds/3600
            
            if hours > 1 {
                return "\(hours) hrs ago";
            } else {
                return "1 hr ago";
            }
            
        }
            //2 days and no more
        else if (seconds < 172800) {
            
            let days = seconds/86400
            
            if days > 1 {
                return "\(days) days ago";
            }
            else {
                return "1 day ago";
            }

}
        else{
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM, YYYY"
            return dateFormatter.string(from: self)
        }
        
    }
    
}

extension String {
    
    func date(_ format:String)->Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)
        
    }
    
    func capitalizeFirstLetter()->String{
        let capString = self.substring(from: self.startIndex).capitalized
        return capString
    }
    
    public func rangesOfString(_ searchString:String, options: NSString.CompareOptions = [], searchRange:Range<Index>? = nil ) -> [Range<Index>] {
        if let range = range(of: searchString, options: options, range:searchRange) {
            
            let nextRange = (range.upperBound ..< self.endIndex)
            return [range] + rangesOfString(searchString, searchRange: nextRange)
        } else {
            return []
        }
    }
    
}

extension UITableView {
    
    func hideEmptyCells(){
        self.tableFooterView = UIView()
    }
    
}

extension UILabel {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        //        print(self.font)
        
        if Helper.isIPad() {
            
            let size = Helper.iPadFontSize(font.pointSize)
            self.font = font.withSize(size)
            
        }
        
    }
    
}

extension UIButton {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if Helper.isIPad() {
            
            let size = Helper.iPadFontSize(titleLabel!.font.pointSize)
            self.titleLabel!.font = titleLabel!.font.withSize(size)
            
        }
        
    }
    
}

extension UITextField {
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        if Helper.isIPad() {
            
            let size = Helper.iPadFontSize(font!.pointSize)
            self.font = font!.withSize(size)
            
        }
        
    }
    
    var isEmpty:Bool {
        
        get{
            return (self.text!.characters.count == 0)
        }
        
    }
    
    var isValidEmail:Bool {
        
        get{
            
            let regex = try? NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
            return regex?.firstMatch(in: self.text!, options: [], range: NSMakeRange(0, self.text!.characters.count)) != nil
            
        }
        
    }
    
}

extension FileManager {
    
    class func documentsDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    class func appSupportDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
    class func cachesDir() -> String {
        var paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [String]
        return paths[0]
    }
    
}

extension UIImage {
    
    func resizeToFit(_ frame: CGRect) -> UIImage {
        
        var length:CGFloat = 0
        
        if (frame.size.width < frame.size.height){
            length = frame.size.width
        }else{
            length = frame.size.height
        }
        
        var newSize = CGSize.zero
        
        if (self.size.width < self.size.height) {
            newSize.width = length * UIScreen.main.scale
            newSize.height = newSize.width * (self.size.height / self.size.width)
        }else{
            newSize.height = length * UIScreen.main.scale
            newSize.width = newSize.height * (self.size.width / self.size.height)
        }
        
        UIGraphicsBeginImageContext(newSize)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

extension UIImageView {
    
    func setCCImageWithUrl(_ imageUrl:URL, placeholderImage:UIImage?){
        
        self.image = placeholderImage
        self.sd_setImage(with: imageUrl) { (image, error, cacheType, url) in
            
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
                // do your task
                
                let rImage = image?.resizeToFit(self.bounds)
                
                DispatchQueue.main.async {
                    // update some UI
                    
                    self.image = rImage
                    
                }
            }
            
        }
        
    }
    
    func setCCImage(_ image:UIImage?){
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            // do your task
            
            let rImage = image?.resizeToFit(self.bounds)
            
            DispatchQueue.main.async {
                // update some UI
                
                self.image = rImage
                
            }
        }
        
    }
    
    func showBorder(){
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1.0
    }
    
}
