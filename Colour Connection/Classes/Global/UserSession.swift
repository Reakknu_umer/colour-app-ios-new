//
//  UserSession.swift
//  Colour Connection
//
//  Created  on 1/26/17.
//  at Eight25Media
//

import UIKit

private let _sharedSession = UserSession()

class UserSession {
    
    fileprivate var loggedInUser:CCLoggedInUser!
    
    class var sharedSession: UserSession {
        return _sharedSession
    }

func invalidateSession(){
        DEFAULTS.removeObject(forKey: "TOKEN")
    }
    
    var pushToken:String{
        
        set(newValue){
            
            DEFAULTS.set(newValue, forKey: "PUSHTOKEN")
            DEFAULTS.synchronize()
            
        }
        
        get{
            
            return DEFAULTS.string(forKey: "PUSHTOKEN") ?? ""
            
        }
        
    }
    
    var isValid:Bool{
        
        get{
            return (DEFAULTS.string(forKey: "TOKEN") != nil)
        }
        
    }
    
    var currentUser:CCLoggedInUser?{
        
        get{
            
            //user
            if loggedInUser == nil {
                
                if let userData = DEFAULTS.object(forKey: "CURRENT_USER") as? Data {
                    loggedInUser = NSKeyedUnarchiver.unarchiveObject(with: userData) as? CCLoggedInUser
                }else{
                    return nil
                }
                
            }
            
            return loggedInUser
            
        }
        
        set {
            
            if newValue == nil {
                DEFAULTS.removeObject(forKey: "CURRENT_USER")
            }else{
                let userData = NSKeyedArchiver.archivedData(withRootObject: newValue!)
                DEFAULTS.set(userData, forKey: "CURRENT_USER")
                DEFAULTS.synchronize()
            }
            
            loggedInUser = newValue
            
        }
        
    }
    
    var imageToShare:UIImage?
    
    var accessToken:String{
        
        set(newValue){
            
            DEFAULTS.set(newValue, forKey: "TOKEN")
            DEFAULTS.synchronize()
            
        }
        
        get{
            
            return DEFAULTS.string(forKey: "TOKEN") ?? ""
            
        }
        
    }
    
    var mainViewController:MainViewController?
    var documentController:UIDocumentInteractionController?
    
}
