//
//  Helpers.swift
//  ProGlobe
//
//  Created on 8/1/16.
//	at Eight25Media
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SDWebImage
import Social
import MBAutoGrowingTextView

class Helper: NSObject {
    
    class func localizeDate(_ createdTime:String)->String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        
        if let createdDate = dateFormatter.date(from: createdTime) {
            
            let localFormatter = DateFormatter()
            localFormatter.dateFormat = "dd/MM/yyyy"
            return localFormatter.string(from: createdDate)
            
        }
        
        return createdTime
    }
    
    class func appIcon()->UIImage {
        
        
        
        let dict1 = Bundle.main.infoDictionary?["CFBundleIcons"] as? NSDictionary
        
        let primaryIconsDictionary = dict1?["CFBundlePrimaryIcon"] as? NSDictionary
        let iconFiles = primaryIconsDictionary!["CFBundleIconFiles"] as! NSArray
        // First will be smallest for the device class, last will be the largest for device class
        let lastIcon = iconFiles.lastObject as! NSString
        let icon = UIImage(named: lastIcon as String)
        
        return icon!
    }
    
    class func shareOnFacebook(_ image:UIImage){
        
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
            let facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText("Sample text!")
            facebookSheet.add(image)
            UserSession.sharedSession.mainViewController!.present(facebookSheet, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Facebook", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            UserSession.sharedSession.mainViewController!.present(alert, animated: true, completion: nil)
        }
        
    }
    
    class func shareWithInstagram(_ image:UIImage){
        
        let instagramURL = URL(string: "instagram://app")
        
        if (UIApplication.shared.canOpenURL(instagramURL!)) {
            
            let imageData = UIImagePNGRepresentation(image)
            
            let captionString = "caption"
            
            let writePath = (NSTemporaryDirectory() as NSString).appendingPathComponent("instagram.igo")
            
            if ((try? imageData?.write(to: URL(fileURLWithPath: writePath), options: [.atomic])) != nil) == false {
                
                return
                
            } else {
                let fileURL = URL(fileURLWithPath: writePath)
                
                let controller = UserSession.sharedSession.mainViewController!
                UserSession.sharedSession.documentController = UIDocumentInteractionController(url: fileURL)
                
                //                self.documentController.delegate = self
                
                UserSession.sharedSession.documentController!.uti = "com.instagram.exlusivegram"
                
                UserSession.sharedSession.documentController!.annotation = NSDictionary(object: captionString, forKey: "InstagramCaption" as NSCopying)
                UserSession.sharedSession.documentController!.presentOpenInMenu(from: controller.view.frame, in: controller.view, animated: true)
                
            }
            
        } else {
            
            let alert = UIAlertController(title: "Instagram", message: "Instagram isn't installed.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            UserSession.sharedSession.mainViewController!.present(alert, animated: true, completion: nil)
            
        }
    }
    
    class func universalFonrSizeFor(_ size: CGFloat)->CGFloat{
        
        if isIPad() {
            return iPadFontSize(size)
        }else{
            return size
        }
        
    }
    
    class func iPadFontSize(_ size:CGFloat)->CGFloat{
        
        switch size {
        case 0...10:
            return 18.0
        case 11:
            return 20.0
        case 12...13:
            return 21.0
        case 14:
            return 24.0
        case 15...17:
            return 30.0
        default:
            return 36.0
        }
        
    }
    
    class func isIphone()->Bool{
        
        return (UIDevice.current.userInterfaceIdiom == .phone)
        
    }
    
    class func isIPad()->Bool{
        
        return (UIDevice.current.userInterfaceIdiom == .pad)
        
    }
    
    class func showProfile(_ userId:Int){
        
        DispatchQueue.main.async(execute: {
            
            let isCurrentUser:Bool = (userId == UserSession.sharedSession.currentUser!.userId)
            
            let profileViewController = StoryboardId.ProfileView.instantiateViewController(StoryboardName.Profile) as! ProfileViewController
            profileViewController.otherUserId = String(userId)
            
            profileViewController.profileViewType = isCurrentUser ? ProfileViewType.my : ProfileViewType.other
            
            UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(profileViewController)
            
        });
        
    }
    
    class func showPost(_ post:CCPost)->CommentViewController{
        
        let commentViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
        commentViewController.post = post
        
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentViewController)
        
        return commentViewController
        
    }
    
    class func showSubscriptionView(){
        
        let viewcontroller = StoryboardId.SubscriptionView.instantiateViewController(StoryboardName.Subscription) as! SubscriptionViewController
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(viewcontroller)
        
    }
    
    class func showArtIdeas(_ templateId:Int, templateName:String){
        
        let viewController = StoryboardId.ArtIdeaView.instantiateViewController(StoryboardName.Art) as! ArtIdeaViewController
        viewController.templateId = templateId
        viewController.templateName = templateName
        
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(viewController)
        
    }
    
    class func showArtBoard(_ savedTemaplate:SavedTemplate){
        
        let controller = StoryboardId.ArtBoardView.instantiateViewController(StoryboardName.Art) as! ArtBoardViewController
        controller.savedTemplate = savedTemaplate
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(controller)
        
    }
    
    class func showTestArtBoard(_ template:CCTemplate){
        
        let controller = StoryboardId.ArtBoardView.instantiateViewController(StoryboardName.Art) as! ArtBoardViewController
        controller.testTemplate = template
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(controller)
        
    }
    
    class func locked(_ success:(()->())){
        
        if UserSession.sharedSession.isValid {
            success()
        }else{
            UserSession.sharedSession.mainViewController?.showLogin()
        }
        
    }
    
    class func storyboard(_ storyboardName:StoryboardName)->UIStoryboard{
        
        return UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        
    }
    
    func generateARandomNumber(_ numberOfDigits:Int) -> Int {
        var place = 1
        var finalNumber:Int = 0;
        for i in 0...numberOfDigits {
            //print(i)
            place *= 10
            let randomNumber = Int(arc4random_uniform(10))
            finalNumber += randomNumber * place
        }
        return finalNumber
    }
    
    class func statusCode(_ response: DataResponse<Any>)->Int{
        
        if let httpErrorCode = response.result.error as? SKError {
            //print("status code: \(httpErrorCode.code)")
            return httpErrorCode.code.rawValue
        } else if let responseStatus = response.response { //no errors
            //print("status code: \(responseStatus.statusCode)")
            return responseStatus.statusCode
        }
        
        return -9999
        
    }
    
    class func jsonResponse(_ response: DataResponse<Any>, shouldShowAlert: Bool) -> SwiftyJSON.JSON? {
        
        let code = statusCode(response)
        let json = getJson(response)
        
        switch code {
        case 200:
            
            if json != nil {
                return json
            }else{
                //print("Show appropriate error message here")
            }
            
        case 417:
            let message = "Invalid password."
            if shouldShowAlert {
                Helper.showErrorMessage(message.localized)
            }
            
            break;
            
        case 400..<500:
            
            let message = getResponseMessage(json)
            
            if shouldShowAlert {
                Helper.showErrorMessage(message.localized)
            }
            
        case -1009:
            
            let message = getResponseMessage(json)
            
            if shouldShowAlert {
                Helper.showErrorMessage("no_internet".localized)
            }
            
        default:
            
            if shouldShowAlert {
                Helper.showErrorMessage("unknown_error".localized)
            }
            
            //print("default: handle error message")
        }
        
        //417
        
        return nil
        
    }
    
    class func getJson(_ response: DataResponse<Any>) -> SwiftyJSON.JSON?{
        
        if let jsonResponse = response.result.value {
            //print("JSON: \(jsonResponse)")
            return JSON(jsonResponse)
        }
        
        return nil
    }
    
    class func getResponseMessage(_ json:SwiftyJSON.JSON?)->String{
        return json?["status"]["message"].stringValue ?? "unknown_error"
    }
    
    class func showErrorMessage(_ message:String){
        
        DispatchQueue.main.async(execute: {
            // update some UI
            
            SVProgressHUD.dismiss()
            let rootViewController = UIApplication.topViewController() as? CCBaseViewController
            rootViewController?.showDefaultAlert(message, message: nil)
            
        });
        
    }
    
    
    class func showDefaultAlert(_ title:String?, message:String?){
        
        DispatchQueue.main.async(execute: {
            // update some UI
            
            SVProgressHUD.dismiss()
            let rootViewController = UIApplication.topViewController() as? CCBaseViewController
            rootViewController?.showDefaultAlert("", message: message)
            
        });
        
    }
    
    class func showProgressAlert(_ progress:Float, message:String){
        SVProgressHUD.showProgress(progress, status: message)
    }
    
    class func hideAlert(){
        SVProgressHUD.dismiss()
    }
    
    class func showLoadingAlert(){
        SVProgressHUD.show()
    }
    
    class func showSuccessAlert(_ message:String){
        
        DispatchQueue.main.async(execute: {
            // update some UI
            SVProgressHUD.dismiss()
            let rootViewController = UIApplication.topViewController() as? CCBaseViewController
            rootViewController?.showDefaultAlert("", message: message)
            
        });
        
        //        SVProgressHUD.showSuccessWithStatus(message)
    }
    
    class func dictToJSONString(_ dict:[String:String])->String{
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: [dict], options: JSONSerialization.WritingOptions.prettyPrinted)
            var jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            jsonString = jsonString.replacingOccurrences(of: "\n", with: "")
            
            return jsonString
            
        } catch let error as NSError {
            
            return ""
            
        }
    }
    
    class func dictArrayToJSONString(_ array:[[String:String]])->String{
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: array, options: JSONSerialization.WritingOptions.prettyPrinted)
            var jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
            jsonString = jsonString.replacingOccurrences(of: "\n", with: "")
            
            return jsonString
            
        } catch let error as NSError {
            
            return ""
            
        }
    }
    
}

class ProfileImageView:UIImageView {
    
    var currentUserId:Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileImageView.handleTap(_:))))
    }
    
    func handleTap(_ recognizer:UITapGestureRecognizer){
        
        if currentUserId != nil && UserSession.sharedSession.isValid {
            Helper.showProfile(currentUserId!)
        }
        
    }
    
}

class PostImageView:UIImageView {
    
    var currentPost:CCPost?
    var isTouchEnabled = true
    var commentDelegate:CommentViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ProfileImageView.handleTap(_:))))
    }
    
    func handleTap(_ recognizer:UITapGestureRecognizer){
        
        if currentPost != nil && UserSession.sharedSession.isValid && isTouchEnabled {
            Helper.showPost(currentPost!).delegate = commentDelegate
        }
        
    }
    
}

class CCNavigationView:UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addShadow()
    }
    
    func addShadow(){
        
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 1.0
        self.layer.masksToBounds = false
        
    }
    
}

class CCTabView:UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addShadow()
    }
    
    func addShadow(){
        
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: -0.5)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 1.0
        self.layer.masksToBounds = false
        
    }
    
}

class CCTextField:UITextField {
    
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y,
                          width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = AppColour.Gray.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 5.0
        
    }
    
}

class CCTabButton:UIButton {
    
    override func layoutSubviews() {
        
        let spacing: CGFloat = 3.0
        
        // lower the text and push it left so it appears centered
        //  below the image
        var titleEdgeInsets = UIEdgeInsets.zero
        if let image = self.imageView?.image {
            titleEdgeInsets.top = 8
            titleEdgeInsets.left = -image.size.width
            titleEdgeInsets.bottom = -(image.size.height + spacing)
        }
        self.titleEdgeInsets = titleEdgeInsets;
        
        // raise the image and push it right so it appears centered
        //  above the text
        var imageEdgeInsets = UIEdgeInsets.zero
        if let text: NSString = self.titleLabel?.text as NSString?, let font = self.titleLabel?.font {
            let attributes = [NSFontAttributeName:font]
            let titleSize = text.size(attributes: attributes)
            imageEdgeInsets.top = -(titleSize.height + spacing)
            imageEdgeInsets.right = -titleSize.width
        }
        self.imageEdgeInsets = imageEdgeInsets
        
        super.layoutSubviews()
    }
    
}

class PGProfileTabButton:CCTabButton {
    
    var on:Bool{
        
        set(newValue){
            
            if newValue {
                self.backgroundColor = UIColor(hex: 0x9197a3)
                self.tintColor = UIColor(hex: 0xf8f8f8)
                self.setTitleColor(UIColor(hex: 0xf8f8f8), for: UIControlState())
                self.setTitleColor(UIColor(hex: 0xf8f8f8), for: UIControlState.highlighted)
            }else{
                self.backgroundColor = UIColor(hex: 0xf8f8f8)
                self.tintColor = UIColor(hex: 0x9197a3)
                self.setTitleColor(UIColor(hex: 0x666666), for: UIControlState())
                self.setTitleColor(UIColor(hex: 0x666666), for: UIControlState.highlighted)
            }
            
        }
        
        get{
            return true
        }
        
    }
    
}

class PGChatTabButton:CCTabButton {
    
    var on:Bool{
        
        set(newValue){
            
            if newValue {
                self.backgroundColor = UIColor(hex: 0x9197a3)
                self.tintColor = UIColor(hex: 0xf8f8f8)
                self.setTitleColor(UIColor(hex: 0xf8f8f8), for: UIControlState())
                self.setTitleColor(UIColor(hex: 0xf8f8f8), for: UIControlState.highlighted)
            }else{
                self.backgroundColor = UIColor(hex: 0xf8f8f8)
                self.tintColor = UIColor(hex: 0x9197a3)
                self.setTitleColor(UIColor(hex: 0x666666), for: UIControlState())
                self.setTitleColor(UIColor(hex: 0x666666), for: UIControlState.highlighted)
            }
            
        }
        
        get{
            return true
        }
        
    }
    
}

class PGTheme{
    
    class func helvaticaLTProMediumFont(_ size:CGFloat)->UIFont{
        return UIFont(name: "HelveticaNeueLTPro-Md", size: size)!
    }
    
    class func helvaticaLTProLightFont(_ size:CGFloat)->UIFont{
        return UIFont(name: "HelveticaNeueLTPro-Lt", size: size)!
    }
    
    class func helvaticaLTProRomanFont(_ size:CGFloat)->UIFont{
        return UIFont(name: "HelveticaNeueLTPro-Roman", size: size)!
    }
    
    class func helvaticaLTProBoldOutlineFont(_ size:CGFloat)->UIFont{
        return UIFont(name: "HelveticaNeueLTPro-BdOu", size: size)!
    }
    
}

class CCRoundedImageView: UIImageView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.bounds.width/2.0
        self.clipsToBounds = true
        
    }
    
}

class CCBorderButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        self.borderLeft =
        //        self.borderTop = Border(size: 3, color: UIColor.orangeColor(), offset: UIEdgeInsets(top: 0, left: -10, bottom: 0, right: -5))
        
    }
    
}

class CCCustomTextView: MBAutoGrowingTextView {
    
    open var placeholder: String?
    open var placeholderColor = UIColor.lightGray
    fileprivate var placeholderLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CCCustomTextView.textDidChangeHandler(_:)), name: NSNotification.Name.UITextViewTextDidChange, object: self)
        
        placeholderLabel = UILabel()
        placeholderLabel?.alpha = 0.85
        placeholderLabel?.textColor = placeholderColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        placeholderLabel?.textColor = placeholderColor
        placeholderLabel?.text = placeholder
        
        placeholderLabel?.frame = CGRect(x: 6, y: 4, width: self.bounds.size.width-16, height: 24)
        
        if text.isEmpty {
            addSubview(placeholderLabel!)
            bringSubview(toFront: placeholderLabel!)
        } else {
            placeholderLabel?.removeFromSuperview()
        }
    }
    
    // Whenever the text changes, just trigger a new layout pass.
    func textDidChangeHandler(_ notification: Notification) {
        layoutSubviews()
    }
    
}
