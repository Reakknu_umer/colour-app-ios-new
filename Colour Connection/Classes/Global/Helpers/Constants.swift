//
//  Constants.swift
//  ProGlobe
//
//  Created  on 8/1/16.
//  at Eight25Media
//

import Foundation
import UIKit

typealias CCButtonAction = () -> ()
typealias CCBoolAction = (Bool) -> ()

struct InternalNotification{
    static let UserLoggedIn = "NOTIUserLoggedIn"
}

struct NotificationIcon{
    static let Like = UIImage(named: "notify_like")!
    static let Comment = UIImage(named: "notify_comment")!
    static let Default = UIImage(named: "notification")!
}

struct AppColour{
    
    static let Blue = UIColor(hex: 0x009ee8)
    static let White = UIColor(hex: 0xffffff)
    static let LightGray = UIColor(hex: 0xe5e5e5)
    static let Gray = UIColor(hex: 0xb2b2b2)
    static let DarkGray = UIColor(hex: 0x4c4c4c)
    static let Black = UIColor(hex: 0x000000)
    
}

struct AppFont{
    
    static let Thin = "SFUIDisplay-Thin"
    static let Regular = "SFUIDisplay-Regular"
    static let Bold = "SFUIDisplay-Bold"
    static let Medium = "SFUIDisplay-Medium"
    static let Light = "SFUIDisplay-Light"
    
}

enum StoryboardName:String{
    
    case Explore = "Explore";
    case Login = "Login";
    case DownloadMore = "DownloadMore";
    case Art = "Art";
    case Profile = "Profile";
    case Notification = "Notification";
    case Settings = "Settings";
    case Subscription = "Subscription";
    case Test = "Test";
    
}

enum ProfileUserAction {
    case following
    case followers
    case favourite
    case follow
    case unfollow
}

enum StoryboardId:String{
    
    case NavigationLogin = "NavigationLogin";
    case NavigationMain = "NavigationMain";
    case NavigationExplore = "NavigationExplore";
    case NavigationDownloadMore = "NavigationDownloadMore";
    case NavigationArt = "NavigationArt";
    case NavigationProfile = "NavigationProfile";
    
    case CommentView = "SIDCommentView";
    case ExploreView = "SIDExploreView";
    case MainView = "SIDMainView";
    case LoginView = "SIDLoginView";
    case RegisterView = "SIDRegisterView";
    case ForgotPasswordView = "SIDForgotPasswordView";
    case DownloadMoreView = "SIDDownloadMoreView";
    case PreviewArtworkView = "SIDPreviewArtworkView";
    case ArtBoardView = "SIDArtBoardView";
    case ArtIdeaView = "SIDArtIdeaView";
    case ArtView = "SIDArtView";
    case FollowView = "SIDFollowView";
    case FavouriteView = "SIDFavouriteView";
    case ProfileView = "SIDProfileView";
    case NotificationView = "SIDNotificationView";
    case NotificationInfoView = "SIDNotificationInfoView";
    case SettingsView = "SIDSettingsView";
    case TermsView = "SIDTermsView";
    case AboutUsView = "SIDAboutUsView";
    case SearchFriendsView = "SIDSearchFriendsView";
    case EditProfileView = "SIDEditProfileView";
    case SubscriptionView = "SIDSubscriptionView";
    case AdminTestView = "SIDAdminTestView";
    case ShareView = "ShareView";
    
    func instantiateViewController(_ storyboardName:StoryboardName)->UIViewController{
        let storyboard = UIStoryboard(name: storyboardName.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: self.rawValue)
    }
    
}

enum PGNotifications:String{
    
    case ProfilePictureUpdatedNotification =  "ProfilePictureUpdatedNotification";
    case CoverImageUpdatedNotification =  "CoverImageUpdatedNotification";
    
}

//struct StoryboardID {
//
//
//    struct Epic {
//        
//        static let Connections = "Epic_Connections"
//        
//    }
//    
//    static let CustomViews = "CustomViews"
//    
//}
