//
//  Downloader.swift
//  Colour Connection
//
//  Created  on 2/21/17.
//  at Eight25Media
//

import UIKit

class Downloader: NSObject {
    
    class func load(_ savedTemplate:SavedTemplate, completion:@escaping ((Bool)->())) {
        
        let requestURL = URL(string: savedTemplate.imageUrlString!)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: requestURL)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                
                let statusCode = httpResponse.statusCode
                
                if (statusCode == 200) {
                    
                    if data != nil {
                        
                        savedTemplate.prepareForDownload()
                        try? data!.write(to: URL(fileURLWithPath: savedTemplate.originalFilePath), options: [.atomic])
                        try? data!.write(to: URL(fileURLWithPath: savedTemplate.filePath), options: [.atomic])
                        
                        DispatchQueue.main.async {
                            completion(true)
                        }
                        
                    }else{
                        
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        
                    }
                    
                }else{
                    
                    DispatchQueue.main.async {
                        completion(false)
                    }
                    
                }
                
            }else{
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        })
        dataTask.resume()
        
        
        
    }
    
    class func saveInGallery(_ post:CCPost, completion:@escaping ((Bool)->())){
        
        let requestURL = post.postImage
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: requestURL!)
 
        let session = URLSession.shared
        let dataTask = session.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let httpResponse = response as? HTTPURLResponse {
                
                let statusCode = httpResponse.statusCode
                
                if (statusCode == 200) {
                    
                    if data != nil {
                        let image = UIImage(data: data!)
                        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
                        completion(true)
                    }
                    
                    completion(false)
                    
                }else{
                    
                    completion(true)
                    
                }
                
            }
        })
        dataTask.resume()
        
    }
}
