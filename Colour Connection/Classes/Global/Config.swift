//
//  Config.swift
//  ProGlobe
//
//  Created  on 8/1/16.
//  at Eight25Media
//

import UIKit

let DEFAULTS = UserDefaults.standard

class Config: NSObject {
    
    fileprivate static let plistFilePath:String! = Bundle.main.path(forResource: "Config", ofType: "plist")
    fileprivate static let AppConfig:NSDictionary! = NSDictionary(contentsOfFile: plistFilePath)
    
    static let uploadImageMaxSize = CGSize(width: 800, height: 800)
    
    fileprivate class func stringValueForAppConfigKey(_ key:String)->String{
        return (AppConfig.object(forKey: key) as? String) ?? ""
    }
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    //MARK: Computed Properties    
    //MARK: Initializers
    //MARK: Segue
    
    class func baseUrl()->String{
        //return stringValueForAppConfigKey("DevelopmentBaseUrl")
        //return stringValueForAppConfigKey("ProductionBaseUrl")
         return stringValueForAppConfigKey("Dev_CF_BaseUrl")
        //Dev_CF_BaseUrl
    }
    
    class func apiKey()->String{
        return stringValueForAppConfigKey("API_KEY")
    }
    
    class var appStoreId:String{
        return stringValueForAppConfigKey("AppStoreID")
    }
    
    class var googleAdUnitId:String{
        return stringValueForAppConfigKey("AdUnitID")
    }
    
    class var googleAdMobId:String{
        return stringValueForAppConfigKey("AdMobId")
    }
    
    class func channelImagePath()->String{
        return stringValueForAppConfigKey("ChannelImagePath")
    }
    
    class func channelImageUrl(_ channelName:String)->URL{
        
        let name = (channelName as NSString).replacingOccurrences(of: " ", with: "")
        let urlString = "\(channelImagePath())\(name.lowercased()).png"
        return URL(string: urlString)!
        
    }
    
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
