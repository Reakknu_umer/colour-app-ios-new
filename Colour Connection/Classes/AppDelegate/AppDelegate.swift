//
//  AppDelegate.swift
//  Colour Connection
//
//  Created by Thilina Chamin Hewagama on 1/20/17.
//  Copyright © 2017 ElegantMedia. All rights reserved.
//

import SVProgressHUD
import UIKit
import GoogleMobileAds
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var noInternetAlertView:UIView?
    var window: UIWindow?
    var shareController:ShareViewController?
    let reachability = Reachability()!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        registerForPushNotifications(application)
        
        if launchOptions != nil {
            if let remoteNotification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as! [AnyHashable: Any]? {
                handlePush(remoteNotification, launched: true)
            }
        }

        loadInApp()
        registerForAdMob()
        Fabric.with([Crashlytics.self])

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)

        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        //        SVProgressHUD.setMaxSupportedWindowLevel(UIWindowLevelStatusBar + 4)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.reachabilityChanged(_:)), name: .reachabilityChanged, object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            //print("could not start reachability notifier")
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func showExploreAfterLogoutScreen(){
        
        DispatchQueue.main.async { 
            
            let controller = StoryboardId.NavigationMain.instantiateViewController(StoryboardName.Explore)
            self.window?.rootViewController = controller
            
        }
        
    }
    
    func showShareController(_ image:UIImage){
        
        if self.shareController == nil {
            
            self.shareController = StoryboardId.ShareView.instantiateViewController(StoryboardName.Art) as? ShareViewController
            
        }
        
        UserSession.sharedSession.imageToShare = image
        self.shareController?.showTransparentView(window!)
        
    }
    
    func handlePush(_ userInfo: [AnyHashable: Any], launched:Bool){
        
        //validate logged users
        if !UserSession.sharedSession.isValid {
            return
        }
        
        //get message
        
        var message = ""
        
        if let aps = userInfo["aps"] as? NSDictionary {
            
            if let alert = aps["alert"] as? String {
                
                message = alert
                
            }
            
        }
        
        let application = UIApplication.shared
        let isActive = (application.applicationState == .active)
        let isBackground = (application.applicationState == .background)
        let isInactive = (application.applicationState == .inactive)
        
        
        let notificationType:String = (userInfo["notification_type"] as? String) ?? ""
        let actionId:String = (userInfo["action_id"] as? String) ?? ""
        
        if launched {
            UserDefaults.standard.set(true, forKey: "HAS_PENDING_NOTIFICATIONS")
        } else {
            if isActive {
                let inNotificationScreen = UserSession.sharedSession.mainViewController?.isInNotificationScreen() ?? false
                
                if inNotificationScreen {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "NEW_NOTIFICATION"), object: nil)
                } else {
                    showInternalMessage(message, type: notificationType, actionId: actionId)
                }
                
            } else if isBackground || isInactive {
                
                let inNotificationScreen = UserSession.sharedSession.mainViewController?.isInNotificationScreen() ?? false
                
                if inNotificationScreen {
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "NEW_NOTIFICATION"), object: nil)
                } else {
                    UserSession.sharedSession.mainViewController?.showNotificationScreen()
                }
            }
        }
    }
    
    //Mark: Notification
    
    //register for PushNotifications
    
    func registerForPushNotifications(_ application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(
            types: [.badge, .sound, .alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        application.registerForRemoteNotifications()
    }
    
    
    //MARK: Register for PushNotifications
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        UserSession.sharedSession.pushToken = self.deviceToken(deviceToken)
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        UserSession.sharedSession.pushToken = ""
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        handlePush(userInfo, launched: false)
    }
    
    //MARK: Helper
    
    func deviceToken(_ data:Data)->String{
        
        let tokenChars = (data as NSData).bytes.bindMemory(to: CChar.self, capacity: data.count)
        var tokenString = ""
        
        for i in 0..<data.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        //print("Device Token:", tokenString)
        
        return tokenString
        
    }
    
    func registerForAdMob(){
        
        // Initialize the Google Mobile Ads SDK.
        GADMobileAds.configure(withApplicationID: Config.googleAdMobId)
        
        //Add Unit Id: ca-app-pub-3903051467009929/9321168094
        
    }
    
    //    func showInternalMessageAlert(application: UIApplication, userInfo: [NSObject : AnyObject])
    //    {
    //        
    //        var message = ""
    //        
    //        if let aps = userInfo["aps"] as? NSDictionary {
    //            
    //            if let alert = aps["alert"] as? NSDictionary {
    //                
    //                if let args = alert["loc-args"] as? NSArray {
    //                    
    //                    message = (args.lastObject as? String) ?? ""
    //                    
    //                }
    //                
    //            }
    //            
    //        }
    //        
    //        let isActive = (application.applicationState == .Active)
    //        let isBackground = (application.applicationState == .Background)
    //        let isInactive = (application.applicationState == .Inactive)
    //        
    //        if isActive  { //&& (!inChatScreen) && (!insideSameConversation)
    //            
    //            //            let username = ConnectionHandler.cleanUserId(fromUri ?? "")
    //            //            let group = isGroupMessage ? (toUri ?? "") : nil
    //            
    //            //            Helper.showNewMessageGlobally(username, message: message, group: group)
    //            
    //            //update bubble count
    //            //            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    //            //            appDelegate.mainViewController?.chatNotificationBubble?.increment()
    //            //            UIApplication.sharedApplication().applicationIconBadgeNumber = Int(appDelegate.mainViewController?.secretaryNotificationBubble?.count ?? 0) + Int(appDelegate.mainViewController?.requestNotificationBubble?.count ?? 0) + Int(appDelegate.mainViewController?.chatNotificationBubble?.count ?? 0)
    //            
    //        } else if isInactive  { //&& (!inChatScreen) && (!insideSameConversation)
    //            
    //            //            let username = ConnectionHandler.cleanUserId(fromUri ?? "")
    //            //            let group = isGroupMessage ? (toUri ?? "") : nil
    //            
    //            //            Helper.handleNotificationWhenAppIsInactive(username, group: group)
    //            
    //        }
    //        
    //        
    //    }
    //  
    
    func showInternalMessage(_ alert:String, type:String, actionId:String){
        
        DispatchQueue.main.async {
            
            let view = CMNavBarNotificationView.notify(withText: "Notification", detail: alert, image: Helper.appIcon(), duration: 2.0, andTouch: { (object) in
                
                DispatchQueue.main.async {
                    
                    UserSession.sharedSession.mainViewController?.showNotificationScreen()
                    
                    //                    if type == "LIKE" || type == "COMMENT" {
                    //                        
                    //                    }else if type == "FOLLOW" {
                    //                        
                    //                    }
                    
                }
                
            })
            
            view?.backgroundColor = UIColor.white
            
        }
        
    }
    
    func loadInApp(){
        
        /*
         
         if(![IAPShare sharedHelper].iap) {
         
         NSSet* dataSet = [[NSSet alloc] initWithObjects:@"com.comquas.iap.test", nil];
         
         [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
         
         }
         
         */
        
        if (IAPShare.sharedHelper().iap == nil) {
            
            let dataSet = NSSet(objects: "au.com.elegantmedia.colourconnection.tc1", "au.com.elegantmedia.colourconnection.tc2")
            IAPShare.sharedHelper().iap = IAPHelper(productIdentifiers: dataSet as! Set<NSObject>)
            
        }
        
        IAPShare.sharedHelper().iap.production = true
        
        getProducts()
        
    }
    
    func getProducts(){
        
        /*
         
         [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest* request,SKProductsResponse* response)
         {
         
         }];
         
         */
        
        IAPShare.sharedHelper().iap.requestProducts { (request, response) in
            
            //print("Completed requesting products.")
            
            for product in IAPShare.sharedHelper().iap.products {
                
                //print("Price: \(IAPShare.sharedHelper().iap .getLocalePrice((product as! SKProduct)))")
                //print("Title: \((product as! SKProduct).localizedTitle)")
                
            }
            
        }
        
        /*
         
         SKProduct* product =[[IAPShare sharedHelper].iap.products objectAtIndex:0];
         
         NSLog(@"Price: %@",[[IAPShare sharedHelper].iap getLocalePrice:product]);
         NSLog(@"Title: %@",product.localizedTitle);
         
         */
        
        
        
    }
    
    func showNoInternetAlert(){
        
        if noInternetAlertView == nil {
            
            var alertFrame = UIScreen.main.bounds
            alertFrame.size.height = 20
            alertFrame.origin.y = -25
            
            var nib = Bundle.main.loadNibNamed("CCAlertView", owner: self, options: nil)
            noInternetAlertView = nib?[0] as! CCAlertView
            noInternetAlertView?.frame = alertFrame
            
            
            //            noInternetAlertView = CCAlertView(frame: alertFrame)
            noInternetAlertView?.layoutSubviews()
        }
        
        DispatchQueue.main.async(execute: {
            // update some UI
            
            self.window?.addSubview(self.noInternetAlertView!)
            self.noInternetAlertView?.window?.windowLevel = UIWindowLevelStatusBar + 1
            self.noInternetAlertView?.frame.origin.y = 0
            self.noInternetAlertView?.setNeedsLayout()
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
                self.noInternetAlertView?.layoutIfNeeded()
                }, completion: {finished in
                    
            })
            
            
        });
        
    }
    
    func hideNoInternetAlert(){
        
        
        self.noInternetAlertView?.frame.origin.y = -25
        self.noInternetAlertView?.setNeedsLayout()
        
        DispatchQueue.main.async(execute: {
            // update some UI
            
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
                self.noInternetAlertView?.layoutIfNeeded()
                }, completion: {finished in
                    self.noInternetAlertView?.window?.windowLevel = UIWindowLevelStatusBar - 1
                    self.noInternetAlertView?.removeFromSuperview()
            })
            
        });
        
    }
    
    func reachabilityChanged(_ note: Notification) {
        
        let reachability = note.object as! Reachability
        
        if reachability.isReachable {
            
            hideNoInternetAlert()
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
            
            
        } else {
            
            showNoInternetAlert()
            
            //print("Network not reachable")
        }
    }
    
}

class CCAlertView:UIView{
    
}

/*
 
 APNS Payload Pattern
 ------------------
 {
 "aps" : {
 "alert" : "Notification from Colour Connection",
 "badge" : 0,
 "sound" : "default"
 },
 "notification_type" : "LIKE",
 "action_id" : 1
 }
 
 Like Notification
 -----------------
 
 "notification_type" : "LIKE",
 "action_id" : 1
 
 
 Comment Notification
 -----------------
 
 "notification_type" : "COMMENT",
 "action_id" : 1
 
 
 Default Notification
 -----------------
 
 "notification_type" : "FOLLOW",
 "action_id" : 1
 
 
 
 */
