//
//  EditProfileViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD
import MobileCoreServices

class EditProfileViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var profilePictureOverlayButton:UIButton!
    @IBOutlet weak var usernameTextField:UITextField!
    @IBOutlet weak var emailTextField:UITextField!
    @IBOutlet weak var currentPasswordTextField:UITextField!
    @IBOutlet weak var newPasswordTextField:UITextField!
    @IBOutlet weak var retypeNewPasswordTextField:UITextField!
    @IBOutlet weak var profilePictureImageView:UIImageView!
    @IBOutlet weak var saveButton:UIButton!
    
    //MARK: Properties 
   
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.alpha = 0.5
        saveButton.isEnabled = false
        
        profilePictureImageView.layer.cornerRadius = Helper.isIPad() ? 80 : 44
        profilePictureImageView.clipsToBounds = true
        
        usernameTextField.addTarget(self, action: #selector(EditProfileViewController.textChanged), for: .editingChanged)
        emailTextField.addTarget(self, action: #selector(EditProfileViewController.textChanged), for: .editingChanged)
        currentPasswordTextField.addTarget(self, action: #selector(EditProfileViewController.textChanged), for: .editingChanged)
        newPasswordTextField.addTarget(self, action: #selector(EditProfileViewController.textChanged), for: .editingChanged)
        retypeNewPasswordTextField.addTarget(self, action: #selector(EditProfileViewController.textChanged), for: .editingChanged)
        
        loadInfo()
        
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func profilePictureButtonAction(_ sender: AnyObject) {
        
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Take Photo", "Choose from Gallery")
        
        
        if Helper.isIPad() {
            actionSheet.show(from: self.profilePictureOverlayButton.frame, in: self.view, animated: true)
        }else{
            actionSheet.show(in: self.view)
        }
        
    }
    
    @IBAction func saveButtonAction(_ sender: AnyObject) {
        
        if !validateForm() {
            return
        }
        
        let usernameModifield = UserSession.sharedSession.currentUser!.username != usernameTextField.text!
        let emailModifield = UserSession.sharedSession.currentUser!.email != emailTextField.text!
        
        var currentPassword = currentPasswordTextField.text!
        
        var newPassword:String?
        var retypeNewPassword:String?
        
        let hasNewPassword = !newPasswordTextField.isEmpty
        
        if hasNewPassword {
            
            currentPassword = currentPasswordTextField.text!
            newPassword = newPasswordTextField.text!
            retypeNewPassword = retypeNewPasswordTextField.text!
            
        }
        
        let hasModification = usernameModifield || emailModifield || (newPassword != nil) || (retypeNewPassword != nil)
        
        if hasModification {
            
            WebServiceAPI.userProfileUpdate(usernameTextField.text!, email: emailTextField.text!, currentPassword: currentPassword, newPassword: newPassword, success: {
                
                DispatchQueue.main.async(execute: {
                    // update some UI
                    self.showDefaultAlert("Alert", message: "Profile updated successfully.")
                });
                
                }, failure: {
                  
            })
            
        }
        
        
    }
    
    //MARK: - Private Functions 
    
    func pickImage(_ fromCamera:Bool){
        
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.sourceType = (fromCamera) ? .camera : .photoLibrary;
        picker.mediaTypes = [kUTTypeImage as String];
        picker.allowsEditing = false
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
           
            picker.modalPresentationStyle = UIModalPresentationStyle.popover
            picker.popoverPresentationController?.permittedArrowDirections = .up
            picker.popoverPresentationController?.sourceView = self.view
            picker.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: 250, width: 1, height: 1)

            
            DispatchQueue.main.async(execute: {
                self.present(picker, animated: true, completion: nil)
            });

           
        } else  {
           self.present(picker, animated: true, completion: nil)
        }
        
    }
    
    
    func validateForm()->Bool {
        
        let invalidCharacterSet:NSMutableCharacterSet = NSMutableCharacterSet(charactersIn: " ")
        invalidCharacterSet.formUnion(with: CharacterSet.symbols)
        
        if usernameTextField.isEmpty {
            self.showDefaultAlert(nil, message: "Username cannot be empty.")
            return false
        }else if usernameTextField.text!.rangeOfCharacter(from: invalidCharacterSet as CharacterSet) != nil {
            self.showDefaultAlert(nil, message: "Invalid Username.")
            return false
        }else if emailTextField.isEmpty {
            self.showDefaultAlert(nil, message: "Email address cannot be empty.")
            return false
        }else if emailTextField.isValidEmail == false {
            self.showDefaultAlert(nil, message: "Invalid email address.")
            return false
        }else if currentPasswordTextField.isEmpty {
            self.showDefaultAlert(nil, message: "Password cannot be empty.")
            return false
        }else if newPasswordTextField.isEmpty == false {
            
            if newPasswordTextField.text!.characters.count < 6{
                self.showDefaultAlert(nil, message: "New password must be at least 6 characters.")
                return false
            }else if (newPasswordTextField.text! != retypeNewPasswordTextField.text! ) {
                self.showDefaultAlert(nil, message: "New passwords do not match.")
                return false
            }
            
        }else if retypeNewPasswordTextField.isEmpty == false {
            
            if newPasswordTextField.isEmpty {
                self.showDefaultAlert(nil, message: "Choose a new password first.")
            }
            
        }
        
        return true
        
    }
    
    func loadInfo(){
        
        if !UserSession.sharedSession.isValid {
            return
        }
        
        profilePictureImageView.setImageWith(UserSession.sharedSession.currentUser!.userProfilePicture, placeholderImage: UIImage(named:"placeholder"))
        usernameTextField.text = UserSession.sharedSession.currentUser?.username
        emailTextField.text = UserSession.sharedSession.currentUser?.email
        
        
    }
    
    func uploadImage(_ image:UIImage){
        
        DispatchQueue.main.async{
            SVProgressHUD.show(withStatus: "Uploading profile picture!")
        }
        
        let resizedImage = resizeImage(image, newWidth: 400.0)
        
        let imageData = UIImageJPEGRepresentation(resizedImage, 0.5)!
        //        let strBase64:String = imageData.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        
        WebServiceAPI.profilePictureUpdate(imageData, success: { (imageUrl) in
            
            //            SVProgressHUD.showSuccessWithStatus("Uploaded!")
            
            SVProgressHUD.dismiss()
            self.showDefaultAlert(nil, message: "Profile picture uploaded.")
            
            UserSession.sharedSession.currentUser!.userProfilePicture = imageUrl ?? URL(string : "")
            self.loadInfo()
            
            }, failure: {
                
                SVProgressHUD.dismiss()
                
                if !WebServiceAPI.hasInternet() {
                    self.showDefaultAlert(nil, message: "No Internet Connection.")
                }else{
                    self.showDefaultAlert(nil, message: "Profile picture uploading failed.")
                }
                
        })
        
    }
    
    func scaleImage(_ image: UIImage, toSize newSize: CGSize) -> (UIImage) {
        let newRect = CGRect(x: 0,y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        context!.interpolationQuality = .high
        let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
        context?.concatenate(flipVertical)
        context?.draw(image.cgImage!, in: newRect)
        let newImage = UIImage(cgImage: (context?.makeImage()!)!)
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func resizeImage(_ image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    //MARK: Helpers 
    
    func textChanged(){
        
        let usernameModifield = UserSession.sharedSession.currentUser!.username != usernameTextField.text!
        let emailModifield = UserSession.sharedSession.currentUser!.email != emailTextField.text!
        
        if usernameModifield || emailModifield || !newPasswordTextField.isEmpty {
            saveButton.isEnabled = true
            saveButton.alpha = 1.0
        }else{
            saveButton.isEnabled = false
            saveButton.alpha = 0.5
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        
        //        profilePictureImageView.image = selectedImage
        
        picker.dismiss(animated: true) {
            
            DispatchQueue.main.async
            {
                self.uploadImage(image)
            }
            
        }
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true) {
            
        }
        
    }
    
    
}

extension EditProfileViewController: UIActionSheetDelegate {
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        
        //print("\(buttonIndex)")
        
        switch (buttonIndex){
            
        case 0:
            //print("Cancel")
            break
        case 1:
            //print("Take Photo")
            pickImage(true)
            break
        case 2:
            //print("Choose from Gallery")
            pickImage(false)
            break
            
        default:
            break
        }
    }
    
}

