//
//  FollowersViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

enum FollowType {
    case following
    case follower
}

class FollowViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var navigationTitleLabel:UILabel!
    @IBOutlet weak var tableview:UITableView!
    
    //MARK: Properties 
    
    var otherUserId:String?
    var followType = FollowType.following
    
    var users = [CCUser]() {
        
        didSet{
            
            if isViewLoaded {
                tableview.reloadData()
            }
            
        }
        
    }
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.hideEmptyCells()
        
        loadData()
        addRefrshController()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func loadData(){
        
        if followType == .following {
            
            navigationTitleLabel.text = "Following"
            
            WebServiceAPI.listFollowings(otherUserId, success: { (users) in
                
                self.users = users
                
                }, failure: {
                    
            })
            
        }else{
            
            navigationTitleLabel.text = "Followers"
            
            WebServiceAPI.listFollowers(otherUserId, success: { (users) in
                
                self.users = users
                
                }, failure: {
                    
            })
            
        }
        
    }
    
    //MARK: Helpers 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableview.addSubview(refreshControl)
        tableview.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension FollowViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let user = users[indexPath.row]
        
        let cell = cell as! FollowCell
        cell.updateVariableContent(user)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = users[indexPath.row]
        Helper.showProfile(user.userId)
        
    }
    
}

extension FollowViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowCell") as! FollowCell
        
        let user = users[indexPath.row]
        
        cell.configureCell(user) { (follow) in
            
            if follow {
                
                user.isFollowing = false
                self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                
                WebServiceAPI.unfollowUser(user.userId, success: {
                    
                    
                    
                    }, failure: {
                        
                })
                
            }else{
                
                user.isFollowing = true
                self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                
                WebServiceAPI.followUser(user.userId, success: {
                    
                    }, failure: {
                        
                })
                
            }
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = Helper.isIphone() ? 68.0 : 114.0
        return CGFloat(height)
    }
}
