//
//  ArtIdeaViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class ArtIdeaViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var placeholderView:UIView!
    
    //MARK: Properties 
    
    var templateId:Int?
    var templateName:String?
    
    var posts = [CCPost](){
        
        didSet{
            
            if self.isViewLoaded {
                
                self.titleLabel.text = templateName
                collectionView.reloadData()
                
            }
            
            
            
        }
        
    }
    
    //MARK: Computed Properties
    
    var cellSize:CGSize {
        
        get{
            
            let width = (collectionView.frame.width / 2.0) - 12
            
            let padding:CGFloat = Helper.isIphone() ? 34.0 : 52.0
            
            return CGSize(width: width, height: (width + padding))
            
            //            let width = (collectionView.frame.width / 2.0) - 12
            //            return CGSizeMake(width, width)
            
        }
        
    }
    
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placeholderView.isHidden = true
        collectionView.dataSource = nil
        collectionView.delegate = nil
        
        self.titleLabel.text = ""
        loadArtIdeas()
        addRefrshController()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func loadArtIdeas(){
        
        
        
        WebServiceAPI.listArtIdeas(templateId!, success: { (posts) in
            
            DispatchQueue.main.async(execute: {
                // update some UI
                self.collectionView.dataSource = self
                self.collectionView.delegate = self
                self.posts = posts
                self.collectionView.reloadData()
            });
            
            }, failure: {
                
        })
        
    }
    
    //MARK: Helpers 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadArtIdeas()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ArtIdeaViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        self.placeholderView.isHidden = !self.posts.isEmpty
        
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let post = posts[indexPath.row]
        
        //let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ArtIdeaCell", forIndexPath: indexPath) as! ArtIdeaCell
        
        //cell.configureCell(post)
        
        //return cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMiniCell", for: indexPath) as! FeedMiniCell
        cell.mainImageView.commentDelegate = self
        
        cell.configureCell(post) { (action) in
            
            Helper.locked({
                
                if action == .like {
                    
                    if !post.isLiked {
                        
                        post.isLiked = true
                        post.likeCount = post.likeCount + 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.likePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }else{
                        
                        post.isLiked = false
                        post.likeCount = post.likeCount - 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.unlikePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }
                    
                }else{
                    
                    let commentsViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
                    commentsViewController.post = post
                    commentsViewController.delegate = self
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentsViewController)
                    
                }
                
            })
            
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let post = posts[indexPath.row]
        Helper.showPost(post).delegate = self
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let post = posts[indexPath.row]
        let cell = cell as! FeedMiniCell
        cell.updateVariableContent(post)
        
    }
    
}

extension ArtIdeaViewController: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        return cellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8,8,8,8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 8
    }
    
}

extension ArtIdeaViewController: CommentViewDelegate {
    
    func deleted(_ post: CCPost) {
        self.posts.removeObject(post)
        self.collectionView.reloadData()
    }
    
}
