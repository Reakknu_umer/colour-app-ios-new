//
//  ExploreViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class ExploreViewController: CCBaseViewController {
    
    //MARK: Enums
    
    enum ExploreSection {
        case inspire
        case newExplore
        case timeLine
    }
    
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var inspireButton:UIButton!
    @IBOutlet weak var newButton:UIButton!
    @IBOutlet weak var timelineButton:UIButton!
    
    @IBOutlet weak var cyanLineLeadingConstraint:NSLayoutConstraint!
    @IBOutlet weak var loginRequiredView:UIView!
    
    //MARK: Properties
    
    fileprivate var inspireCurrentPage = 1
    fileprivate var exploreCurrentPage = 1
    fileprivate var timelineCurrentPage = 1
    
    //    var currentPage : Int {
    //        
    //        
    //        get{
    //            
    //            if currentSection == .Inspire {
    //                return inspireCurrentPage
    //            }else if currentSection == .NewExplore {
    //                return exploreCurrentPage
    //            }else {
    //                return timelineCurrentPage
    //            }
    //            
    //        }
    //        
    //        set{
    //            
    //            if currentSection == .Inspire {
    //                inspireCurrentPage = newValue
    //            }else if currentSection == .NewExplore {
    //                exploreCurrentPage = newValue
    //            }else {
    //                timelineCurrentPage = newValue
    //            }
    //            
    //        }
    //        
    //    }
    
    var previousSectionButton:UIButton?
    var currentSection:ExploreSection?
    
    var posts = [CCPost]() {
        
        didSet{
            //print("There are \(posts.count) posts.")
        }
        
    }
    
    var feedMiniCellSize:CGSize {
        
        get{
            
            let width = (collectionView.frame.width / 2.0) - 12
            
            let padding:CGFloat = Helper.isIphone() ? 34.0 : 52.0
            
            return CGSize(width: width, height: (width + padding))
            
        }
        
    }
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginRequiredView.isHidden = true
        
        tableview.hideEmptyCells()
        
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 409
        
        selectExploreSection(.inspire)
        
        // Do any additional setup after loading the view.
        
        refreshAfterLogin { 
            //print("Refresh :-)")
            
        }
        
        addRefrshController()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (currentSection == .timeLine) && UserSession.sharedSession.isValid && self.posts.isEmpty{
            
            self.loginRequiredView.isHidden = true
            loadTimeline(1)
            
        }else{
            self.refreshVisibleCells()
        }
        
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func inspireButtonAction(_ button:UIButton) {
        
        selectExploreSection(.inspire)
        
    }
    
    @IBAction func newButtonAction(_ button:UIButton) {
        
        
        selectExploreSection(.newExplore)
        
    }
    
    @IBAction func timelineButtonAction(_ button:UIButton) {
        
        
        selectExploreSection(.timeLine)
        
    }
    
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        
        UserSession.sharedSession.mainViewController?.showLogin()
        
    }
    
    //MARK: - Private Functions 
    
    func loadInspire(_ pageIndex:Int){
        
        WebServiceAPI.exploreInspire(pageIndex, success: { (posts, page) in
            
            if page == 0 {
                return
            }
            
            self.inspireCurrentPage = page
            
            if pageIndex == 1 {
                self.posts = posts
            }else{
                self.posts = CCPost.merge(posts, existingPosts: self.posts)
            }
            
            self.tableview.reloadData()
            
            }, failure: {
                
        })
        
    }
    
    func loadNewExplore(_ pageIndex:Int){
        
        WebServiceAPI.exploreNewPosts(pageIndex, success: { (posts, page) in
            
            if page == 0 {
                return
            }
            
            self.exploreCurrentPage = page
            
            if pageIndex == 1 {
                self.posts = posts
            }else{
                self.posts = CCPost.merge(posts, existingPosts: self.posts)
            }
            
            self.collectionView.reloadData()
            
            }, failure: {
                
        })
        
    }
    
    func loadTimeline(_ pageIndex:Int){
        guard pageIndex < -1 else {
            return
        }
        //saro
        WebServiceAPI.exploreTimeline(pageIndex, success: { (posts, page) in
            
            if page == 0 {
                return
            }
            
            self.timelineCurrentPage = page
            
            if pageIndex == 1 {
                self.posts = posts
            }else{
                self.posts = CCPost.merge(posts, existingPosts: self.posts)
            }
            
            self.tableview.reloadData()
            
            }, failure: {
                
        })
        
    }
    
    func selectExploreSection(_ section:ExploreSection){
        
        if currentSection == section {
            return
        }
        
        loginRequiredView.isHidden = true
        
        switch section {
            
        case .inspire:
            tableview.isHidden = false
            collectionView.isHidden = true
            cyanLineLeadingConstraint.constant = 10
            applyOnButtonStype(inspireButton)
            loadInspire(1)
            break
        case .newExplore:
            tableview.isHidden = true
            collectionView.isHidden = false
            cyanLineLeadingConstraint.constant = inspireButton.frame.width + 10
            applyOnButtonStype(newButton)
            loadNewExplore(1)
            break
        case .timeLine:
            
            tableview.isHidden = false
            collectionView.isHidden = true
            cyanLineLeadingConstraint.constant = inspireButton.frame.width * 2.0 + 10
            applyOnButtonStype(timelineButton)
            
            if UserSession.sharedSession.isValid {
                loadTimeline(1)
            }else{
                self.posts = [CCPost]()
                self.tableview.reloadData()
                loginRequiredView.isHidden = false
            }
            
            break
        }
        
        currentSection = section
        animateButtonIndicatorView()
        
    }
    
    //MARK: Helpers
    
    func addRefrshController(){
        
        let refreshControlT = UIRefreshControl()
        refreshControlT.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableview.addSubview(refreshControlT)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        
        switch currentSection! {
            
        case .inspire:
            loadInspire(1)
            break
        case .newExplore:
            loadNewExplore(1)
            break
        case .timeLine:
            loadTimeline(1)
            break
        }
        
        
    }
    
    func applyOnButtonStype(_ button:UIButton){
        
        if previousSectionButton == button {
            return
        }
        
        if previousSectionButton != nil {
            previousSectionButton?.setTitleColor(AppColour.DarkGray, for: UIControlState())
            previousSectionButton?.setTitleColor(AppColour.Blue, for: .highlighted)
        }
        
        button.setTitleColor(AppColour.DarkGray, for: .highlighted)
        button.setTitleColor(AppColour.Blue, for: UIControlState())
        
        previousSectionButton = button
        
    }
    
    func animateButtonIndicatorView(){
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }) { (finished:Bool) -> Void in
            
        }
    }
    
    func cellCount()->Int {
        
        if currentSection == nil {
            return 0
        }
        
        switch currentSection! {
        case .inspire:
            
            return posts.count + (Int(posts.count) / Int(15)) * 1
            
        case .newExplore:
            
            return posts.count
            
        case .timeLine:
            
            return posts.count + (Int(posts.count) / Int(15)) * 1
            
        }
        
    }
    
    func cellForIndexPath(_ indexPath:IndexPath)->CCPost? {
        
        if currentSection == nil {
            return nil
        }
        
        switch currentSection! {
            
        case .inspire:
            
            return posts[indexPath.row]
            
        case .newExplore:
            
            return posts[indexPath.row]
            
        case .timeLine:
            
            return posts[indexPath.row]
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refreshVisibleCells(){
        
        if currentSection == .newExplore {
            let indexPaths = self.collectionView.indexPathsForVisibleItems
            self.collectionView.reloadItems(at: indexPaths)
        }else{
            
            if let indexPaths = self.tableview.indexPathsForVisibleRows {
                self.tableview.reloadRows(at: indexPaths, with: .none)
            }
            
        }
        
    }
    
    func loadMore(){
        
        switch currentSection! {
            
        case .inspire:
            loadInspire(inspireCurrentPage + 1)
            break
        case .newExplore:
            //exploreCurrentPage + 1
            loadNewExplore(exploreCurrentPage + 1)
            break
        case .timeLine:
            loadTimeline(timelineCurrentPage + 1)
            break
        }
        
    }
    
}

extension ExploreViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //print("cell count: \(cellCount())")
        
        return cellCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let refinedIndex = indexPath.row + 1
        
        if (refinedIndex == 0) || (refinedIndex % 15 != 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDefaultCell") as! FeedDefaultCell
            cell.mainImageView.commentDelegate = self
            
            let refinedPostIndex = refinedIndex - 1 - ((refinedIndex / 15) * 1)
            let refinedPostIndexPath = IndexPath(row: refinedPostIndex, section: indexPath.section)
            let post = cellForIndexPath(refinedPostIndexPath)!
            
            cell.configureCell(post){(action) in
                
                Helper.locked({
                    
                    if action == .like {
                        
                        if !post.isLiked {
                            
                            
                            post.isLiked = true
                            post.likeCount = post.likeCount + 1
                            
                            self.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
                            
                            WebServiceAPI.likePost(post.postId, success: { currentCount in
                                
                                post.likeCount = currentCount
                                
                                DispatchQueue.main.async(execute: {
                                    self.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
                                });
                                
                                }, failure: {
                                    
                            })
                            
                        }else{
                            
                            post.isLiked = false
                            post.likeCount = post.likeCount - 1
                            
                            self.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
                            
                            WebServiceAPI.unlikePost(post.postId, success: { currentCount in
                                
                                post.likeCount = currentCount
                                
                                DispatchQueue.main.async(execute: {
                                    self.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
                                });
                                
                                }, failure: {
                                    
                            })
                            
                        }
                        
                    }else if action == .comment {
                        
                        let commentsViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
                        commentsViewController.post = post
                        commentsViewController.delegate = self
                        
                        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentsViewController)
                        
                    }else if action == .share {
                        
                        
                        /*
                         Code commented here is not required
                         let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! FeedDefaultCell
                         cell.mainImageView.commentDelegate = self
                         
                         */
                        
                        if let imageToSave = cell.mainImageView.image {
                            
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.showShareController(imageToSave)
                            
                        }
                        
                        
                    }else if action == .favourite{
                        
                        if post.isFavourite {
                            
                            post.isFavourite = false
                            self.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
                            
                            WebServiceAPI.unfavouritePost(post.postId, success: {
                                
                                }, failure: {
                                    
                            })
                            
                        }else{
                            
                            post.isFavourite = true
                            self.tableView(tableView, willDisplay: cell, forRowAt: indexPath)
                            
                            WebServiceAPI.favouritePost(post.postId, success: {
                                
                                }, failure: {
                                    
                            })
                            
                        }
                        
                    }
                    
                })
                
            }
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdCell") as! AdCell
            
            cell.configure(self)
            
            return cell
            
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        // UITableView only moves in one direction, y axis
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            self.loadMore()
        }
        
    }
    
}

extension ExploreViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let refinedIndex = indexPath.row + 1
        
        if (refinedIndex == 0) || (refinedIndex % 15 != 0) {
            
            let refinedPostIndex = refinedIndex - 1 - ((refinedIndex / 15) * 1)
            let refinedPostIndexPath = IndexPath(row: refinedPostIndex, section: indexPath.section)
            
            let post = cellForIndexPath(refinedPostIndexPath)!
            let cell = cell as! FeedDefaultCell
            cell.updateVariableContent(post)
        }
        
    }
    
}

extension ExploreViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //print("cell count: \(cellCount())")
        
        return cellCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMiniCell", for: indexPath) as! FeedMiniCell
        cell.mainImageView.commentDelegate = self
        
        let post = cellForIndexPath(indexPath)!
        
        cell.configureCell(post) { (action) in
            
            Helper.locked({ 
                
                if action == .like {
                    
                    if !post.isLiked {
                        
                        post.isLiked = true
                        post.likeCount = post.likeCount + 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.likePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }else{
                        
                        post.isLiked = false
                        post.likeCount = post.likeCount - 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.unlikePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }
                    
                }else{
                    
                    let commentsViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
                    commentsViewController.post = post
                    commentsViewController.delegate = self
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentsViewController)
                    
                }
                
            })
            
        }
        
        return cell
        
    }
    
    
}

extension ExploreViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let post = cellForIndexPath(indexPath)!
        let cell = cell as! FeedMiniCell
        cell.updateVariableContent(post)
        
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension ExploreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return feedMiniCellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8,8,8,8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}

extension ExploreViewController: CommentViewDelegate {
    
    func deleted(_ post: CCPost) {
        
        self.posts.removeObject(post)
        
        switch currentSection! {
            
        case .inspire:
            self.tableview.reloadData()
            break
        case .newExplore:
            self.collectionView.reloadData()
            break
        case .timeLine:
            self.tableview.reloadData()
            break
        }
        
    }
    
}


/*
 
 self.setVisibleSegment(.Note)
 
 buttonSelectionIndicatorConstraint.constant = button.frame.width * 2.0
 self.animateButtonIndicatorView()
 self.clearButtonSelections()
 dispatch_async(dispatch_get_main_queue()) {
 
 button.highlighted = true
 
 }
 
 */
