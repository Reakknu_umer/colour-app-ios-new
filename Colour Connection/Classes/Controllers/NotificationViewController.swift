//
//  NotificationViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class NotificationViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var tableview:UITableView!
    
    //MARK: Properties
    
    var dataSource:[String:String] = ["like":"0","comment":"0","default":"0"]
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addObserver()
        tableview.hideEmptyCells()
        // Do any additional setup after loading the view.
        
        loadNotifications()
        
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func loadNotifications(){
        
        WebServiceAPI.notificationCount({ (likeCount, commentCount, notificationCount) in
            
            self.dataSource["like"] = String(likeCount)
            self.dataSource["comment"] = String(commentCount)
            self.dataSource["default"] = String(notificationCount)
            self.tableview.reloadData()
            
            }, failure: {
                
        })
        
    }
    
    func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationViewController.loadNotifications), name: NSNotification.Name(rawValue: "NEW_NOTIFICATION"), object: nil)
    }
    
    //MARK: Helpers 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension NotificationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        let key = [String](dataSource.keys)[indexPath.row]
        
        cell.configureCell(NotificationCellType(rawValue:key)!, notificationCount: dataSource[key]!)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = Helper.isIphone() ? 68.0 : 114.0
        return CGFloat(height)
    }
    
}

extension NotificationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let notificationInfoViewController = StoryboardId.NotificationInfoView.instantiateViewController(StoryboardName.Notification) as! NotificationInfoViewController
        
        if indexPath.row == 0 {
            
            notificationInfoViewController.notificationType = .like
            self.dataSource["like"] = "0"
            
        }else if indexPath.row == 1 {
            
            notificationInfoViewController.notificationType = .comment
            self.dataSource["comment"] = "0"
            
        }else{
            
            notificationInfoViewController.notificationType = .default
            self.dataSource["default"] = "0"
            
        }
        
        self.tableview.reloadData()
        
        self.navigationController?.pushViewController(notificationInfoViewController, animated: true)
        
    }
    
}
