//
//  LoginViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD

class LoginViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var emailAddressTextField:UITextField!
    @IBOutlet weak var passwordTextField:UITextField!
    
    @IBOutlet weak var forgotPasswordButton:UIButton!
    @IBOutlet weak var loginButton:UIButton!
    @IBOutlet weak var registerButton:UIButton!
    
    //MARK: Properties 
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyStyle()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func forgotPasswordButtonAction(_ sender: AnyObject) {
        
        let forgotpasswordViewController:ForgotPasswordViewController = StoryboardId.ForgotPasswordView.instantiateViewController(StoryboardName.Login) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotpasswordViewController, animated: true)
        
    }
    
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if !validateForm() {
            return
        }
        
        let emailAddress = emailAddressTextField.text!
        let password = passwordTextField.text!
        
        WebServiceAPI.userLogin(emailAddress, password: password, success: { 
            
            UserSession.sharedSession.mainViewController?.hideLogin({ 
                
                NotificationCenter.default.post(name: Notification.Name(rawValue: InternalNotification.UserLoggedIn), object: nil)
                
            })
            
            }, failure: {
                
                DispatchQueue.main.async(execute: {
                    
                    SVProgressHUD.dismiss()
                    
                    if !WebServiceAPI.hasInternet() {
                        self.showDefaultAlert(nil, message: "No Intertnet Connection.")
                    }else{
                        self.showDefaultAlert(nil, message: "Credentials are not matching with our records.")
                    }
                    
                });
                
        })
        
    }
    
    @IBAction func registerButtonAction(_ sender: AnyObject) {
        
        
        let registerViewController:RegisterViewController = StoryboardId.RegisterView.instantiateViewController(StoryboardName.Login) as! RegisterViewController
        self.navigationController?.pushViewController(registerViewController, animated: true)
        
    }
    
    //MARK: - Private Functions 
    
    func validateForm()->Bool{
        
        if emailAddressTextField.isEmpty {
            self.showDefaultAlert(nil, message: "All fields are required.")
            return false
        }else if emailAddressTextField.isValidEmail == false {
            self.showDefaultAlert(nil, message: "Invalid email address.")
            return false
        }else if passwordTextField.isEmpty {
            self.showDefaultAlert(nil, message: "All fields are required.")
            return false
        }
        
        return true
    }
    
    //MARK: Helpers 
    
    func applyStyle(){
        
        let attributes1 = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue, NSForegroundColorAttributeName: AppColour.Blue, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(15.0))!] as [String : Any]
        
        let attributes2 = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue, NSForegroundColorAttributeName: AppColour.DarkGray, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(15.0))!] as [String : Any]
        
        forgotPasswordButton.setAttributedTitle(NSAttributedString(string: "Forgot Password", attributes: attributes1), for: UIControlState())
        
        registerButton.setAttributedTitle(NSAttributedString(string: "Register", attributes: attributes2), for: UIControlState())
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
