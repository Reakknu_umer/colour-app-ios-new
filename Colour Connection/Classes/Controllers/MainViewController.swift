//
//  MainViewController.swift
//  Colour Connection
//
//  Created  on 1/27/17.
//  at Eight25Media
//

import UIKit

class MainViewController: CCBaseViewController {
    
    //MARK: Enums 
    
    enum CustomTab{
        
        case exploreTab;
        case downloadMoreTab;
        case artTab;
        case profileTab;
        
        func storyboardInfo()->(StoryboardName,StoryboardId) {
            
            switch self {
                
            case .exploreTab:
                return (StoryboardName.Explore,StoryboardId.NavigationExplore)
            case .downloadMoreTab:
                return (StoryboardName.DownloadMore,StoryboardId.NavigationDownloadMore)
            case .artTab:
                return (StoryboardName.Art,StoryboardId.NavigationArt)
            case .profileTab:
                return (StoryboardName.Profile,StoryboardId.NavigationProfile)
                
            }
            
        }
        
    }
    
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var exploreButton:CCTabButton!
    @IBOutlet weak var downloadButton:CCTabButton!
    @IBOutlet weak var artButton:CCTabButton!
    @IBOutlet weak var profileButton:CCTabButton!
    
    @IBOutlet weak var viewControllerContainer:UIView!
    
    //MARK: Properties 
    
    var lastSelectedTabBarButton:CCTabButton?
    
    var currentNavigationController:UINavigationController?
    var previousNavigationController:UINavigationController?
    
    var loginNavigationController:UINavigationController?
    var exploreNavigationController:UINavigationController?
    var downloadMoreNavigationController:UINavigationController?
    var artNavigationController:UINavigationController?
    var profileNavigationController:UINavigationController?
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserSession.sharedSession.mainViewController = self
        
        let hasNotifications = UserDefaults.standard.bool(forKey: "HAS_PENDING_NOTIFICATIONS")
        
        if hasNotifications {
            
            UserDefaults.standard.set(false, forKey: "HAS_PENDING_NOTIFICATIONS")
            
            selectTabBarButton(profileButton)
            selectTab(.profileTab)
            
            profileNavigationController?.viewControllers = [profileNavigationController!.viewControllers.first!]
            let profileViewController = profileNavigationController?.viewControllers.first as! ProfileViewController
            profileViewController.infoButtonAction(UIButton())
            
        }else{
            
            //selectTabBarButton(exploreButton)
          //  selectTab(.exploreTab)
            
            //saro
            selectTabBarButton(downloadButton)
            selectTab(.downloadMoreTab)
            
            
            
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func exploreButtonAction(_ button:CCTabButton) {
        selectTabBarButton(button)
        selectTab(CustomTab.exploreTab)
    }
    
    @IBAction func downloadMoreAction(_ button:CCTabButton) {
        selectTabBarButton(button)
        selectTab(CustomTab.downloadMoreTab)
    }
    
    @IBAction func artButtonAction(_ button:CCTabButton) {
        selectTabBarButton(button)
        selectTab(CustomTab.artTab)
    }
    
    @IBAction func profileButtonAction(_ button:CCTabButton) {
        selectTabBarButton(button)
        selectTab(CustomTab.profileTab)
    }
    
    //MARK: - Private Functions 
    
    //MARK: Helpers 
    
    
    func showLogin(){
        
        if loginNavigationController == nil {
            loginNavigationController = StoryboardId.NavigationLogin.instantiateViewController(StoryboardName.Login) as! UINavigationController
        }
        
        present(loginNavigationController!, animated: true, completion: {
            
        })
        
        
    }
    
    func hideLogin(_ completion:@escaping (()->())){
        
        loginNavigationController?.dismiss(animated: true, completion: { 
            completion()
        })
        
    }
    
    func pushViewControllerFromMain(_ viewcontroller:UIViewController){
        
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    func selectTabBarButton(_ button: CCTabButton){
        
        if lastSelectedTabBarButton != nil {
            deselectTabBarButton(lastSelectedTabBarButton!)
        }
        
        
        let image = UIImage(named: "\(getImageName(button))_selected")
        button.setImage(image, for: UIControlState())
        button.setImage(image, for: UIControlState.highlighted)
        
        button.setTitleColor(AppColour.Blue, for: UIControlState())
        button.tag = 1
        lastSelectedTabBarButton = button
        
    }
    
    func deselectTabBarButton(_ button: UIButton){
        button.backgroundColor = UIColor.white
        
        let image = UIImage(named: "\(getImageName(button))")
        button.setImage(image, for: UIControlState())
        button.setImage(image, for: UIControlState.highlighted)
        
        button.setTitleColor(AppColour.DarkGray, for: UIControlState())
        button.tag = 0
    }
    
    func getImageName(_ button:UIButton) -> String {
        
        if button == exploreButton  {
            return "tab_explote"
        } else if button == downloadButton  {
            return "tab_download"
        }else if button == artButton  {
            return "tab_art"
        } else {
            return "tab_profile"
        }
        
    }
    
    func selectTab(_ tab:CustomTab){
        
        //print("Navigation Controller: \(self.navigationController)")
        
        for viewcontroller in self.navigationController!.viewControllers {
            
            //print("View Controller: \(viewcontroller)")
            
        }
        
        //print("count: \(self.navigationController!.viewControllers.count)")
        
        if self.navigationController!.viewControllers.count > 1 {
            self.navigationController?.popToRootViewController(animated: true)
            //            self.navigationController?.popViewControllerAnimated(true)
        }
        
        previousNavigationController = currentNavigationController
        
        switch tab {
            
            
        case .exploreTab:
            
            if exploreNavigationController == nil {
                
                exploreNavigationController = tab.storyboardInfo().1.instantiateViewController(tab.storyboardInfo().0) as? UINavigationController
                
            }
            
            if currentNavigationController == exploreNavigationController {
                exploreNavigationController?.popToRootViewController(animated: true)
                return
            }
            
            currentNavigationController = exploreNavigationController
            
        case .downloadMoreTab:
            
            if downloadMoreNavigationController == nil {
                downloadMoreNavigationController = tab.storyboardInfo().1.instantiateViewController(tab.storyboardInfo().0) as? UINavigationController
            }
            
            if currentNavigationController == downloadMoreNavigationController {
                currentNavigationController?.popToRootViewController(animated: true)
                return
            }
            
            currentNavigationController = downloadMoreNavigationController
            
        case .artTab:
            
            if artNavigationController == nil {
                artNavigationController = tab.storyboardInfo().1.instantiateViewController(tab.storyboardInfo().0) as? UINavigationController
            }
            
            if currentNavigationController == artNavigationController {
                currentNavigationController?.popToRootViewController(animated: true)
                return
            }
            
            currentNavigationController = artNavigationController
            
        case .profileTab:
            
            if profileNavigationController == nil {
                profileNavigationController = tab.storyboardInfo().1.instantiateViewController(tab.storyboardInfo().0) as? UINavigationController
            }
            
            if currentNavigationController == profileNavigationController {
                currentNavigationController?.popToRootViewController(animated: true)
                return
            }
            
            currentNavigationController = profileNavigationController
            
        }
        
        addViewControllerToContainer(currentNavigationController!)
        
    }
    
    func addViewControllerToContainer(_ viewController:UIViewController){
        
        self.addChildViewController(viewController)
        viewController.view.frame = CGRect(x: 0, y: 0, width: viewControllerContainer.frame.size.width, height: viewControllerContainer.frame.size.height);
        viewControllerContainer.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        
        if previousNavigationController != nil {
            
            previousNavigationController?.willMove(toParentViewController: nil)
            previousNavigationController?.view.removeFromSuperview()
            previousNavigationController?.removeFromParentViewController()
            
        }
        
    }
    
    func isInNotificationScreen()->Bool {
        
        if currentNavigationController == profileNavigationController {
            
            if let controller = currentNavigationController?.viewControllers.last {
                
                let condition1 = controller is NotificationViewController
                let condition2 = controller is NotificationInfoViewController
                
                return (condition1 || condition2)
                
            }
            
        }
        
        return false
    }
    
    func showNotificationScreen(){
        
        if currentNavigationController == profileNavigationController {
            
            let condition1 = currentNavigationController!.viewControllers.last is NotificationViewController
            let condition2 = currentNavigationController!.viewControllers.last is NotificationInfoViewController
            
            if !(condition1 || condition2) {
                
                profileNavigationController?.viewControllers = [profileNavigationController!.viewControllers.first!]
                let profileViewController = profileNavigationController?.viewControllers.first as! ProfileViewController
                profileViewController.infoButtonAction(UIButton())
                
            }else{
                //Does not come here
            }
            
            
        }else{
            
            selectTabBarButton(profileButton)
            selectTab(.profileTab)
            
            profileNavigationController?.viewControllers = [profileNavigationController!.viewControllers.first!]
            let profileViewController = profileNavigationController?.viewControllers.first as! ProfileViewController
            profileViewController.infoButtonAction(UIButton())
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

