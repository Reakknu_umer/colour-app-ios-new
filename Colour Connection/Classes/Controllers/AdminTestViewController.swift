//
//  AdminTestViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class AdminTestViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var collectionView:UICollectionView!
    
    //MARK: Properties 
    
    var testTemplates = [CCTemplate](){
        
        didSet{
            
            
            if isViewLoaded {
                
                collectionView.reloadData()
                
            }
            
            
        }
        
    }
    
    //MARK: Computed Properties
    
    var feedMiniCellSize:CGSize {
        
        get{
            
            let width = (collectionView.frame.width / 2.0) - 12
            
            return CGSize(width: width, height: width)
            
        }
        
    }
    
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadTemplate()
        addRefrshController()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func loadTemplate(){
        
        WebServiceAPI.listTestTemplates({ (templates) in
            self.testTemplates = templates
            }, failure: {
                
        })
        
    }
    
    //MARK: Helpers 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadTemplate()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension AdminTestViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return testTemplates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestTemplateCell", for: indexPath) as! TestTemplateCell
        
        cell.configureCell(testTemplates[indexPath.row])
        
        return cell
        
    }
    
    
}

extension AdminTestViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        Helper.showTestArtBoard(testTemplates[indexPath.row])
        
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension AdminTestViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return feedMiniCellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8,8,8,8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}
