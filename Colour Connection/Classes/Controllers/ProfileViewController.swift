//
//  ProfileViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

enum ProfileViewType {
    case my
    case other
}

class ProfileViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var navigationView:UIView!
    @IBOutlet weak var loginRequiredView:UIView!
    @IBOutlet weak var collectionView:UICollectionView!
    
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var infoButton:UIButton!
    @IBOutlet weak var settingButton:UIButton!
    
    @IBOutlet weak var navigationHeightConstraint:NSLayoutConstraint!
    
    
    //MARK: Properties
    
    var otherUserId:String?
    var profile:CCProfile?
    var profileViewType = ProfileViewType.my
    
    var isPrimaryMyProfileView:Bool{
        
        get{
            
            if let identifier = self.navigationController?.restorationIdentifier {
                
                let condition1 = (identifier == "NavigationProfile")
                let condition2 = self.navigationController!.viewControllers.count == 1
                
                return condition1 && condition2
                
            }else{
                return false
            }
            
        }
        
    }
    
    
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleButtonVisibility()
        
        if UserSession.sharedSession.isValid {
            
            loginRequiredView.isHidden = true
            
        }else{
            
            loginRequiredView.isHidden = false
            
            //            refreshAfterLogin {
            //                //print("Refresh :-)")
            //                self.infoButton.hidden = false
            //                self.loadPosts()
            //            }
            
        }
        
        addRefrshController()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserSession.sharedSession.isValid {
            loginRequiredView.isHidden = true
            
            if profile == nil {
                loadPosts(true)
            }else{
                loadPosts(false)
            }
            
        }else{
            loginRequiredView.isHidden = false
        }
        
        handleButtonVisibility()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if profileViewType == .my && isPrimaryMyProfileView {
            self.navigationHeightConstraint.constant = Helper.isIPad() ? 80 : 44
        }else{
            self.navigationHeightConstraint.constant = Helper.isIPad() ? 100 : 64
        }
        
        self.navigationView.layoutIfNeeded()
        
        //NavigationProfile
        //print("Navigation Controller: \(self.navigationController)")
        //print("StoryboardId: \(self.navigationController?.restorationIdentifier)")
        
        //NavigationProfile
        
    }
    
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        
        UserSession.sharedSession.mainViewController?.showLogin()
        
    }
    
    @IBAction func infoButtonAction(_ sender: AnyObject) {
        
        Helper.locked(){
            
            let notificationViewController = StoryboardId.NotificationView.instantiateViewController(StoryboardName.Notification) as! NotificationViewController
            UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(notificationViewController)
            //            self.navigationController?.pushViewController(notificationViewController, animated: true)
            
        }
        
        
    }
    
    @IBAction func settingsButtonAction(_ sender: AnyObject) {
        
        Helper.locked(){
            
            let settingsViewController = StoryboardId.SettingsView.instantiateViewController(StoryboardName.Settings) as! SettingsViewController
            UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(settingsViewController)
            //            self.navigationController?.pushViewController(settingsViewController, animated: true)
            
        }
    }
    
    //MARK: - Private Functions 
    
    func loadPosts(_ canDisturbUser: Bool){
        
        if profileViewType == .my {
            
            WebServiceAPI.userProfile({ (profile) in
                self.profile = profile
                self.collectionView.reloadData()
                }, failure: {
                    
                }, canDisturbUser: canDisturbUser)
            
        }else{
            
            WebServiceAPI.otherUserProfile(otherUserId!, success: { (profile) in
                self.profile = profile
                self.collectionView.reloadData()
                }, failure: {
                    
                }, canDisturbUser: canDisturbUser)
            
        }
        
    }
    
    //MARK: Helpers 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadPosts(true)
        
    }
    
    var feedMiniCellSize:CGSize {
        
        get{
            
            let width = (collectionView.frame.width / 2.0) - 12
            
            let padding:CGFloat = Helper.isIphone() ? 34.0 : 52.0
            
            return CGSize(width: width, height: (width + padding))
            
            
        }
        
    }
    
    func handleButtonVisibility(){
        
        if profileViewType == .my && isPrimaryMyProfileView {
            
            backButton.isHidden = true
            infoButton.isHidden = !UserSession.sharedSession.isValid
            settingButton.isHidden = false
            
        }else{
            
            backButton.isHidden = false
            infoButton.isHidden = true
            settingButton.isHidden = true
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension ProfileViewController:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
        }else{
            
            let post = self.profile!.posts[indexPath.row]
            let cell = cell as! FeedMiniCell
            cell.updateVariableContent(post)
            
        }
        
    }
    
}

extension ProfileViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 {
            
            return (self.profile == nil) ?  0 : 1
            
        }else{
            
            return self.profile?.posts.count ?? 0
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherProfileCell", for: indexPath) as! OtherProfileCell
            
            cell.configureCell(profile!) { (action) in
                
                switch action {
                    
                case .following:
                    
                    let followingViewController = StoryboardId.FollowView.instantiateViewController(StoryboardName.Profile) as! FollowViewController
                    
                    followingViewController.otherUserId = self.otherUserId
                    followingViewController.followType = .following
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(followingViewController)
                    
                    break
                case .followers:
                    
                    let followersViewController = StoryboardId.FollowView.instantiateViewController(StoryboardName.Profile) as! FollowViewController
                    
                    followersViewController.otherUserId = self.otherUserId
                    followersViewController.followType = .follower
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(followersViewController)
                    
                    break
                case .favourite:
                    
                    let favouriteViewController = StoryboardId.FavouriteView.instantiateViewController(StoryboardName.Profile) as! FavouriteViewController
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(favouriteViewController)
                    
                    break
                    
                case .follow:
                    
                    WebServiceAPI.followUser(self.profile!.userId, success: { 
                        
                        self.profile!.isFollowing = true
                        self.collectionView.reloadItems(at: [indexPath])
                        
                        }, failure: {
                            
                    })
                    
                    break
                    
                case .unfollow:
                    
                    WebServiceAPI.unfollowUser(self.profile!.userId, success: {
                        
                        self.profile!.isFollowing = false
                        self.collectionView.reloadItems(at: [indexPath])
                        
                        }, failure: {
                            
                    })
                    
                    break
                    
                }
                
            }
            
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            
            return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMiniCell", for: indexPath) as! FeedMiniCell
//            cell.isTappable = false
            
            let post = self.profile!.posts[indexPath.row]
            
            cell.configureCell(post, action: { (action) in
                
                if action == .like {
                    
                    if !post.isLiked {
                        
                        post.isLiked = true
                        post.likeCount = post.likeCount + 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.likePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    } else {
                        
                        post.isLiked = false
                        post.likeCount = post.likeCount - 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.unlikePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }
                    
                }else{
                    
                    let commentsViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
                    commentsViewController.post = post
                    commentsViewController.delegate = self
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentsViewController)
                    
                }
                
            })
            
            return cell
            
        }
        
        
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension ProfileViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.section == 0 {
            
            let width = collectionView.frame.width - 16
            let height:CGFloat = Helper.isIphone() ? 148.0 : 260.0
            
            return CGSize(width: width, height: height)
            
        }else{
            
            return feedMiniCellSize
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(4,8,8,8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}

extension ProfileViewController: CommentViewDelegate {
    
    func deleted(_ post: CCPost) {
        self.profile?.posts.removeObject(post)
        self.collectionView.reloadData()
    }
    
}
