//
//  TermsViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class TermsViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var webview:UIWebView!
    
    //MARK: Properties 
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let targetURL = Bundle.main.url(forResource: "privicy_policy", withExtension: "pdf")! // This value is force-unwrapped for the sake of a compact example, do not do this in your code
        let request = URLRequest(url: targetURL)
        webview.loadRequest(request)
        
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    //MARK: Helpers 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
