//
//  ArtViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class ArtViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var loginRequiredView:UIView!
    
    //MARK: Properties 
    //MARK: Computed Properti
    
    var feedMiniCellSize:CGSize {
        
        get{
            
            let width = (collectionView.frame.width / 2.0) - 12
            let height = width + (Helper.isIPad() ? 80 : 46)
            
            return CGSize(width: width, height: height)
            
        }
        
    }
    
    var savedTemplates = [SavedTemplate](){
        
        didSet{
            
            collectionView.reloadData()
            
        }
        
    }
    
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = nil
        collectionView.dataSource = nil
        
        self.addRefrshController()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserSession.sharedSession.isValid {
            
            collectionView.delegate = self
            collectionView.dataSource = self
            
            savedTemplates = SavedTemplate.allTemapltes()
            self.loginRequiredView.isHidden = true
        }else{
            self.loginRequiredView.isHidden = false
        }
        
        
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        
        UserSession.sharedSession.mainViewController?.showLogin()
        
    }
    
    //MARK: - Private Functions 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        savedTemplates = SavedTemplate.allTemapltes()
        
    }
    
    //MARK: Helpers 
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension ArtViewController:UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let controller = StoryboardId.ArtBoardView.instantiateViewController(StoryboardName.Art) as! ArtBoardViewController
        controller.savedTemplate = savedTemplates[indexPath.row]
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(controller)
        
    }
    
}

extension ArtViewController: UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.savedTemplates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let temaplete = savedTemplates[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ArtCell", for: indexPath) as! ArtCell
        
        cell.configureCell(temaplete)
        
        return cell
        
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension ArtViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return feedMiniCellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8,8,8,8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}
