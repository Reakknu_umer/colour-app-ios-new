//
//  FavouriteViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class FavouriteViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var collectionView:UICollectionView!
    
    //MARK: Properties 
    
    var posts = [CCPost](){
        
        didSet{
            
            if isViewLoaded {
                self.collectionView.reloadData()
            }
            
        }
    }
    
    //MARK: Computed Properties
    
    var feedMiniCellSize:CGSize {
        
        get{
            
            let width = (collectionView.frame.width / 2.0) - 12
            
            let padding:CGFloat = Helper.isIphone() ? 34.0 : 52.0
            
            return CGSize(width: width, height: (width + padding))
            
        }
        
    }
    
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadfavourites()
        addRefrshController()
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func loadfavourites(){
        
        WebServiceAPI.favouritePosts({ (posts) in
            self.posts = posts
            
            }, failure: {
                
        })
        
    }
    
    //MARK: Helpers 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        self.collectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadfavourites()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension FavouriteViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return feedMiniCellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8,8,8,8)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}

extension FavouriteViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let post = posts[indexPath.row]
        let cell = cell as! FeedMiniCell
        cell.updateVariableContent(post)
        
    }
    
}

extension FavouriteViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        //print("cell count: \(posts.count)")
        
        return posts.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedMiniCell", for: indexPath) as! FeedMiniCell
        cell.isTappable = false
        
        cell.mainImageView.commentDelegate = self
        
        let post = posts[indexPath.row]
        
        cell.configureCell(post) { (action) in
            
            Helper.locked({
                
                if action == .like {
                    
                    if !post.isLiked {
                        
                        post.isLiked = true
                        post.likeCount = post.likeCount + 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.likePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            
                            }, failure: {
                                
                        })
                        
                    }else{
                        
                        post.isLiked = false
                        post.likeCount = post.likeCount - 1
                        
                        self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                        
                        WebServiceAPI.unlikePost(post.postId, success: { currentCount in
                            
                            post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }
                    
                }else{
                    
                    let commentsViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
                    commentsViewController.post = post
                    
                    UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentsViewController)
                    
                }
                
            })
            
        }
        
        return cell
        
    }
    
}

extension FavouriteViewController: CommentViewDelegate {
    
    func deleted(_ post: CCPost) {
        
        self.posts.removeObject(post)
        self.collectionView.reloadData()
        
    }
    
}
