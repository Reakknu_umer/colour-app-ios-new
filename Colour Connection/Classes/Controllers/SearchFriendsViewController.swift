//
//  SearchFriendsViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class SearchFriendsViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var searchTextField:UITextField!
    @IBOutlet weak var usersTableView:UITableView!
    
    //MARK: Properties 
    
    var searchResult:[CCUser]? {
        
        didSet{
            
            if isViewLoaded {
                usersTableView.reloadData()
            }
            
        }
        
    }
    
    var resultCount:Int {
        
        get{
            
            if searchResult == nil {
                return 0
            }else{
                return searchResult!.count
            }
            
        }
        
    }
    
    var requestTimer:Timer?
    
    //MARK: Computed Properties
    
    //MARK: Initializers
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usersTableView.hideEmptyCells()
        
        applyStyles()
        addTextChangeObserver()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    
    //MARK: IBAction 
    
    //MARK: - Private Functions
    
    //MARK: Helpers 
    
    func applyStyles(){
        
        //add search icon
        let imageView = UIImageView(image: UIImage(named: "find_friends"))
        imageView.contentMode = .center
        imageView.frame = CGRect(x: Helper.isIPad() ? 50 : 24 , y: 0, width: 36, height: 20)
        searchTextField.leftView = imageView
        searchTextField.leftViewMode = .always
        
        searchTextField.layer.cornerRadius = Helper.isIPad() ? 25 : 14
        searchTextField.layer.borderColor = AppColour.Gray.cgColor
        searchTextField.layer.borderWidth = 1.0
        searchTextField.clipsToBounds = true
        
    }
    
    func addTextChangeObserver(){
        searchTextField.addTarget(self, action: #selector(SearchFriendsViewController.didChangeText(_:)), for: .editingChanged)
    }
    
    func didChangeText(_ textField:UITextField){
        
        let noKeyword = searchTextField.isEmpty
        
        if noKeyword {
            searchResult = nil
        }else{
            registerSearchRequest()
        }
        
    }
    
    func registerSearchRequest(){
        
        requestTimer?.invalidate()
        requestTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(SearchFriendsViewController.requestSearch), userInfo: nil, repeats: false)
        
    }
    
    func requestSearch(){
        
        if searchTextField.text!.characters.count == 0 {
            searchResult = nil
            return
        }
        
        WebServiceAPI.searchUsers(searchTextField.text!, success: { (users) in
            
            self.searchResult = users
            
            }, failure: {
                
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension SearchFriendsViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let user = searchResult![indexPath.row]
        
        let cell = cell as! SearchCell
        cell.updateVariableContent(user)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return Helper.isIPad() ? 114 :66
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let user = searchResult![indexPath.row]
        Helper.showProfile(user.userId)
        
    }
}

extension SearchFriendsViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchCell
        
        let user = searchResult![indexPath.row]
        
        cell.configureCell(user){ follow in
            
            if follow {
                
                user.isFollowing = false
                self.tableView(self.usersTableView, willDisplay: cell, forRowAt: indexPath)
                
                WebServiceAPI.unfollowUser(user.userId, success: {
                    
                    }, failure: {
                        
                })
                
            }else{
                
                user.isFollowing = true
                self.tableView(self.usersTableView, willDisplay: cell, forRowAt: indexPath)
                
                WebServiceAPI.followUser(user.userId, success: {
                    
                    }, failure: {
                        
                })
                
            }
            
            
        }
        
        return cell
    }
    
}

extension SearchFriendsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
