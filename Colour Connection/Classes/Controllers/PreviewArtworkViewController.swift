//
//  PreviewArtworkViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD

class PreviewArtworkViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var navigationTitleLabel:UILabel!
    @IBOutlet weak var mainImageView:UIImageView!
    @IBOutlet weak var button:UIButton!
    
    //MARK: Properties
    
    var template:CCTemplate!
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainImageView.showBorder()
        loadTemplate()
        
        button.layer.cornerRadius = 5.0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateButtonTitle()
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    /*
     
     SVProgressHUD.showWithStatus("Downloading...")
     
     let savedTemplate = SavedTemplate.addNewSavedTemplate(template)
     
     Downloader.load(savedTemplate, completion: { (success) in
     
     if success {
     
     collectionView.reloadItemsAtIndexPaths([indexPath])
     
     SVProgressHUD.showSuccessWithStatus("Downloaded")
     }else{
     SVProgressHUD.showErrorWithStatus("Failed")
     }
     
     })
     
     
     */
    
    @IBAction func buttonAction(_ sender: AnyObject) {
        
        if template.isSubOnly {
            
            if UserSession.sharedSession.currentUser!.isSubscriptionValid {
                
                if template.isSaved() {
                    Helper.showArtBoard(template.savedTemplate()!)
                }else{
                    downloadTemplete()
                }
                
            }else{
                
                Helper.showSubscriptionView()
                
            }
            
        }else{
            
            if template.isSaved() {
                Helper.showArtBoard(template.savedTemplate()!)
            }else{
                downloadTemplete()
            }
            
        }
        
    }
    
    func downloadTemplete(){
        
        SVProgressHUD.show(withStatus: "Downloading...")
        
        let savedTemplate:SavedTemplate = SavedTemplate.addNewSavedTemplate(template)
        
        Downloader.load(savedTemplate, completion: { (success) in
            
            DispatchQueue.main.async {
                
                if success {
                    
                    SVProgressHUD.dismiss()
                    self.showDefaultAlert(nil, message: "Downloaded.")
                    
                }else{
                    
                    SVProgressHUD.dismiss()
                    self.showDefaultAlert(nil, message: "Failed.")
                    
                }
                
                self.updateButtonTitle()
                
            }
            
            
            
        })
        
    }
    
    //MARK: - Private Functions 
    
    func updateButtonTitle(){
        
        if template.isSubOnly {
            
            if UserSession.sharedSession.currentUser!.isSubscriptionValid {
                
                if template.isSaved() {
                    button.setTitle("Open", for: UIControlState())
                }else{
                    button.setTitle("Get", for: UIControlState())
                }
                
            }else{
                
                button.setTitle("Subscribe", for: UIControlState())
                
            }
            
        }else{
            
            if template.isSaved() {
                button.setTitle("Open", for: UIControlState())
            }else{
                button.setTitle("Free", for: UIControlState())
            }
            
        }
        
    }
    
    func loadTemplate(){
        
        navigationTitleLabel.text = template.templateName
        mainImageView.sd_setImage(with: template.thumbImageUrl as URL!, placeholderImage: nil)
        
    }
    
    //MARK: Helpers 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
