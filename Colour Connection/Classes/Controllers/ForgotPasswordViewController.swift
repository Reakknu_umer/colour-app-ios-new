//
//  ForgotPasswordViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD

class ForgotPasswordViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var emailTextField:UITextField!
    
    //MARK: Properties 
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func resetButtonAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if !validateForm() {
            return
        }
        
        let emailAddress = emailTextField.text!
        
        WebServiceAPI.userForgotPassword(emailAddress, success: { 
            
            DispatchQueue.main.async(execute: {
                
                self.showDefaultAlert(nil, message: "Password reset link has been sent to your email.")
                
            });
            
            }, failure: { errorMessage in
                
                DispatchQueue.main.async(execute: {
                    SVProgressHUD.dismiss()
                    
                    if !WebServiceAPI.hasInternet() {
                        self.showDefaultAlert(nil, message: "No Internet Connection.")
                    }else{
                        self.showDefaultAlert(nil, message: errorMessage)
                    }
                    
                });
                
        })
        
    }
    
    //MARK: - Private Functions 
    
    func validateForm()->Bool {
        
        if emailTextField.isEmpty {
            self.showDefaultAlert(nil, message: "Email address cannot be empty.")
            return false
        }else if emailTextField.isValidEmail == false {
            self.showDefaultAlert(nil, message: "Invalid email address.")
            return false
        }
        
        return true
    }
    
    //MARK: Helpers 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
