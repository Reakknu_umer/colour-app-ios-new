import UIKit
import SVProgressHUD
import SDWebImage
import SVGKit
@IBDesignable
class CustomSlider : UISlider {
    @IBInspectable  var trackWidth:CGFloat = 2 {
        didSet {setNeedsDisplay()}
    }
    
    @IBInspectable var type:Int = 0
    
    let gradient = CAGradientLayer()
    
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        
        let defaultBounds = super.trackRect(forBounds: bounds)
        let rect = CGRect(
            x: defaultBounds.origin.x,
            y: defaultBounds.origin.y + defaultBounds.size.height/2 - trackWidth/2,
            width: defaultBounds.size.width,
            height: trackWidth
        )
        
        gradient.frame = rect
        
        return rect
        
    }
    
    override func awakeFromNib() {
        self.setThumbImage(UIImage(named: "thumb"), for: UIControlState())
        super.awakeFromNib()
        
        gradient.startPoint = CGPoint(x: 0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1, y: 0.5)
        gradient.cornerRadius = 5.0
        gradient.borderColor = UIColor.white.cgColor
        gradient.borderWidth = 1.0
        
        if type == 0 {
            
            
            gradient.colors = [UIColor(hue: 0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor,
                               UIColor(hue: 1/6.0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor,
                               UIColor(hue: 2/6.0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor,
                               UIColor(hue: 3/6.0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor,
                               UIColor(hue: 4/6.0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor,
                               UIColor(hue: 5/6.0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor,
                               UIColor(hue: 1.0, saturation: 1.0, brightness: 1.0, alpha: 1.0).cgColor
            ]
            
            
        }
        
        self.layer.insertSublayer(gradient, at: 0)
        
    }
    
    func update(_ hue:CGFloat, saturation:CGFloat, brightness:CGFloat){
        
        if type == 0 {
            
            
            gradient.colors = [UIColor(hue: 0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: 1/6.0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: 2/6.0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: 3/6.0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: 4/6.0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: 5/6.0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: 1.0, saturation: saturation, brightness: brightness, alpha: 1.0).cgColor
            ]
            
            
        }else if type == 1 {
            
            gradient.colors = [UIColor(hue: hue, saturation: 0, brightness: brightness, alpha: 1.0).cgColor,
                               UIColor(hue: hue, saturation: 1.0, brightness: brightness, alpha: 1.0).cgColor
            ]
            
        }else if type == 2{
            
            gradient.colors = [UIColor(hue: hue, saturation: saturation, brightness: 0, alpha: 1.0).cgColor,
                               UIColor(hue: hue, saturation: saturation, brightness: 1.0, alpha: 1.0).cgColor
            ]
            
        }
        
    }
    
}

struct Palette {
    
    var name:String?
    var colours:[UIColor]?
    
    init(name:String, colours:[UIColor]){
        self.name = name
        self.colours = colours
    }
    
}

struct ArtBoard {
    
    static let palettes = [
        Palette(name: "Ocean", colours:
            [UIColor(hex:0x080056),UIColor(hex:0x080098),UIColor(hex:0x27007b),UIColor(hex:0x004080),UIColor(hex:0x0066cc),
                UIColor(hex:0x3246ff),UIColor(hex:0x336699),UIColor(hex:0x2769ff),UIColor(hex:0x4169e1),UIColor(hex:0x3299ff),
                UIColor(hex:0x00ccff),UIColor(hex:0x6699ff),UIColor(hex:0x4ab2ff),UIColor(hex:0x87cefa),UIColor(hex:0x99ccff)]),
        Palette(name: "Autumn", colours:
            [UIColor(hex:0x741a00),UIColor(hex:0x990606),UIColor(hex:0x853512),UIColor(hex:0xad401f),UIColor(hex:0x90540b),
                UIColor(hex:0xa27017),UIColor(hex:0xfd5900),UIColor(hex:0xff6d20),UIColor(hex:0xcd9818),UIColor(hex:0xff8b4d),
                UIColor(hex:0xffaa7c),UIColor(hex:0xffd401),UIColor(hex:0xffe700),UIColor(hex:0xfff272),UIColor(hex:0xfff7b4)]),
        Palette(name: "Classic", colours:
            [UIColor(hex:0xb88f73),UIColor(hex:0x664e40),UIColor(hex:0x6b7f64),UIColor(hex:0x879b66),UIColor(hex:0x264065),
                UIColor(hex:0x1b2064),UIColor(hex:0xba4e80),UIColor(hex:0x8c2b65),UIColor(hex:0x633164),UIColor(hex:0x402d65),
                UIColor(hex:0x5a42a4),UIColor(hex:0x450b5f),UIColor(hex:0xb75f1d),UIColor(hex:0xb91e1a),UIColor(hex:0x3e3e3e)]),
        Palette(name: "Shades", colours:
            [UIColor(hex:0x000000),UIColor(hex:0x171717),UIColor(hex:0x1f1f1f),UIColor(hex:0x333300),UIColor(hex:0x333333),
                UIColor(hex:0x3b3b2c),UIColor(hex:0x454545),UIColor(hex:0x4f4f4f),UIColor(hex:0x5c5c45),UIColor(hex:0x757575),
                UIColor(hex:0x708090),UIColor(hex:0x949494),UIColor(hex:0xbfbfbf),UIColor(hex:0xd9d9d9),UIColor(hex:0xffffff)]),
        Palette(name: "Pop Art", colours:
            [UIColor(hex:0xc07cb7),UIColor(hex:0xbf58b1),UIColor(hex:0xbf3cac),UIColor(hex:0x40477e),UIColor(hex:0xbf3d3d),
                UIColor(hex:0xbfa93c),UIColor(hex:0x93832e),UIColor(hex:0x50922e),UIColor(hex:0x2e926e),UIColor(hex:0x2d7e93),
                UIColor(hex:0x6f2793),UIColor(hex:0x93286e),UIColor(hex:0xd03739),UIColor(hex:0x3a43ce),UIColor(hex:0x38a1ce)]),
        Palette(name: "Basic Pink", colours:
            [UIColor(hex:0xa1007b),UIColor(hex:0x86226a),UIColor(hex:0xc71585),UIColor(hex:0xaf2c89),UIColor(hex:0xff00cc),
                UIColor(hex:0x89508d),UIColor(hex:0x9c4d8f),UIColor(hex:0xca399e),UIColor(hex:0xffabff),UIColor(hex:0xb04b91),
                UIColor(hex:0xb05693),UIColor(hex:0xcb57a8),UIColor(hex:0xd46dd1),UIColor(hex:0xff6ed3),UIColor(hex:0xff8ddb)]),
        Palette(name: "Pastels", colours:
            [UIColor(hex:0x8c70a1),UIColor(hex:0x6bb6ff),UIColor(hex:0x8cbda1),UIColor(hex:0xd6989e),UIColor(hex:0xd698be),
                UIColor(hex:0xa4c567),UIColor(hex:0x8cbdff),UIColor(hex:0xc3b6ff),UIColor(hex:0xecc5be),UIColor(hex:0xffc69e),
                UIColor(hex:0xc3d6ff),UIColor(hex:0x91ffff),UIColor(hex:0xffff46),UIColor(hex:0xccffff),UIColor(hex:0xffff88)]),
        Palette(name: "Brown", colours:
            [UIColor(hex:0xffefe0),UIColor(hex:0xf2e6ce),UIColor(hex:0xe4d8c2),UIColor(hex:0xdec4ab),UIColor(hex:0xc8aa92),
                UIColor(hex:0xa88373),UIColor(hex:0x95705e),UIColor(hex:0x7e5d4a),UIColor(hex:0x532919),UIColor(hex:0x3a1e10),
                UIColor(hex:0x481713),UIColor(hex:0x4b342e),UIColor(hex:0x54250b),UIColor(hex:0x3a2d25),UIColor(hex:0x200d00)]),
        Palette(name: "Purple", colours:
            [UIColor(hex:0x330066),UIColor(hex:0x37084c),UIColor(hex:0x560d76),UIColor(hex:0x660099),UIColor(hex:0x6f1199),
                UIColor(hex:0x53378a),UIColor(hex:0x8b16c6),UIColor(hex:0x663399),UIColor(hex:0x7e3ddf),UIColor(hex:0x9933ff),
                UIColor(hex:0x9854ff),UIColor(hex:0x906cd9),UIColor(hex:0x9966ff),UIColor(hex:0xae79ff),UIColor(hex:0xc198ff)]),
        Palette(name: "Aqua", colours:
            [UIColor(hex:0x177785),UIColor(hex:0x0187d0),UIColor(hex:0x1f889b),UIColor(hex:0x019d9a),UIColor(hex:0x259ba7),
                UIColor(hex:0x14adfd),UIColor(hex:0x669999),UIColor(hex:0x6699cc),UIColor(hex:0x56afad),UIColor(hex:0x52c0fb),
                UIColor(hex:0x68d5d2),UIColor(hex:0x83d3d0),UIColor(hex:0x99cccc),UIColor(hex:0x7dd2ff),UIColor(hex:0xa4dffd)]),
        Palette(name: "Forrest", colours:
            [UIColor(hex:0x230d00),UIColor(hex:0x092500),UIColor(hex:0x0b3b00),UIColor(hex:0x115500),UIColor(hex:0x156600),
                UIColor(hex:0x1a7e00),UIColor(hex:0x506b00),UIColor(hex:0x437d00),UIColor(hex:0x3c8529),UIColor(hex:0x6c9103),
                UIColor(hex:0x4b9658),UIColor(hex:0x6a8562),UIColor(hex:0x1dc200),UIColor(hex:0x88a23d),UIColor(hex:0x93a267)])
    ]
    
}

class ArtBoardViewController: CCBaseViewController {
    
    var savedTemplate:SavedTemplate?
    var testTemplate:CCTemplate?
    var alert:UIAlertController?
    
    var savedColour1: UIColor {
        get {
            return UserDefaults.standard.colorForKey("savedColour1") ?? UIColor.red
        }
        set {
            UserDefaults.standard.setColor(newValue, forKey: "savedColour1")
        }
    }
    
    var savedColour2: UIColor {
        get {
            return UserDefaults.standard.colorForKey("savedColour2") ?? UIColor.green
        }
        set {
            UserDefaults.standard.setColor(newValue, forKey: "savedColour2")
        }
    }
    
    var savedColour3: UIColor {
        get {
            return UserDefaults.standard.colorForKey("savedColour3") ?? UIColor.blue
        }
        set {
            UserDefaults.standard.setColor(newValue, forKey: "savedColour3")
        }
    }
    
    var savedColour4: UIColor {
        get {
            return UserDefaults.standard.colorForKey("savedColour4") ?? UIColor.yellow
        }
        set {
            UserDefaults.standard.setColor(newValue, forKey: "savedColour4")
        }
    }
    
    var currentPalletIndex = -1
    
    var hue:CGFloat = 0.5
    var saturation:CGFloat = 1.0
    var brightness:CGFloat = 1.0
    
    @IBOutlet weak var colourPickerButton:UIButton!
    
    @IBOutlet weak var savedColourButton1:UIButton!
    @IBOutlet weak var savedColourButton2:UIButton!
    @IBOutlet weak var savedColourButton3:UIButton!
    @IBOutlet weak var savedColourButton4:UIButton!
    
    var currentSavedButton:UIButton?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: FloodFillImageView!
    
    @IBOutlet weak var topStackView:UIStackView!
    @IBOutlet weak var bottomStackView:UIStackView!
    
    @IBOutlet weak var colourSectionContainer:UIView!
    @IBOutlet weak var bottomContainer:UIView!
    
    @IBOutlet weak var constraint:NSLayoutConstraint!
    @IBOutlet weak var leadingConstraint:NSLayoutConstraint!
    @IBOutlet weak var bottomContainerHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var undoLeadingConstraint:NSLayoutConstraint!
    @IBOutlet weak var undoTrailingConstraint:NSLayoutConstraint!
    @IBOutlet weak var undoCenterConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var slider1:CustomSlider!
    @IBOutlet weak var slider2:CustomSlider!
    @IBOutlet weak var slider3:CustomSlider!
    
    @IBOutlet weak var nextButton:UIButton!
    @IBOutlet weak var undoButton:UIButton!
    @IBOutlet weak var ideaButton:UIButton!

    @IBOutlet weak var drawingVW: LayerDrawingView!
    override func viewDidLoad() {
        super.viewDidLoad()
        freeupMemory()
        // Do any additional setup after loading the view, typically from a nib.
        
        imageView.isHidden = true
        self.scrollView.minimumZoomScale = 1.0
        self.scrollView.maximumZoomScale = 20.0
        self.scrollView.decelerationRate = 0.5
        
        imageView.tolorance = 0
        imageView.newcolor = UIColor.red
        setColor(color: .red)
        imageView.showBorder()
        
        constraint.constant = Helper.isIphone() ? 80 : 194
        
        
        savedColourButton1.backgroundColor =  savedColour1
        savedColourButton2.backgroundColor =  savedColour2
        savedColourButton3.backgroundColor =  savedColour3
        savedColourButton4.backgroundColor =  savedColour4
        
        selectCurrentSavedButton(savedColourButton1)
        makeSavedColourButtonsRounded()
        
        if testTemplate != nil {
            
            self.ideaButton.isHidden = true
            self.nextButton.isHidden = true
            
            imageView.sd_setImage(with: testTemplate!.imageUrl as URL!, completed: { (image, error, cacheType, url) in
                self.imageView.image = image
                (ImageManager.sharedManager() as AnyObject).configure(image)
            })
            
        }else{
            
          //  imageView.image = savedTemplate!.image!
          //  (ImageManager.sharedManager() as AnyObject).configure(savedTemplate!.originalImage!)
            
        }
        
        removeUnwantedConstraints()
        
        imageView.completion = {(colour) in
            
            DispatchQueue.main.async(execute: {
                // update some UI
                
                self.currentSavedButton?.backgroundColor = colour
                self.selectCurrentSavedButton(self.currentSavedButton!)
                
                self.imageView.isColourPickingOn = false
                self.colorPickerButtonStyle(false)
                
            });
            
        }
        
  
    }
    
    override func viewDidLayoutSubviews() {
        
        let scrollViewBounds = scrollView.bounds
        let containerViewBounds = imageView.bounds
        
        var scrollViewInsets = UIEdgeInsets.zero
        scrollViewInsets.top = scrollViewBounds.size.height/2.0;
        scrollViewInsets.top -= imageView.bounds.size.height/2.0;
        
        scrollViewInsets.left = scrollViewBounds.size.width/2.0;
        scrollViewInsets.left -= imageView.bounds.size.width/2.0;
        
        scrollViewInsets.bottom = scrollViewBounds.size.height/2.0
        scrollViewInsets.bottom -= imageView.bounds.size.height/2.0;
        scrollViewInsets.bottom += 1
        
        scrollViewInsets.right = scrollViewBounds.size.width/2.0;
        scrollViewInsets.right -= imageView.bounds.size.width/2.0;
        
        scrollView.contentInset = scrollViewInsets
        
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        imageView.newcolor = AppColour.Blue
        setColor(color: AppColour.Blue)
      //
     
    
        if let filePath = savedTemplate?.originalFilePath{
            let path = "file://\(filePath)"
            if  let fileURL = URL.init(string:path ){
                let source = SVGKSourceURL.source(from: fileURL)
                self.setSVGSource(source: source!)
            }
        }

    }
    
    func setColor(color : UIColor){
       self.drawingVW.setFillColor(color: color)
    }
    
    func setSVGSource(source: SVGKSource) {
        _ = SVGKImage.image(with: source, onCompletion: { (loadedImage, parseResult) in
            DispatchQueue.main.async {
                
                self.drawingVW.drawingLayer = loadedImage?.caLayerTree
            }
        })
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if savedTemplate != nil && imageView.image != nil {
            savedTemplate!.replaceImage(imageView.image!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        if savedTemplate != nil && imageView.image != nil {
            savedTemplate!.replaceImage(imageView.image!)
        }
    }
    
    override func applicationWillTerminate() {
        super.applicationWillTerminate()
        if savedTemplate != nil && imageView.image != nil {
            savedTemplate!.replaceImage(imageView.image!)
        }
    }
    
    //Action for single button in pallet
    
    @IBAction func palette(_ button: UIButton) {
        
        for i in 1...8 {
            let button = (topStackView.viewWithTag(i) as! UIButton)
            button.layer.borderWidth = 0
        }
        
        for i in 9..<16 {
            let button = (bottomStackView.viewWithTag(i) as! UIButton)
            button.layer.borderWidth = 0
        }
        
        if button.tag == 16 {
            constraint.constant = Helper.isIphone() ? 80 : 194
            view.setNeedsLayout()
            
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        } else {
            imageView.newcolor = button.backgroundColor!
            setColor(color: button.backgroundColor!)
            button.layer.borderWidth = 2.0
            button.layer.borderColor = UIColor.white.cgColor
            setPalletColourMap(currentPalletIndex, buttonIndex: button.tag)
        }
    }
    
    @IBAction func colourSection(_ button: UIButton) {
        
        if button.tag == 0 {
            
            currentPalletIndex = -1
            
            //print("Width: \(colourSectionContainer.bounds.size.width)")
            
            leadingConstraint.constant = -bottomContainer.bounds.size.width
            view.setNeedsLayout()
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
                
            })
            
            
        }else{
            
            let index = button.tag - 1
            currentPalletIndex = index
            
            let palette = ArtBoard.palettes[index]
            
            applyButtonColour(true, palette: palette)
            applyButtonColour(false, palette: palette)
            
            constraint.constant = 0
            view.setNeedsLayout()
            
            //Select previous colour
            if let buttonIndex = indexFromColourMap(currentPalletIndex) {
                
                if buttonIndex <= 8 {
                    let button = (topStackView.viewWithTag(buttonIndex) as! UIButton)
                    self.palette(button)
                }else{
                    let button = (bottomStackView.viewWithTag(buttonIndex) as! UIButton)
                    self.palette(button)
                }
                
            }else{
                
                let button = (topStackView.viewWithTag(4) as! UIButton)
                self.palette(button)
                
            }
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.view.layoutIfNeeded()
                
            })
            
            
        }
        
    }
    
    @IBAction func savedColoursBackAction(){
        
        leadingConstraint.constant = 0
        view.setNeedsLayout()
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.view.layoutIfNeeded()
            
        })
        
    }
    
    @IBAction func savedColoursPickColourAction(){
        
        imageView.isColourPickingOn = true
        colorPickerButtonStyle(true)
        
    }
    
    func applyButtonColour(_ isTop:Bool, palette:Palette){
        
        if isTop {
            
            for i in 1...8 {
                (topStackView.viewWithTag(i) as! UIButton).backgroundColor = palette.colours![i-1]
                
                if palette.colours![i-1] == imageView.newcolor {
                    let button = (topStackView.viewWithTag(i) as! UIButton)
                    button.layer.borderWidth = 2.0
                    button.layer.borderColor = UIColor.white.cgColor
                }
                
            }
            
        }else{
            
            for i in 9..<16 {
                (bottomStackView.viewWithTag(i) as! UIButton).backgroundColor = palette.colours![i-1]
                
                if palette.colours![i-1] == imageView.newcolor {
                    let button = (bottomStackView.viewWithTag(i) as! UIButton)
                    button.layer.borderWidth = 2.0
                    button.layer.borderColor = UIColor.white.cgColor
                }
                
            }
            
        }
        
    }
    
    @IBAction func sliderValueChanged(_ slider:UISlider){
        
        if slider == slider1 {
            
            hue = CGFloat(slider.value)
            
            slider2.update(hue, saturation: saturation, brightness: brightness)
            slider3.update(hue, saturation: saturation, brightness: brightness)
            
        }else if slider == slider2 {
            
            saturation = CGFloat(slider.value)
            
            slider1.update(hue, saturation: saturation, brightness: brightness)
            slider3.update(hue, saturation: saturation, brightness: brightness)
            
        }else if slider == slider3 {
            
            brightness = CGFloat(slider.value)
            
            slider1.update(hue, saturation: saturation, brightness: brightness)
            slider2.update(hue, saturation: saturation, brightness: brightness)
            
        }
        
        let currentColour = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
        currentSavedButton?.backgroundColor = currentColour
        imageView.newcolor = currentColour
        setColor(color: currentColour)
        if currentSavedButton == savedColourButton1 {
            savedColour1 = currentColour
        }else if currentSavedButton == savedColourButton2 {
            savedColour2 = currentColour
        }else if currentSavedButton == savedColourButton3 {
            savedColour3 = currentColour
        }else if currentSavedButton == savedColourButton4 {
            savedColour4 = currentColour
        }
        
    }
    
    func selectCurrentSavedButton(_ button:UIButton){
        
        if currentSavedButton != nil {
            currentSavedButton?.layer.borderWidth = 0
        }
        
        currentSavedButton = button
        
        currentSavedButton?.layer.borderWidth = 1.0
        currentSavedButton?.layer.borderColor = UIColor.white.cgColor
        
        imageView.newcolor = currentSavedButton?.backgroundColor
        setColor(color: currentSavedButton?.backgroundColor ?? .red)
        let (xH,xS,xB,xA) = currentSavedButton!.backgroundColor!.hsba()
        
        hue = xH
        saturation = xS
        brightness = xB
        
        slider1.value = Float(hue)
        slider2.value = Float(saturation)
        slider3.value = Float(brightness)
        
        slider1.update(hue, saturation: saturation, brightness: brightness)
        slider2.update(hue, saturation: saturation, brightness: brightness)
        slider3.update(hue, saturation: saturation, brightness: brightness)
        
    }
    
    @IBAction func savedButtonAction(_ button:UIButton) {
        
        selectCurrentSavedButton(button)
        
    }
    
    @IBAction func undoButtonAction(_ button:UIButton) {
        
        self.drawingVW.undoAction()
        
        imageView.undoAction()
        
    }
    
    @IBAction func ideasButtonAction(_ button:UIButton) {
        
        Helper.showArtIdeas(savedTemplate!.templateId!.intValue, templateName: savedTemplate!.templateName!)
        
    }
    
    @IBAction func nextButtonAction(_ button:UIButton) {
        
        Helper.locked {
            
            var actionSheet:UIActionSheet?
            
            if self.savedTemplate == nil {
                
                return
                
                //                actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Share", "Save to album", "Delete")
                
            }else{
                
                actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil , otherButtonTitles: "Upload to gallery", "Share", "Save to album", "Clear template", "Delete")
                actionSheet?.destructiveButtonIndex = 5
                
            }
            
            
            
            
            if Helper.isIPad() {
                actionSheet?.show(from: self.nextButton.frame, in: self.view, animated: true)
            }else{
                actionSheet?.show(in: self.view)
            }
            
        }
        
        
    }
    
    func uploadImageToGallery(){
        
        //1. Create the alert controller.
        alert = UIAlertController(title: nil, message: "Give a name", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert!.addTextField(configurationHandler: { (textField) -> Void in
            textField.text = (self.testTemplate != nil) ? self.testTemplate!.templateName : (self.savedTemplate!.templateName ?? "")
        })
        
        alert!.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        //3. Grab the value from the text field, and print it when the user clicks OK.
        alert!.addAction(UIAlertAction(title: "Upload", style: .default, handler: { [weak alert] (action) -> Void in
            let textField = alert!.textFields![0] as UITextField
            
            if textField.isEmpty {
                
                
                DispatchQueue.main.async(execute: {
                    
                    // update some UI
                    self.showDefaultAlert("Opps.", message: "Name cannot be empty.", dismissAction: {
                        
                        self.uploadImageToGallery()
                        
                    })
                    
                });
                
                return
                
            }
            
            DispatchQueue.main.async {
                SVProgressHUD.show(withStatus: "Uploading...")
            }
            
            
            WebServiceAPI.uploadPost(textField.text!, image: self.imageView.image!, templateId: self.savedTemplate!.templateId!.stringValue, success: {
                
                DispatchQueue.main.async {
                    //                    SVProgressHUD.showSuccessWithStatus("Done")
                    SVProgressHUD.dismiss()
                    self.showDefaultAlert(nil, message: "Uploaded.")
                }
                
                }, failure: {
                    
                    DispatchQueue.main.async {
                        //                        SVProgressHUD.showErrorWithStatus("Failed")
                        SVProgressHUD.dismiss()
                        
                        if !WebServiceAPI.hasInternet() {
                            self.showDefaultAlert(nil, message: "No Internet Connection.")
                        }else{
                            self.showDefaultAlert(nil, message: "Failed uploading.")
                        }
                        
                        
                    }
                    
            })
            
            
            }))
        
        if Helper.isIPad() {
            
            // Dismiss the Old
            
            DispatchQueue.main.async(execute: {
                // update some UI
                if let presented = self.presentedViewController {
                    presented.removeFromParentViewController()
                }
            });
            
            alert!.popoverPresentationController?.sourceView = self.view
            alert!.popoverPresentationController?.sourceRect = self.nextButton.frame
            
        }else{
            
        }
        
        // 4. Present the alert.
        
        DispatchQueue.main.async(execute: {
            // update some UI
            self.present(self.alert!, animated: true, completion: nil)
        });
        
        
    }
    
    func colorPickerButtonStyle(_ on:Bool){
        
        colourPickerButton.alpha = on ? 0.5 : 1.0
        
    }
    
    func removeUnwantedConstraints(){
        
        if testTemplate != nil {
            view.removeConstraints([undoLeadingConstraint, undoTrailingConstraint])
            undoCenterConstraint.priority = 1000
        }else{
            view.removeConstraints([undoCenterConstraint])
        }
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
        
    }
    
    func makeSavedColourButtonsRounded(){
        self.savedColourButton1.layer.cornerRadius = Helper.isIphone() ? 12 : 26
        self.savedColourButton2.layer.cornerRadius = Helper.isIphone() ? 12 : 26
        self.savedColourButton3.layer.cornerRadius = Helper.isIphone() ? 12 : 26
        self.savedColourButton4.layer.cornerRadius = Helper.isIphone() ? 12 : 26
    }
    
    func setPalletColourMap(_ paletteIndex:Int, buttonIndex:Int){
        
        let defaults = UserDefaults.standard
        var dictionary: [Int:Int]?
        
        if let data =  defaults.object(forKey: "PalletColourMap") {
            dictionary = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [Int:Int]
        }else{
            dictionary = [Int:Int]()
        }
        
        dictionary![paletteIndex] = buttonIndex
        
        let dataToSave = NSKeyedArchiver.archivedData(withRootObject: dictionary!)
        defaults.set(dataToSave, forKey: "PalletColourMap")
        defaults.synchronize()
        
    }
    
    func indexFromColourMap(_ paletteIndex:Int)->Int?{
        
        let defaults = UserDefaults.standard
        
        if let data =  defaults.object(forKey: "PalletColourMap") {
            var dictionary = NSKeyedUnarchiver.unarchiveObject(with: data as! Data) as? [Int:Int]
            return dictionary?[paletteIndex]
        }else{
            return nil
        }
        
    }
    
    func freeupMemory(){
        
        DispatchQueue.main.async(execute: {
            // update some UI
            let sdImageCache = SDImageCache.shared()
            sdImageCache?.clearMemory()
            sdImageCache?.cleanDisk()
        });
        
        (CCUndoManager.sharedManager() as AnyObject).clear()
        
    }
    
}

extension ArtBoardViewController: UIActionSheetDelegate {
    
    func actionSheet(_ actionSheet: UIActionSheet, didDismissWithButtonIndex buttonIndex: Int) {
        
        //print("\(buttonIndex)")
        
        
        switch (buttonIndex){
            
        case 5:
            
            let alert = UIAlertController(title: "Delete Art", message: "Are you sure you want to delete this art ?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler: { action in
                
                DispatchQueue.main.async(execute: {
                    // update some UI
                    self.savedTemplate?.removeSavedTemplate()
                    self.savedTemplate = nil
                    self.navigationController?.popViewController(animated: true)
                });
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
            
            if let popover = alert.popoverPresentationController {
                popover.sourceView = nextButton
                popover.sourceRect = nextButton.bounds
                popover.permittedArrowDirections = .any
            }
            
            self.present(alert, animated: true, completion: nil)
            
            
            //print("Delete")
            
        case 1:
            
            uploadImageToGallery()
            
            //print("Upload")
            
        case 2:
            if let imageToSave = self.createImage(from: self.drawingVW){
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showShareController(imageToSave)
                
            }
            
            //print("Share")
            
        case 3:
            
            // save
            let alert = UIAlertController(title: "Save Art", message: "Are you sure to save this art to album ?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                
                if let imageSave = self.createImage(from: self.drawingVW){
                    UIImageWriteToSavedPhotosAlbum(imageSave, nil, nil, nil)
                    print("Success")
                }
                
//                if let imageToSave = self.imageView.image {
//
//
//                    //saro
//                    //saveimage
//                    UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil)
//                }
                
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
            
            if let popover = alert.popoverPresentationController {
                popover.sourceView = nextButton
                popover.sourceRect = nextButton.bounds
                popover.permittedArrowDirections = .any
            }
            
            self.present(alert, animated: true, completion: nil)
            
            //print("Save in Photo Gallery")
            
        case 4:
            
            
            if self.savedTemplate == nil {
                
            }else{
                
                let alert = UIAlertController(title: "Clear Template", message: "Are you sure you want to clear this template ?", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler: { action in
                    
                    DispatchQueue.main.async(execute: {
                        // update some UI
                        self.imageView.image = self.savedTemplate?.clearTemaplete()
                        //Saro
                    //    (ImageManager.sharedManager() as AnyObject).configure(self.savedTemplate!.image!)
                    });
                    
                }))
                
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
                
                if let popover = alert.popoverPresentationController {
                    popover.sourceView = nextButton
                    popover.sourceRect = nextButton.bounds
                    popover.permittedArrowDirections = .any
                }
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
            //print("Clear Temlpate")
            
        default:
            print("Cancel")
            
        }
        
    }
    
    func createImage(from view: UIView) -> UIImage? {
        
        UIGraphicsBeginImageContext(view.layer.bounds.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let viewImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return viewImage
    }
    
}

extension ArtBoardViewController: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return imageView
        
    }
    
}


extension UIColor {
    
    func rgb() -> (red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat)? {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = fRed * 255.0
            let iGreen = fGreen * 255.0
            let iBlue = fBlue * 255.0
            let iAlpha = fAlpha * 255.0
            
            return (red:iRed, green:iGreen, blue:iBlue, alpha:iAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
    
    func hsba()-> (h: CGFloat, s: CGFloat, b: CGFloat, a: CGFloat) {
        var hsba: (h: CGFloat, s: CGFloat, b: CGFloat, a: CGFloat) = (0, 0, 0, 0)
        self.getHue(&(hsba.h), saturation: &(hsba.s), brightness: &(hsba.b), alpha: &(hsba.a))
        return hsba
    }
    
}

//MARK: Colour Memory
extension UserDefaults {
    func setColor(_ value: UIColor?, forKey: String) {
        guard let value = value else {
            set(nil, forKey:  forKey)
            return
        }
        set(NSKeyedArchiver.archivedData(withRootObject: value), forKey: forKey)
    }
    func colorForKey(_ key:String) -> UIColor? {
        guard let data = data(forKey: key), let color = NSKeyedUnarchiver.unarchiveObject(with: data) as? UIColor
            else { return nil }
        return color
    }
}
