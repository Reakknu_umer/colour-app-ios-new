//
//  CCBaseViewController.swift
//  Colour Connection
//
//  Created  on 2/9/17.
//  at Eight25Media
//

import UIKit

class CCBaseViewController: UIViewController {
    
    //MARK: Typealias
    
    typealias RefreshAfterLoginContent = (()->())
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    //MARK: Properties 
    
    var refreshAfterLoginContent:RefreshAfterLoginContent?
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillTerminate), name: Notification.Name.UIApplicationWillTerminate, object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func applicationWillTerminate() {
        
    }
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        
        if navigationController != nil {
            
            let firstController = navigationController!.viewControllers.count == 1
            
            if firstController {
                navigationController?.dismiss(animated: true, completion: {
                })
            } else {
                navigationController!.popViewController(animated: true)
            }
        } else {
            dismiss(animated: true, completion: {
            })
        }
    }
    
    //MARK: - Private Functions 
    
    func refreshAfterLogin(_ content:@escaping RefreshAfterLoginContent){
        refreshAfterLoginContent = content
    }
    
    func observeLogin() {
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: InternalNotification.UserLoggedIn), object: nil, queue: nil) { (notification) in
            
            self.refreshAfterLoginContent?()
        }
    }
    
    //MARK: Helpers 
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    func showDefaultAlert(_ title:String?, message:String?){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showDefaultAlert(_ title:String?, message:String?, dismissAction:@escaping (()->())){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: {action in
            DispatchQueue.main.async(execute: {
                // update some UI
                dismissAction()
            });
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //    override func prefersStatusBarHidden() -> Bool {
    //        return true
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
