//
//  SettingsViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

class SettingsViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var buttonContainer:UIView!
    
    @IBOutlet weak var adminButton:UIButton!
    @IBOutlet weak var logoutButton:UIButton!
    
    
    @IBOutlet weak var editProfileButton:UIButton!
    @IBOutlet weak var subscriptionButton:UIButton!
    
    @IBOutlet weak var findFriendsTopConstraint:NSLayoutConstraint!
    @IBOutlet weak var rateUsTopConstraint:NSLayoutConstraint!
    @IBOutlet weak var logoutButtonTopConstraint:NSLayoutConstraint!
    
    //MARK: Properties 
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        style()
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func editProfileButtonAction(_ sender: AnyObject) {
        
        let viewcontroller = StoryboardId.EditProfileView.instantiateViewController(StoryboardName.Settings) as! EditProfileViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    @IBAction func findFriendsButtonAction(_ sender: AnyObject) {
        
        let viewcontroller = StoryboardId.SearchFriendsView.instantiateViewController(StoryboardName.Settings) as! SearchFriendsViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    @IBAction func subscriptionButtonAction(_ sender: AnyObject) {
        
        let viewcontroller = StoryboardId.SubscriptionView.instantiateViewController(StoryboardName.Subscription) as! SubscriptionViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    @IBAction func rateusButtonAction(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: "Rate Colour Connection", message: "If you like our app, please take a moment to rate it in App Store.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Rate", style: UIAlertActionStyle.default, handler: { action in
            
            DispatchQueue.main.async(execute: {
                let urlString = "itms-apps://itunes.apple.com/app/\(Config.appStoreId)"
                UIApplication.shared.openURL(URL(string: urlString)!)
            })
            
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func aboutUsButtonAction(_ sender: AnyObject) {
        
        let viewcontroller = StoryboardId.AboutUsView.instantiateViewController(StoryboardName.Settings) as! AboutUsViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    @IBAction func termsButtonAction(_ sender: AnyObject) {
        
        let viewcontroller = StoryboardId.TermsView.instantiateViewController(StoryboardName.Settings) as! TermsViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    @IBAction func adminTestButtonAction(_ sender: AnyObject) {
        
        let controller = StoryboardId.AdminTestView.instantiateViewController(StoryboardName.Test)
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func logoutButtonAction(_ sender: AnyObject) {
        
        if UserSession.sharedSession.isValid {
            
            let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout ?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler: { action in
                
                DispatchQueue.main.async(execute: {
                    // update some UI
                    
                    WebServiceAPI.userLogout({
                        
                        let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        
                        
                        DispatchQueue.main.async(execute: {
                            // update some UI
                            self.discardCurrentUserData()
                            appDelegate?.showExploreAfterLogoutScreen()
                        });
                        
                        }, failure: {
                            
                    })
                    
                });
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
            
            if let popover = alert.popoverPresentationController {
                popover.sourceView = logoutButton
                popover.sourceRect = logoutButton.bounds
                popover.permittedArrowDirections = .any
            }
            
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            
            UserSession.sharedSession.mainViewController?.showLogin()
            
        }
        
    }
    
    //MARK: - Private Functions 
    
    //MARK: Helpers 
    
    func style(){
        
        let isLoggedIn = UserSession.sharedSession.isValid
        
        if isLoggedIn {
            
            
            findFriendsTopConstraint.constant = Helper.isIPad() ? 66 : 44
            rateUsTopConstraint.constant = Helper.isIPad() ? 66 : 44
            
            if UserSession.sharedSession.currentUser!.isAdmin {
                logoutButtonTopConstraint.constant = Helper.isIPad() ? 66 : 44
                adminButton.isHidden = false
            }else{
                logoutButtonTopConstraint.constant = 0
                adminButton.isHidden = true
            }
            
            
            editProfileButton.isHidden = false
            subscriptionButton.isHidden = false
            
            
            self.buttonContainer.setNeedsLayout()
            
            self.logoutButton.setTitle("Logout", for: UIControlState())
            
        }else{
            
            editProfileButton.isHidden = true
            subscriptionButton.isHidden = true
            adminButton.isHidden = true
            
            findFriendsTopConstraint.constant = 0
            rateUsTopConstraint.constant = 0
            logoutButtonTopConstraint.constant = 0
            
            self.buttonContainer.setNeedsLayout()
            
            self.logoutButton.setTitle("LogIn", for: UIControlState())
            
        }
        
        
        if Helper.isIPad() {
            
            for view in buttonContainer.subviews {
                
                if let button = view as? UIButton {
                    button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 44, bottom: 0, right: 0)
                }
                
            }
            
        }
        
        self.view.layoutSubviews()
        self.view.setNeedsUpdateConstraints()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func discardCurrentUserData(){
        
        let templates = SavedTemplate.allTemapltes()
        
        for template in templates {
            template.discardFiles()
            template.removeSavedTemplate()
        }
        
    }
    
}
