//
//  RegisterViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD

class RegisterViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var emailAddressTextField:UITextField!
    @IBOutlet weak var confirmEmailAddressTextField:UITextField!
    @IBOutlet weak var usernameTextField:UITextField!
    @IBOutlet weak var passwordTextField:UITextField!
    @IBOutlet weak var confirmPasswordTextField:UITextField!
    
    //MARK: Properties 
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func registerButtonAction(_ sender: AnyObject) {
        
        self.view.endEditing(true)
        
        if !validateForm() {
            return
        }
        
        let emailAddress = emailAddressTextField.text!
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        WebServiceAPI.userRegistration(username, email: emailAddress, password: password, success: { 
            
            DispatchQueue.main.async(execute: {
                // update some UI
                
                let alert = UIAlertController(title: "Congratulations", message: "Your account created successfully.", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: { action in
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            });
            
            //            UserSession.sharedSession.mainViewController?.hideLogin({
            //                
            //                NSNotificationCenter.defaultCenter().postNotificationName(InternalNotification.UserLoggedIn, object: nil)
            //                
            //            })
            
            }, failure: { errorMessage in
                
                DispatchQueue.main.async(execute: {
                    SVProgressHUD.dismiss()
                    
                    if !WebServiceAPI.hasInternet() {
                        self.showDefaultAlert(nil, message: "No Internet Connection.")
                    }else{
                        self.showDefaultAlert(nil, message: errorMessage)
                    }
                    
                });
                
        })
        
    }
    
    //MARK: - Private Functions 
    
    func validateForm()->Bool{
        
        let invalidCharacterSet:NSMutableCharacterSet = NSMutableCharacterSet(charactersIn: " ")
        invalidCharacterSet.formUnion(with: CharacterSet.symbols)
        
        if emailAddressTextField.isEmpty {
            self.showDefaultAlert(nil, message: "All fields are required.")
            return false
        }else if emailAddressTextField.isValidEmail == false {
            self.showDefaultAlert(nil, message: "Invalid email address.")
            return false
        }else if confirmEmailAddressTextField.isEmpty {
            self.showDefaultAlert(nil, message: "All fields are required.")
            return false
        }else if (confirmEmailAddressTextField.text! != emailAddressTextField.text! ) {
            self.showDefaultAlert(nil, message: "Email address mismatch.")
            return false
        }else if usernameTextField.isEmpty {
            self.showDefaultAlert(nil, message: "All fields are required.")
            return false
        }else if usernameTextField.text!.rangeOfCharacter(from: invalidCharacterSet as CharacterSet) != nil {
            self.showDefaultAlert(nil, message: "Invalid Username.")
            return false
        }else  if passwordTextField.isEmpty {
            self.showDefaultAlert(nil, message: "All fields are required.")
            return false
        }else if passwordTextField.text!.characters.count < 6{
            self.showDefaultAlert(nil, message: "Password must be at least 6 characters.")
            return false
        }else if (passwordTextField.text! != confirmPasswordTextField.text! ) {
            self.showDefaultAlert(nil, message: "Password mismatch.")
            return false
        }
        
        return true
    }
    
    //MARK: Helpers 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
