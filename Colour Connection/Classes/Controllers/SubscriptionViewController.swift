//
//  SubscriptionViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD

class SubscriptionViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var topLabel:UILabel!
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var alertLabel:UILabel!
    
    //MARK: Properties
    
    var dataSource = [(name:String,price:String, product:AnyObject)]()
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 68
        tableview.hideEmptyCells()
        loadInApp()
        loadSubscriptionInfo()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateAlertText()
        
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func updateAlertText(){
        
        let attributedString = NSMutableAttributedString()
        
        let part1 = NSMutableAttributedString(string: "Subscription", attributes: [NSForegroundColorAttributeName:AppColour.DarkGray, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(15.0))!])
        
        attributedString.append(part1)
        
        if UserSession.sharedSession.currentUser?.isSubscribed == true {
            
            if UserSession.sharedSession.currentUser?.isSubscriptionExpired == true {
                
                let part2 = NSMutableAttributedString(string: " Expired!", attributes: [NSForegroundColorAttributeName:AppColour.Blue, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(14.0))!])
                
                attributedString.append(part2)
                
            }else{
                
                let dateFormatter = DateFormatter()
                
                dateFormatter.dateFormat = "dd/MM/yyyy"
                let convertedDate: String = dateFormatter.string(from: UserSession.sharedSession.currentUser!.packageExpirationDate! as Date)
                
                let part2 = NSMutableAttributedString(string: " Expire Date: \(convertedDate)", attributes: [NSForegroundColorAttributeName:AppColour.Blue, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(14.0))!])
                
                attributedString.append(part2)
                
            }

}
        
        topLabel.attributedText = attributedString
        
    }
    
    func loadSubscriptionInfo(){
        
        WebServiceAPI.subscriptionExpireDate({ 
            
            DispatchQueue.main.async(execute: {
                // update some UI
                self.updateAlertText()
                self.tableview.reloadData()
            });
            
            }, failure: {
                
        })
        
    }
    
    func loadInApp(){
        
        SVProgressHUD.show()
        dataSource.removeAll()
        
        if (IAPShare.sharedHelper().iap == nil) {
            
            let dataSet = NSSet(objects: "au.com.elegantmedia.colourconnection.tc1", "au.com.elegantmedia.colourconnection.tc2")
            IAPShare.sharedHelper().iap = IAPHelper(productIdentifiers: dataSet as Set<NSObject>)
            
        }
        
        IAPShare.sharedHelper().iap.production = true
        
        IAPShare.sharedHelper().iap.requestProducts { (request, response) in
            
            //print("Completed requesting products.")
            
            for product in IAPShare.sharedHelper().iap.products {
                
                let inAppName = (product as! SKProduct).localizedTitle
                let price = IAPShare.sharedHelper().iap .getLocalePrice((product as! SKProduct))
                
                self.dataSource.append((name:inAppName, price:price!, product: product as AnyObject))

}
            
            self.tableview.reloadData()
            SVProgressHUD.dismiss()
            
        }
        
    }
    
    func purchase(_ product: SKProduct, indexPath:IndexPath){
        
        DispatchQueue.main.async(execute: {
            // update some UI
            SVProgressHUD.show()
        });
        
        IAPShare.sharedHelper().iap.buyProduct(product) { (transaction) in
            //print("Transaction: \(transaction)")
            
            DispatchQueue.main.async(execute: {
                // update some UI
                
                SVProgressHUD.dismiss()
                
                if let error = transaction?.error as? SKError {
                    
                    if error._domain == SKErrorDomain {
                        // handle all possible errors
                        switch (error.errorCode) {
                        case SKError.unknown.rawValue:
                            //print("Unknown error")
                            self.showDefaultAlert(nil, message: "Unknown error")
                            return
                            break;
                        case SKError.clientInvalid.rawValue:
                            //print("client is not allowed to issue the request")
                            self.showDefaultAlert("Error", message: "Client is not allowed to issue the request.")
                            return
                            break;
                        case SKError.paymentCancelled.rawValue:
                            //print("user cancelled the request")
                            self.showDefaultAlert("Error", message: "User cancelled the request.")
                            return
                            break;
                        case SKError.paymentInvalid.rawValue:
                            //print("purchase identifier was invalid")
                            self.showDefaultAlert(nil, message: "Unknown error")
                            return
                            break;
                        case SKError.paymentNotAllowed.rawValue:
                            //print("this device is not allowed to make the payment")
                            self.showDefaultAlert("Error", message: "This device is not allowed to make the payment.")
                            return
                            break;
                        default:
                            //print("Default")
                            self.showDefaultAlert(nil, message: "Unknown error")
                            return
                            break;
                        }
                    }
                    
                }else{
                    self.performPurchase(indexPath)
                }

});

}

}
    
    func performPurchase(_ indexPath:IndexPath){
        
        if indexPath.row == 0 {
            
            WebServiceAPI.subscribeForMonth({
                DispatchQueue.main.async(execute: {
                    // update some UI
                    self.updateAlertText()
                    self.tableview.reloadData()
                });
                }, failure: {
                    
            })
            
        }else{
            
            WebServiceAPI.subscribeForYear({
                DispatchQueue.main.async(execute: {
                    // update some UI
                    self.updateAlertText()
                    self.tableview.reloadData()
                });
                }, failure: {
                    
            })
            
        }

}
    
    //MARK: Helpers
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension SubscriptionViewController:UITableViewDataSource {

func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubscriptionCell") as! SubscriptionCell
        
        cell.configureCell(dataSource[indexPath.row].name, price: dataSource[indexPath.row].price) {
            
            let product = self.dataSource[indexPath.row].product as! SKProduct
            self.purchase(product, indexPath: indexPath)
            
        }
        
        if indexPath.row == 0 {
            
            cell.subscriptioButton.isEnabled = !UserSession.sharedSession.currentUser!.isMonthlyActive
            cell.subscriptioButton.alpha = UserSession.sharedSession.currentUser!.isMonthlyActive ? 0.5 :  1.0
            
        }else if indexPath.row == 1 {
            
            cell.subscriptioButton.isEnabled = !UserSession.sharedSession.currentUser!.isYearlyActive
            cell.subscriptioButton.alpha = UserSession.sharedSession.currentUser!.isYearlyActive ? 0.5 :  1.0
            
        }
        
        //print("Button Enable: \(cell.subscriptioButton.enabled) for indexpath: \(indexPath.row)")
        
        return cell
        
    }
    
}
