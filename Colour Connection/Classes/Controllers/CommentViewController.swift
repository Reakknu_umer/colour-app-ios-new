//
//  CommentViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD

@objc protocol CommentViewDelegate {
    @objc optional func deleted(_ post:CCPost)
}

class CommentViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var commentBoxBottomConstraint:NSLayoutConstraint!
    @IBOutlet weak var commentTextView:CCCustomTextView!
    @IBOutlet weak var optionButton:UIButton!
    
    //MARK: Properties
    
    var post:CCPost!
    var template:CCTemplate?
    var delegate: CommentViewDelegate? {
        
        didSet{
            //print("Did set delegate: \(delegate)")
        }
        
    }
    //    var comments:[CCComment]?
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.hideEmptyCells()
        
        addRefrshController()
        addKeyboardObservers()
        
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 140
        commentTextView.layer.borderColor = UIColor(hex: 0x9a9a9a).cgColor
        commentTextView.layer.borderWidth = 1.0
        commentTextView.placeholder = "Comment"
        
        
        loadComments()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func moreButtonAction(_ button:UIButton){
        
        hideKeyboard()
        
        var actionSheet:UIActionSheet?
        
        if post.userId == UserSession.sharedSession.currentUser!.userId {
            actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Save to album", "Share", "Related work", "Colour it", "Delete")
            actionSheet?.destructiveButtonIndex =  5
        } else {
            actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Save to album", "Share", "Related work", "Colour it")
        }
        
        if Helper.isIPad() {
            actionSheet?.show(from: self.optionButton.frame, in: self.view, animated: true)
        } else {
            actionSheet?.show(in: self.view)
        }
    }
    
    //MARK: - Private Functions 
    
    func addRefrshController(){
        
        let refreshControlT = UIRefreshControl()
        refreshControlT.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableview.addSubview(refreshControlT)
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        
        // Do your job, when done:
        control.endRefreshing()
        loadComments()
        
    }
    
    func loadComments(){
        
        WebServiceAPI.listComments(post.postId, success: { (comments, post, template) in
            
            self.post.comments = comments
            self.template = template
            
            if self.post == nil {
                
            }else{
                self.post.templateName = post.templateName
                self.post.templateId = post.templateId
            }
            
            
            self.tableview.reloadData()
            
            }, failure: {
                
        })
        
    }
    
    func comment() {
        
        if commentTextView.text.characters.count > 0 {
            
            commentTextView.resignFirstResponder()
            
            let commentText = commentTextView.text
            
            DispatchQueue.main.async(execute: {
                // update some UI
                self.commentTextView.text = ""
                self.commentTextView.layoutSubviews()
                self.commentTextView.placeholder = "Comment"
            });
            
            WebServiceAPI.addComment(self.post.postId, comment: commentText!, success: { (post) in
                
                self.post.comments = post.comments
                self.post.commentCount = post.comments.count
                self.tableview.reloadData()
                self.scrollToBottomAnimated(true)
                
                }, failure: {
                    
            })
            
        }
        
    }
    
    //MARK: Helpers 
    
    func hideKeyboard(){
        
        if commentTextView.isFirstResponder {
            commentTextView.resignFirstResponder()
        }
        
    }
    
    func addKeyboardObservers(){
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(CommentViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(CommentViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        //        if isFromPictureView {
        commentBoxBottomConstraint.constant = keyboardHeight
        //        } else {
        //            commentBoxBottomConstraint.constant = keyboardHeight - 65
        //        }
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.view.layoutIfNeeded()
            
        }) { (finished) in
            self.scrollToBottomAnimated(true)
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        
        let userInfo = notification.userInfo!
        
        let keyboardHeight = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
        
        commentBoxBottomConstraint.constant = 0
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(), animations: {
            self.view.layoutIfNeeded()
        }) { (finished) in
            
        }
        
    }
    
    func scrollToBottomAnimated(_ animated:Bool){
        
        let section = 0
        let row = commentCount()
        let scrollIndexPath = IndexPath(row: row, section: section)
        self.tableview.scrollToRow(at: scrollIndexPath, at: UITableViewScrollPosition.bottom, animated: animated)
        
    }
    
    func commentCount()->Int {
        return self.post.comments.count
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CommentViewController: UIActionSheetDelegate {
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        
        //print("\(buttonIndex)")
        
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
            
        case 1:
            //print("Save to album")
            
            // save
            let alert = UIAlertController(title: "Save Art", message: "Are you sure to save this art to album ?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                
                let cell = self.tableview.cellForRow(at: IndexPath(row: 0, section: 0)) as! FeedDefaultCell
                
                
                if let imageToSave = cell.mainImageView.image {
                    UIImageWriteToSavedPhotosAlbum(imageToSave, nil, nil, nil)
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
            
            if let popover = alert.popoverPresentationController {
                popover.sourceView = optionButton
                popover.sourceRect = optionButton.bounds
                popover.permittedArrowDirections = .any
            }
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
            })
            
            
            
        case 2:
            //print("Share")
            
            let cell = tableview.cellForRow(at: IndexPath(row: 0, section: 0)) as! FeedDefaultCell
            
            if let imageToSave = cell.mainImageView.image {
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.showShareController(imageToSave)
                
            }
            
            
        case 3:
            //print("Related work")
            
            Helper.showArtIdeas(post.templateId, templateName: post.templateName)
            
        case 4:
            //print("Colour it")
            
            if let savedTemplate = SavedTemplate.template(post.templateId) {
                
                if !savedTemplate.isFileExist() {
                    
                    DispatchQueue.main.async{
                        //print("File is being downloaded.")
                        SVProgressHUD.show(withStatus: "Downloading...")
                    }
                    
                    Downloader.load(savedTemplate, completion: { (success) in
                        
                        if success {
                            
                            DispatchQueue.main.async{
                                SVProgressHUD.dismiss()
                                Helper.showArtBoard(savedTemplate)
                            }
                            
                        }else{
                            
                            DispatchQueue.main.async{
                                //                                SVProgressHUD.showErrorWithStatus("Downloading failed!")
                                SVProgressHUD.dismiss()
                                self.showDefaultAlert(nil, message: "Downloading failed!")
                            }
                            
                        }
                        
                    })
                    
                }else{
                    
                    DispatchQueue.main.async{
                        Helper.showArtBoard(savedTemplate)
                    }
                    
                }
                
            }else{
                
                if template != nil {
                    
                    let controller = StoryboardId.PreviewArtworkView.instantiateViewController(StoryboardName.DownloadMore) as! PreviewArtworkViewController
                    controller.template = template
                    
                    DispatchQueue.main.async(execute: {
                        // update some UI
                        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(controller)
                    });
                    
                }
                
                
                //                dispatch_async(dispatch_get_main_queue()){
                //                    //print("File is being downloaded.")
                //                    SVProgressHUD.showWithStatus("Downloading...")
                //                }
                //                
                //                WebServiceAPI.template(post.templateId, success: { (template) in
                //                    
                //                    let savedTemplate = SavedTemplate.addNewSavedTemplate(template)
                //                    
                //                    Downloader.load(savedTemplate, completion: { (success) in
                //                        
                //                        if success {
                //                            
                //                            dispatch_async(dispatch_get_main_queue()){
                //                                SVProgressHUD.dismiss()
                //                                Helper.showArtBoard(savedTemplate)
                //                            }
                //                            
                //                        }else{
                //                            
                //                            dispatch_async(dispatch_get_main_queue()){
                //                                //                                SVProgressHUD.showErrorWithStatus("Downloading failed!")
                //                                SVProgressHUD.dismiss()
                //                                self.showDefaultAlert(nil, message: "Downloading failed!")
                //                            }
                //                            
                //                        }
                //                        
                //                        
                //                    })
                //                    
                //                    }, failure: {
                //                        
                //                        dispatch_async(dispatch_get_main_queue()){
                //                            SVProgressHUD.dismiss()
                //                            
                //                            if !WebServiceAPI.hasInternet() {
                //                                self.showDefaultAlert(nil, message: "No Internet Connection.")
                //                            }else{
                //                                self.showDefaultAlert(nil, message: "Downloading failed!")
                //                            }
                //                            
                //                        }
                //                        
                //                })
                
            }
            
            
            
            
        case 5:
            //print("Delete")
            
            let alert = UIAlertController(title: "Delete Art", message: "Are you sure you want to delete this art ?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.destructive, handler: { action in
                    self.deletePost()
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
            
            if let popover = alert.popoverPresentationController {
                popover.sourceView = optionButton
                popover.sourceRect = optionButton.bounds
                popover.permittedArrowDirections = .any
            }
            
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true, completion: nil)
                })
            
            
            
  
        default:
            print("Colour it")
            
        }
        
    }
    
    
    func deletePost() {
        WebServiceAPI.deletePost(post.postId, success: {
            
            DispatchQueue.main.async(execute: {
                // update some UI
                
                SVProgressHUD.dismiss()
                
                //print("Delegate: \(self.delegate)")
                //print("Post: \(self.post)")
                
                self.showDefaultAlert(nil, message: "Post deleted successfully!", dismissAction: {
                    self.delegate?.deleted?(self.post)
                    self.navigationController?.popViewController(animated: true)
                    
                })
                
                
            });
            
            
            
            }, failure: {
                
                SVProgressHUD.dismiss()
                
                if !WebServiceAPI.hasInternet() {
                    self.showDefaultAlert(nil, message: "No Internet Connection.")
                }else{
                    self.showDefaultAlert(nil, message: "Post not deleted!")
                }
                
        })
    }
    
    
}

extension CommentViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //print("count: \(1 + commentCount())")
        
        return 1 + commentCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let isFirstCell = (indexPath.row == 0)
        
        if isFirstCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDefaultCell") as! FeedDefaultCell
            
            cell.mainImageView.isTouchEnabled = false
            
            cell.configureCell(self.post, action: { (action) in
                
                
                if action == .like {
                    
                    if !self.post.isLiked {
                        
                        
                        self.post.isLiked = true
                        self.post.likeCount = self.post.likeCount + 1
                        
                        self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                        
                        WebServiceAPI.likePost(self.post.postId, success: { currentCount in
                            
                            self.post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }else{
                        
                        self.post.isLiked = false
                        self.post.likeCount = self.post.likeCount - 1
                        
                        self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                        
                        WebServiceAPI.unlikePost(self.post.postId, success: { currentCount in
                            
                            self.post.likeCount = currentCount
                            
                            DispatchQueue.main.async(execute: {
                                self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                            });
                            
                            }, failure: {
                                
                        })
                        
                    }
                    
                }else if action == .comment {
                    
                    
                    
                    
                }else if action == .share {
                    
                    let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! FeedDefaultCell
                    
                    if let imageToSave = cell.mainImageView.image {
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.showShareController(imageToSave)
                        
                    }
                    
                }else if action == .favourite{
                    
                    if self.post.isFavourite {
                        
                        self.post.isFavourite = false
                        self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                        
                        WebServiceAPI.unfavouritePost(self.post.postId, success: {
                            
                            }, failure: {
                                
                        })
                        
                    }else{
                        
                        self.post.isFavourite = true
                        self.tableView(self.tableview, willDisplay: cell, forRowAt: indexPath)
                        
                        WebServiceAPI.favouritePost(self.post.postId, success: { 
                            
                            }, failure: {
                                
                        })
                        
                    }
                    
                }
                
            })
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
            let comment = self.post.comments[indexPath.row - 1]
            
            cell.configureCell(comment)
            
            return cell
            
        }
        
    }
    
}

extension CommentViewController:UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let cell = cell as! FeedDefaultCell
            cell.updateVariableContent(self.post)
            
        }
        
    }
    
}

extension CommentViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            comment()
            return false
        }
        return true
    }
    
}
