//
//  NotificationInfoViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit

enum NotificationType {
    case like
    case comment
    case `default`
}

class NotificationInfoViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets 
    
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var navigationTitleLabel:UILabel!
    
    //MARK: Properties
    
    var notificationType = NotificationType.default
    
    var notifiactions = [CCNotification]() {
        
        didSet{
            
            if isViewLoaded {
                
                tableview.reloadData()
                
            }
            
        }
        
    }
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.estimatedRowHeight = 68
        
        tableview.hideEmptyCells()
        
        loadNotifications()
        addRefrshController()
        
        // Do any additional setup after loading the view.
        addObserver()
    }
    
    //MARK: Segue
    //MARK: IBAction 
    //MARK: - Private Functions 
    
    func loadNotifications(){
        
        if notificationType == .like {
            
            navigationTitleLabel.text = "Likes"
            
            WebServiceAPI.unreadLikeNotifications({ (notifiactions) in
                self.notifiactions = notifiactions
                
                WebServiceAPI.readLikeNotifications({
                    
                    }, failure: {
                        
                })
                
                }, failure: {
                    
            })
            
            
            
        }else if notificationType == .comment {
            
            navigationTitleLabel.text = "Comments"
            
            WebServiceAPI.unreadCommentNotifications({ (notifiactions) in
                self.notifiactions = notifiactions
                
                WebServiceAPI.readCommentNotifications({
                    
                    }, failure: {
                        
                })
                
                }, failure: {
                    
            })
            
            
            
        }else{
            
            navigationTitleLabel.text = "Notifications"
            
            WebServiceAPI.unreadFollowingNotifications({ (notifiactions) in
                self.notifiactions = notifiactions
                
                WebServiceAPI.readFollowingNotifciations({
                    
                    }, failure: {
                        
                })
                
                }, failure: {
                    
            })
            
            
            
        }
        
    }
    
    func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(NotificationInfoViewController.loadNotifications), name: NSNotification.Name(rawValue: "NEW_NOTIFICATION"), object: nil)
    }
    
    //MARK: Helpers 
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        tableview.addSubview(refreshControl)
        tableview.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadNotifications()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showCommentScreen(_ notification:CCNotification){
        
        WebServiceAPI.listComments(notification.postId, success: { (comments, post, template) in
            
            let commentsViewController = StoryboardId.CommentView.instantiateViewController(StoryboardName.Explore) as! CommentViewController
            commentsViewController.post = post
            
            UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(commentsViewController)
            
            }, failure: {
                
                
                
        })
        
    }
    
}

extension NotificationInfoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if notificationType == .default {
            
            let nofication = notifiactions[indexPath.row]
            Helper.showProfile(nofication.userId)
            
        }else{
            
            let nofication = notifiactions[indexPath.row]
            self.showCommentScreen(nofication)
            
        }
        
        
    }
    
}

extension NotificationInfoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifiactions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let nofication = notifiactions[indexPath.row]
        
        if notificationType == .like {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationInfoCellTypeA", for: indexPath) as! NotificationInfoCellTypeA
            
            cell.configureCell(nofication, type: notificationType){ didTapOnUsername in
                
                if didTapOnUsername {
                    Helper.showProfile(nofication.userId)
                }else{
                    self.showCommentScreen(nofication)
                }
                
            }
            
            return cell
            
        }else if notificationType == .comment {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationInfoCellTypeA", for: indexPath) as! NotificationInfoCellTypeA
            
            cell.configureCell(nofication, type: notificationType){ didTapOnUsername in
                
                if didTapOnUsername {
                    Helper.showProfile(nofication.userId)
                }else{
                    self.showCommentScreen(nofication)
                }
                
            }
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationInfoCellTypeB", for: indexPath) as! NotificationInfoCellTypeB
            
            cell.configureCell(nofication)
            
            return cell
            
        }
        
    }
    
    //    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    //        let height = Helper.isIphone() ? 68.0 : 114.0
    //        return CGFloat(height)
    //    }
    
}
