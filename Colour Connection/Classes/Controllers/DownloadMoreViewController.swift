//
//  DownloadMoreViewController.swift
//  Colour Connection
//
//  Created  on 1/20/17.
//  at Eight25Media
//

import UIKit
import SVProgressHUD


class DownloadMoreViewController: CCBaseViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var floralButton:UIButton!
    @IBOutlet weak var peopleButton:UIButton!
    @IBOutlet weak var shapeButton:UIButton!
    @IBOutlet weak var houseButton:UIButton!
    
    @IBOutlet weak var templateCollectionView:UICollectionView!
    @IBOutlet weak var loginRequiredView:UIView!
    
    //MARK: Properties
    
    var categories = [CCCategory]()
    var selectedCategoryIndex = 0
    
    //MARK: Computed Properties
    
    var feedMiniCellSize:CGSize {
        
        get{
            
            let width = (templateCollectionView.frame.width - 24) / 2.0
            let imageWidth = width - 16
            let height = imageWidth + 68 + (Helper.isIPad() ? 32 : 0 )
            //60
            return CGSize(width: width, height: height)
            
        }
        
    }
    
    //    var categoryCellSize:CGSize {
    //        
    //        get{
    //            
    //            let width = (templateCollectionView.frame.width / 4.0)
    //            return CGSizeMake(width, 44.0)
    //            
    //        }
    //        
    //    }
    
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addRefrshController()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserSession.sharedSession.isValid {
            loadContent()
            self.loginRequiredView.isHidden = true
        }else{
            self.loginRequiredView.isHidden = false
        }
        
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func categoryButtonAction(_ button: UIButton) {
        
        if !UserSession.sharedSession.isValid {
            return
        }
        
        selectedCategoryIndex = button.tag
        
        updateStyle()
        
        templateCollectionView.reloadData()
        
    }
    
    @IBAction func loginButtonAction(_ sender: AnyObject) {
        
        UserSession.sharedSession.mainViewController?.showLogin()
        
    }
    
    //MARK: - Private Functions 
    
    func loadContent(){
        
        WebServiceAPI.listCategories({ (categories) in
            
            self.categories = categories
            self.updateStyle()
            self.templateCollectionView.reloadData()
            
            }, failure: {
                
        })
        
    }
    
    //MARK: Helpers 
    
    func updateStyle(){
        
        let darkColour = UIColor(hex: 0x4d4d4d)
        let blueColour = UIColor(hex: 0x019fe9)
        
        //019fe9
        
        floralButton.backgroundColor = UIColor.clear
        floralButton.tintColor = darkColour
        peopleButton.backgroundColor = UIColor.clear
        peopleButton.tintColor = darkColour
        shapeButton.backgroundColor = UIColor.clear
        shapeButton.tintColor = darkColour
        houseButton.backgroundColor = UIColor.clear
        houseButton.tintColor = darkColour
        
        switch selectedCategoryIndex {
            
        case 0:
            floralButton.backgroundColor = blueColour
            floralButton.tintColor = UIColor.white
        case 1:
            peopleButton.backgroundColor = blueColour
            peopleButton.tintColor = UIColor.white
        case 2:
            shapeButton.backgroundColor = blueColour
            shapeButton.tintColor = UIColor.white
        case 3:
            houseButton.backgroundColor = blueColour
            houseButton.tintColor = UIColor.white
            
        default:
            break
        }
        
    }
    
    func addRefrshController(){
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        templateCollectionView.addSubview(refreshControl)
        templateCollectionView.alwaysBounceVertical = true
        
    }
    
    func refresh(_ control: UIRefreshControl) {
        // Do your job, when done:
        control.endRefreshing()
        loadContent()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension DownloadMoreViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let template =  categories[selectedCategoryIndex].templates[indexPath.row]
        
        let controller = StoryboardId.PreviewArtworkView.instantiateViewController(StoryboardName.DownloadMore) as! PreviewArtworkViewController
        controller.template = template
        UserSession.sharedSession.mainViewController?.pushViewControllerFromMain(controller)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        let downCell = cell as! DownloadCell
        
        let template =  categories[selectedCategoryIndex].templates[indexPath.row]
        
        downCell.updateVariableContent(template)
        
    }
    
}

extension DownloadMoreViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if categories.count > 0 {
            
            return categories[selectedCategoryIndex].templates.count
            
        }else{
            
            return 0
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DownloadCell", for: indexPath) as! DownloadCell
        
        let template =  categories[selectedCategoryIndex].templates[indexPath.row]
        
        cell.configureCell(template){
            
            if template.isSubOnly {
                
                if UserSession.sharedSession.currentUser!.isSubscriptionValid {
                    
                    if template.isSaved() {
                        Helper.showArtBoard(template.savedTemplate()!)
                    }else{
                        self.downloadTemplate(template, indexPath: indexPath)
                    }
                    
                }else{
                    
                    Helper.showSubscriptionView()
                    
                }
                
            }else{
                
                if template.isSaved() {
                    Helper.showArtBoard(template.savedTemplate()!)
                }else{
                    self.downloadTemplate(template, indexPath: indexPath)
                }
                
            }
            
        }
        
        return cell
        
    }
    
    func downloadTemplate(_ template: CCTemplate, indexPath:IndexPath){
        
        let svgpath = template.svgUrl.absoluteString
        if !svgpath.contains("svg"){
            self.showDefaultAlert(nil, message: "SVG File Not Found.")
            return
        }
 
        DispatchQueue.main.async(execute: {
            // update some UI
            SVProgressHUD.show(withStatus: "Downloading...")
        });
        
        
        let savedTemplate = SavedTemplate.addNewSavedTemplate(template)
        
        Downloader.load(savedTemplate, completion: { (success) in
            
            if success {
                
                self.templateCollectionView.reloadItems(at: [indexPath])
                
                DispatchQueue.main.async(execute: {
                    // update some UI
                    SVProgressHUD.dismiss()
                    self.showDefaultAlert(nil, message: "Downloaded.")
                });
                
            }else{
                
                DispatchQueue.main.async(execute: {
                    // update some UI
                    savedTemplate.removeSavedTemplate()
                    SVProgressHUD.dismiss()
                    self.showDefaultAlert(nil, message: "Download Failed.")
                });
                
                
            }
            
        })
        
    }
    
}

//MARK: UICollectionViewDelegateFlowLayout

extension DownloadMoreViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return feedMiniCellSize
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(8,8,8,8)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 8
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 8
        
    }
    
}
