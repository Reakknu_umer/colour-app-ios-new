//
//  ShareViewController.swift
//  Colour Connection
//
//  Created  on 2/21/17.
//  at Eight25Media
//

import UIKit

class ShareViewController: UIViewController {
    
    //MARK: Enums 
    //MARK: Structs 
    //MARK: IBOutlets
    
    @IBOutlet weak var fbShareButton:UIButton!
    @IBOutlet weak var instagramShareButton:UIButton!
    @IBOutlet weak var transparentView:UIView!
    @IBOutlet weak var bottomLayoutConstraint:NSLayoutConstraint!
    
    //MARK: Properties 
    
    
    //MARK: Computed Properties
    //MARK: Initializers 
    
    //MARK: View Life Cycle 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transparentView.isUserInteractionEnabled = true
        transparentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(ShareViewController.handleTap(_:))))
    }
    
    //MARK: Segue
    //MARK: IBAction 
    
    @IBAction func fbShareButton(_ button:UIButton){
        
        hideSharePanel {
            DispatchQueue.main.async(execute: { 
                Helper.shareOnFacebook(UserSession.sharedSession.imageToShare!)
            })
            
        }
        
    }
    
    @IBAction func instagramShareButton(_ button:UIButton){
        
        hideSharePanel {
            
            DispatchQueue.main.async(execute: {
                Helper.shareWithInstagram(UserSession.sharedSession.imageToShare!)
            })
            
        }
        
    }
    
    
    //MARK: - Private Functions 
    
    func handleTap(_ recognizer:UITapGestureRecognizer){
        
        hideSharePanel {
            print("Cancel")
        }
        
    }
    
    func showTransparentView(_ window:UIWindow){
        
        self.view.backgroundColor = UIColor.clear
        self.transparentView.backgroundColor = UIColor.black
        self.transparentView.alpha = 0.0
        
        bottomLayoutConstraint.constant = Helper.isIPad() ? -220 : -120
        
        self.view.frame = window.bounds
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        window.addSubview(self.view)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.transparentView.alpha = 0.4
            }, completion: {(finished) in
                self.showSharePanel()
        })
        
    }
    
    func hideTransparentView(_ complation:@escaping (()->())){
        
        UIView.animate(withDuration: 0.3, animations: { 
            
            self.transparentView.alpha = 0
            
        }, completion: { (finished) in
            
            self.view.removeFromSuperview()
            complation()
            
        }) 
        
    }
    
    func showSharePanel(){
        
        
        bottomLayoutConstraint.constant = 0
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) in
            
            
            
        }) 
        
        
    }
    
    func hideSharePanel(_ complation:@escaping (()->())){
        
        bottomLayoutConstraint.constant = Helper.isIPad() ? -220 : -120
        self.view.setNeedsLayout()
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) in
            
            self.hideTransparentView(complation)
            
        }) 
        
    }
    
    //MARK: Helpers 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
