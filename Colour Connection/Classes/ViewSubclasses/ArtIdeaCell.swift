//
//  ArtIdeaCell.swift
//  Colour Connection
//
//  Created  on 2/22/17.
//  at Eight25Media
//

import UIKit

class ArtIdeaCell: UICollectionViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var mainImageView:UIImageView!
    
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainImageView.showBorder()
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ post:CCPost){
        
        mainImageView.sd_setImage(with: post.postThumbImage as URL!, placeholderImage: nil)
        
    }
    
    //MARK: Helpers
    
}
