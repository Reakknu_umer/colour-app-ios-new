//
//  NotificationCell.swift
//  Colour Connection
//
//  Created  on 2/2/17.
//  at Eight25Media
//

import UIKit

//MARK: Enums

enum NotificationCellType:String {
    case Default = "default"
    case Like = "like"
    case Comment = "comment"
}

class NotificationCell: UITableViewCell {
    
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var iconImageView:UIImageView!
    @IBOutlet weak var notificationCountLabel:UILabel!
    @IBOutlet weak var notificationView:UIView!
    
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ type:NotificationCellType, notificationCount:String){
        
        switch type {
            
        case .Like:
            iconImageView.image = NotificationIcon.Like
            nameLabel.text = "Likes"
            notificationView.isHidden = false
            notificationCountLabel.text = notificationCount
            break
        case .Comment:
            iconImageView.image = NotificationIcon.Comment
            nameLabel.text = "Comments"
            notificationView.isHidden = false
            notificationCountLabel.text = notificationCount
            break
        default:
            iconImageView.image = NotificationIcon.Default
            nameLabel.text = "Notifications"
            notificationView.isHidden = false
            notificationCountLabel.text = notificationCount
        }
        
        notificationView.isHidden = (notificationCount == "0")
        
    }
    
    //MARK: Helpers
    
}
