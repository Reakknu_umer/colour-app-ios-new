//
//  FeedDefaultCell.swift
//  Colour Connection
//
//  Created  on 1/28/17.
//  at Eight25Media
//

import UIKit

enum FeedDefaultCellAction {
    case like
    case comment
    case share
    case favourite
}

class FeedDefaultCell: UITableViewCell {
    
    //MARK: Typealias
    typealias ButtonAction = (FeedDefaultCellAction)->()
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var mainImageView:PostImageView!
    @IBOutlet weak var likeButton:UIButton!
    @IBOutlet weak var likeCountLabel:UILabel!
    @IBOutlet weak var commentButton:UIButton!
    @IBOutlet weak var comentCountLabel:UILabel!
    @IBOutlet weak var shareButton:UIButton!
    @IBOutlet weak var favouriteButton:UIButton!
    
    @IBOutlet weak var postNameLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var infoLabel:UILabel!
    
    //MARK: Properties
    
    var userId:Int?
    var buttonAction:ButtonAction?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.infoLabel.isUserInteractionEnabled = true
        self.infoLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FeedDefaultCell.handleTap(_:))))
        
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func likeButtonAction(_ sender: AnyObject) {
        buttonAction?(.like)
    }
    
    @IBAction func commentButtonAction(_ sender: AnyObject) {
        buttonAction?(.comment)
    }
    
    @IBAction func shareButtonAction(_ sender: AnyObject) {
        buttonAction?(.share)
    }
    
    @IBAction func favouriteButtonAction(_ sender: AnyObject) {
        buttonAction?(.favourite)
    }
    
    //MARK: - Private Functions
    
    func configureCell(_ post:CCPost, action: @escaping ButtonAction){
        buttonAction = action
        mainImageView.currentPost = post
        
        userId = nil
        
        mainImageView.sd_setImage(with: post.postThumbImage as URL!, placeholderImage: nil)
        
        postNameLabel.text = post.postName
        dateLabel.text = post.createdDate
        setInfoLabelText(post)
        
    }
    
    func updateVariableContent(_ post:CCPost){
        
        likeCountLabel.text = String(post.likeCount)
        comentCountLabel.text = String(post.commentCount)
        
        let likeImageName = post.isLiked ? "like_selected" : "like"
        let favouriteImageName = post.isFavourite ? "favourite_selected" : "favourite"
        
        likeButton.setImage(UIImage(named: likeImageName), for: UIControlState())
        favouriteButton.setImage(UIImage(named: favouriteImageName), for: UIControlState())
        
    }
    
    //MARK: Helpers
    
    func handleTap(_ recog:UITapGestureRecognizer){
        
        //infoLabel
        
        Helper.locked { 
            
            if self.userId != nil {
                
                Helper.showProfile(self.userId!)
                
            }
            
        }

}
    
    func setInfoLabelText(_ post:CCPost){
        
        userId = post.userId
        
        let attributedString = NSMutableAttributedString()
        
        let part1 = NSMutableAttributedString(string: "Coloured by ", attributes: [NSForegroundColorAttributeName:AppColour.DarkGray, NSFontAttributeName : UIFont(name: AppFont.Light, size: Helper.universalFonrSizeFor(11.0))!])
        
        let part2 = NSMutableAttributedString(string: post.userName, attributes: [NSForegroundColorAttributeName:AppColour.Blue, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(14.0))!])
        
        attributedString.append(part1)
        attributedString.append(part2)
        
        infoLabel.attributedText = attributedString
        
    }
    
}

