//
//  DownloadCell.swift
//  Colour Connection
//
//  Created  on 1/28/17.
//  at Eight25Media
//

import UIKit

enum DownloadCellAction {
    case free
    case get
    case subscribe
    case open
}

class DownloadCell: UICollectionViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var mainImageView:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var button:UIButton! //Free / Get / Subscribe / Open
    
    //MARK: Properties
    
    var buttonAction:(()->())?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        button.layer.cornerRadius = Helper.isIPad() ? 20 : 11
        button.layer.borderColor = UIColor(hex: 0x009EE8).cgColor
        button.layer.borderWidth = 1.0
        
        mainImageView.showBorder()
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func buttonAction(_ button: UIButton) {
        self.buttonAction?()
    }
    
    //MARK: - Private Functions

func configureCell(_ template:CCTemplate, action:@escaping (()->())){
        buttonAction = action
        //print(template.imageUrl)
        
        mainImageView.setCCImageWithUrl(template.thumbImageUrl, placeholderImage: nil)
        nameLabel.text = template.templateName
        
        if template.isSubOnly {
            
            if UserSession.sharedSession.currentUser!.isSubscriptionValid {
                
                if template.isSaved() {
                    button.setTitle(" Open ", for: UIControlState())
                }else{
                    button.setTitle(" Get ", for: UIControlState())
                }
                
            }else{
                
                button.setTitle(" Subscribe ", for: UIControlState())
                
            }
            
        }else{
            
            if template.isSaved() {
                button.setTitle(" Open ", for: UIControlState())
            }else{
                button.setTitle(" Free ", for: UIControlState())
            }
            
        }
        
    }
    
    func updateVariableContent(_ template:CCTemplate){
        
        //        if template.isSubOnly {
        //            
        //            if UserSession.sharedSession.isValid {
        //                button.setTitle("Subscribe / Open", forState: .Normal)
        //            }else{
        //                button.setTitle("Get", forState: .Normal)
        //            }
        //            
        //        }else{
        //            
        //            button.setTitle("Free", forState: .Normal)
        //            
        //        }

}
    
    //MARK: Helpers
    
}
