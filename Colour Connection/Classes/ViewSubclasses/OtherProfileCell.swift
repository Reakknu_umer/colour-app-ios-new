//
//  OtherProfileCell.swift
//  Colour Connection
//
//  Created  on 2/2/17.
//  at Eight25Media
//

import UIKit

class OtherProfileCell: UICollectionViewCell {
    
    //MARK: Typealias
    
    typealias ButtonAction = (ProfileUserAction)->()
    
    //MARK: Enums

//MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var profilePictureImageView:UIImageView!
    @IBOutlet weak var usernameLabel:UILabel!
    
    @IBOutlet weak var followingCountLabel:UILabel!
    @IBOutlet weak var followersCountLabel:UILabel!
    @IBOutlet weak var favouriteImageView:UIImageView!
    
    @IBOutlet weak var favouriteLabel:UILabel!
    
    //MARK: Properties
    
    var buttonAction:ButtonAction?
    var profile:CCProfile?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePictureImageView.layer.cornerRadius = Helper.isIPad() ? 60 : 33
        self.profilePictureImageView.clipsToBounds = true
        addGestureRecognizers()
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ profile:CCProfile, action:@escaping ButtonAction){
        self.buttonAction = action
        self.profile = profile
        
        profilePictureImageView.sd_setImage(with: profile.userProfilePicture, placeholderImage: UIImage(named: "user_image_placeholder"))
        usernameLabel.text = profile.username
        
        followersCountLabel.text = String(profile.followersCount)
        followingCountLabel.text = String(profile.followingCount)
        
        if !profile.isCurrentUser() {
            
            if profile.isFollowing {
                
                favouriteImageView.image = UIImage(named: "unfollow")
                favouriteLabel.text = "Unfollow"
                
            }else{
                
                favouriteImageView.image = UIImage(named: "follow")
                favouriteLabel.text = "Follow"
                
            }
            
        }else{
            
            favouriteImageView.image = UIImage(named: "star")
            
        }
        
    }
    
    func followingPressed(){
        
        buttonAction?(.following)
        
    }
    
    func followersPressed(){
        
        buttonAction?(.followers)
        
    }
    
    func favouritePressed(){
        
        if !profile!.isCurrentUser() {
            
            if profile!.isFollowing {
                
                favouriteImageView.image = UIImage(named: "unfollow")
                favouriteLabel.text = "Unfollow"
                buttonAction?(.unfollow)
                
            }else{
                
                favouriteImageView.image = UIImage(named: "follow")
                favouriteLabel.text = "Follow"
                buttonAction?(.follow)
                
            }
            
        }else{
            
            buttonAction?(.favourite)
            
        }

}
    
    //MARK: Helpers
    
    func addGestureRecognizers(){
        
        let view1:UIView = self.viewWithTag(100)!
        let view2:UIView = self.viewWithTag(101)!
        
        let view3:UIView = self.viewWithTag(200)!
        let view4:UIView = self.viewWithTag(201)!
        
        let view5:UIView = self.viewWithTag(300)!
        let view6:UIView = self.viewWithTag(301)!
        
        view1.isUserInteractionEnabled = true
        view2.isUserInteractionEnabled = true
        view3.isUserInteractionEnabled = true
        view4.isUserInteractionEnabled = true
        view5.isUserInteractionEnabled = true
        view6.isUserInteractionEnabled = true
        
        view1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.followingPressed)))
        view2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.followingPressed)))
        
        view3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.followersPressed)))
        view4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.followersPressed)))
        
        view5.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.favouritePressed)))
        view6.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OtherProfileCell.favouritePressed)))
        
    }
    
}
