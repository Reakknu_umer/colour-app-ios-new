//
//  CommentCell.swift
//  Colour Connection
//
//  Created  on 2/6/17.
//  at Eight25Media
//

import UIKit

class CommentCell: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var profilePictureImageView:UIImageView!
    @IBOutlet weak var usernameLabel:UILabel!
    @IBOutlet weak var dateLabel:UILabel!
    @IBOutlet weak var commentLabel:UILabel!
    
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ comment:CCComment){
        
        profilePictureImageView.sd_setImage(with: comment.userProfilePicture, placeholderImage: UIImage(named: "user_image_placeholder"))
        usernameLabel.text = comment.userName
        dateLabel.text = comment.createdTime
        commentLabel.text = comment.comment
        
    }
    
    //MARK: Helpers
    
}

/*
 
 var commentId:Int!
 var comment = ""
 var userId:Int!
 var userName = ""
 var userProfilePicture:NSURL!
 var createdTime:NSDate!
 
 */
