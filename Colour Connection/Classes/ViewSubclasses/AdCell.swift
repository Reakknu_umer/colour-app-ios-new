//
//  AdCell.swift
//  Colour Connection
//
//  Created  on 6/20/17.
//  at Eight25Media
//

import UIKit
import GoogleMobileAds

class AdCell: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    var bannerView:GADBannerView?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //AdCell
        
        bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        bannerView?.adUnitID = Config.googleAdUnitId
        
        addSubview(bannerView!)
    }
    
    override func prepareForReuse() {
        if bannerView != nil {
            bannerView?.rootViewController = nil
        }
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configure(_ viewController:UIViewController){
        
        if self.bannerView?.rootViewController == nil {
            self.bannerView?.rootViewController = viewController
            self.bannerView?.load(GADRequest())
        }
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
