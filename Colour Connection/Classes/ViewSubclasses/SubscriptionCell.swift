//
//  SubscriptionCell.swift
//  Colour Connection
//
//  Created  on 1/28/17.
//  at Eight25Media
//

import UIKit

class SubscriptionCell: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var subscriptionNameLabel:UILabel!
    @IBOutlet weak var subscriptioButton:UIButton!
    
    //MARK: Properties
    
    var buttonAction:CCButtonAction?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        subscriptioButton.layer.cornerRadius = 11.0
        subscriptioButton.layer.borderColor = UIColor(hex: 0x009EE8).cgColor
        subscriptioButton.layer.borderWidth = 1.0
        
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func subscriptionButtonAction(_ sender: AnyObject) {
        
        self.buttonAction?()
        
    }
    
    //MARK: - Private Functions
    
    func configureCell(_ name:String, price:String, action: @escaping CCButtonAction){
        self.buttonAction = action
        
        subscriptionNameLabel.text = name
        subscriptioButton.setTitle(price, for: UIControlState())
        
    }
    
    //MARK: Helpers
    
}
