//
//  TestTemplateCell.swift
//  Colour Connection
//
//  Created  on 2/22/17.
//  at Eight25Media
//

import UIKit

class TestTemplateCell: UICollectionViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var mainImageView:UIImageView!
    
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainImageView.showBorder()
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ template:CCTemplate){
        mainImageView.sd_setImage(with: template.imageUrl as URL!, placeholderImage: nil)
    }
    
    //MARK: Helpers
    
}
