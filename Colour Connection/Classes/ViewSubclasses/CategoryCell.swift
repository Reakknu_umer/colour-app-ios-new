//
//  CategoryCell.swift
//  Colour Connection
//
//  Created  on 2/13/17.
//  at Eight25Media
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var categoryImageView:UIImageView!
    
    //MARK: Properties
    
    var iconMap = ["Flora" : "floral", "Crystal" : "shapes", "Human" : "people", "Star" : "house"]
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ category: CCCategory){
        
        if let imageName = iconMap[category.categoryName] {
            
            categoryImageView.image = UIImage(named: imageName)
            
        }else{
            categoryImageView.image = nil
        }
        
    }
    
    //MARK: Helpers
    
}
