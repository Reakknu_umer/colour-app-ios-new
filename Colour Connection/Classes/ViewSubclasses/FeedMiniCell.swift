//
//  FeedMiniCell.swift
//  Colour Connection
//
//  Created  on 1/28/17.
//  at Eight25Media
//

import UIKit

enum FeedMiniCellAction {
    case like
    case comment
}

class FeedMiniCell: UICollectionViewCell {
    
    //MARK: Typealias
    typealias ButtonAction = ((FeedMiniCellAction)->())
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var mainImageView:PostImageView!
    @IBOutlet weak var likeButton:UIButton!
    @IBOutlet weak var likeCountLabel:UILabel!
    @IBOutlet weak var commentButton:UIButton!
    @IBOutlet weak var comentCountLabel:UILabel!
    
    //MARK: Properties
    
    var buttonAction:ButtonAction?
    
    var isTappable = true {
        
        didSet{
            
            likeButton.isEnabled = isTappable
            
        }
        
    }
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainImageView.showBorder()
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func lineButtonAction(_ sender: AnyObject) {
        buttonAction?(.like)
    }
    
    @IBAction func commentButtonAction(_ sender: AnyObject) {
        buttonAction?(.comment)
    }
    
    //MARK: - Private Functions
    
    func configureCell(_ post:CCPost, action: @escaping ButtonAction){
        self.buttonAction = action
        self.mainImageView.currentPost = post
        
        mainImageView.sd_setImage(with: post.postThumbImage as URL!, placeholderImage: nil)
        
    }
    
    func updateVariableContent(_ post:CCPost){
        
        likeCountLabel.text = String(post.likeCount)
        comentCountLabel.text = String(post.commentCount)
        
        let likeImageName = post.isLiked ? "like_selected" : "like"
        likeButton.setImage(UIImage(named: likeImageName), for: UIControlState())
        
    }
    
    //MARK: Helpers
    
}
