//
//  NotificationInfoCellTypeB.swift
//  Colour Connection
//
//  Created  on 2/12/17.
//  at Eight25Media
//

import UIKit

class NotificationInfoCellTypeB: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var profilePictureImageView:ProfileImageView!
    @IBOutlet weak var infoLabel:UILabel!
    
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePictureImageView.layer.cornerRadius = Helper.isIPad() ? 33 : 22
        self.profilePictureImageView.clipsToBounds = true
        
        // Initialization code
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ notification:CCNotification){
        
        profilePictureImageView.currentUserId = notification.userId
        profilePictureImageView.sd_setImage(with: notification.postThumbImage as URL!, placeholderImage: nil)
        setInfoLabelText(notification)
        
    }
    
    func setInfoLabelText(_ notification:CCNotification){
        
        let attributedString = NSMutableAttributedString()
        
        let part1 = NSMutableAttributedString(string: notification.username, attributes: [NSForegroundColorAttributeName:AppColour.Blue, NSFontAttributeName : UIFont(name: AppFont.Medium, size: Helper.universalFonrSizeFor(15.0))!])
        
        let part2 = NSMutableAttributedString(string: " \(notification.notificationText)", attributes: [NSForegroundColorAttributeName:AppColour.DarkGray, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(12.0))!])
        
        attributedString.append(part1)
        attributedString.append(part2)
        
        infoLabel.attributedText = attributedString
        
    }
    
    //MARK: Helpers
}
