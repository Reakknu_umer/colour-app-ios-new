//
//  ProfileCell.swift
//  Colour Connection
//
//  Created  on 2/6/17.
//  at Eight25Media
//

import UIKit

class ProfileCell: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    //MARK: Helpers
    
}
