//
//  ArtCell.swift
//  Colour Connection
//
//  Created  on 1/28/17.
//  at Eight25Media
//

import UIKit

class ArtCell: UICollectionViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var mainImageView:UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    
    //MARK: Properties
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainImageView.showBorder()
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ savedTemplate:SavedTemplate){
        
        self.mainImageView.image = nil
        
        let image = UIImage(contentsOfFile: savedTemplate.filePath)
        self.mainImageView.setCCImage(image)
        
        nameLabel.text = savedTemplate.templateName
        
    }
    
    //MARK: Helpers
    
}
