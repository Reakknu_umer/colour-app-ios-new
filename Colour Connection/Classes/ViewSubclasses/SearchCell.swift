//
//  SearchCell.swift
//  Colour Connection
//
//  Created  on 2/1/17.
//  at Eight25Media
//

import UIKit

class SearchCell: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var profilePictureImageView:ProfileImageView!
    @IBOutlet weak var usernameLabel:UILabel!
    @IBOutlet weak var followUnfollowButton:UIButton!
    
    //MARK: Properties
    
    var buttonAction:((Bool)->())?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        profilePictureImageView.layer.cornerRadius = Helper.isIPad() ? 33 : 22
        profilePictureImageView.clipsToBounds = true
        
        // Initialization code
        
        updateFollowUnfollowButtonTheme(false)
        
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    
    @IBAction func followUnfollowButtonAction(_ button: UIButton) {
        
        let isLiked = (button.tag == 1)
        buttonAction?(isLiked)
        
    }
    
    //MARK: - Private Functions
    
    func configureCell(_ user:CCUser, buttonAction:@escaping ((Bool)->())){
        self.buttonAction = buttonAction
        self.profilePictureImageView.currentUserId = user.userId
        
        usernameLabel.text = user.username
        profilePictureImageView.sd_setImage(with: user.userProfilePicture, placeholderImage: UIImage(named: "user_image_placeholder"))
        
    }
    
    //MARK: Helpers
    
    func updateVariableContent(_ user:CCUser){
        
        updateFollowUnfollowButtonTheme(user.isFollowing)
        
    }
    
    func updateFollowUnfollowButtonTheme(_ isFollowing:Bool){
        
        if isFollowing {
            self.followUnfollowButton.tag = 1
            self.followUnfollowButton.setTitle("Unfollow", for: UIControlState())
            self.followUnfollowButton.setTitleColor(UIColor.white, for: UIControlState())
            self.followUnfollowButton.backgroundColor = UIColor(hex: 0x009EE8)
            self.followUnfollowButton.layer.borderColor = UIColor.clear.cgColor
            self.followUnfollowButton.layer.borderWidth = 0
        }else{
            self.followUnfollowButton.tag = 0
            self.followUnfollowButton.setTitle("Follow", for: UIControlState())
            self.followUnfollowButton.setTitleColor(UIColor(hex: 0x009EE8), for: UIControlState())
            self.followUnfollowButton.backgroundColor = UIColor.white
            self.followUnfollowButton.layer.borderColor = UIColor(hex: 0x009EE8).cgColor
            self.followUnfollowButton.layer.borderWidth = 1.0
        }
        
    }
    
}
