//
//  NotificationInfoCellTypeA.swift
//  Colour Connection
//
//  Created  on 2/12/17.
//  at Eight25Media
//

import UIKit

class NotificationInfoCellTypeA: UITableViewCell {
    
    //MARK: Enums
    //MARK: Structs
    //MARK: IBOutlets
    
    @IBOutlet weak var profilePictureImageView:PostImageView!
    @IBOutlet weak var usernameLabel:UILabel!
    @IBOutlet weak var infoLabel:UILabel!
    
    //MARK: Properties
    
    var notification:CCNotification?
    var topLabelGesture: UITapGestureRecognizer?
    var bottomLabelGesture: UITapGestureRecognizer?
    var usernameTapAction:((Bool)->())?
    
    //MARK: Computed Properties
    //MARK: Initializers
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePictureImageView.layer.cornerRadius = Helper.isIPad() ? 33 : 22
        self.profilePictureImageView.clipsToBounds = true
        
        topLabelGesture = UITapGestureRecognizer(target: self, action: #selector(NotificationInfoCellTypeA.handleTopLabelTap(_:)))
        bottomLabelGesture = UITapGestureRecognizer(target: self, action: #selector(NotificationInfoCellTypeA.handleBottomLabelTap(_:)))
        
        usernameLabel.addGestureRecognizer(topLabelGesture!)
        infoLabel.addGestureRecognizer(bottomLabelGesture!)
        
        // Initialization code
    }
    
    //MARK: View Life Cycle
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: Segue
    //MARK: IBAction
    //MARK: - Private Functions
    
    func configureCell(_ notification:CCNotification, type: NotificationType, action: @escaping ((Bool)->())){
        
        profilePictureImageView.isUserInteractionEnabled = false
        
        self.notification = notification
        self.usernameTapAction = action
        
        if type == .comment {
            infoLabel.isUserInteractionEnabled = false
            usernameLabel.isUserInteractionEnabled = true
            profilePictureImageView.sd_setImage(with: notification.postThumbImage as URL!, placeholderImage: nil)
            infoLabel.text = notification.notificationText
            setInfoLabelText(notification, type:  type)
        }else{
            infoLabel.isUserInteractionEnabled = true
            usernameLabel.isUserInteractionEnabled = false
            profilePictureImageView.sd_setImage(with: notification.postThumbImage as URL!, placeholderImage: nil)
            usernameLabel.text = notification.postName
            setInfoLabelText(notification, type:  type)
        }
        
    }
    
    func setInfoLabelText(_ notification:CCNotification,  type: NotificationType){
        
        let attributedString = NSMutableAttributedString()
        
        let part1 = NSMutableAttributedString(string: notification.username, attributes: [NSForegroundColorAttributeName:AppColour.Blue, NSFontAttributeName : UIFont(name: AppFont.Medium, size: Helper.universalFonrSizeFor(15.0))!])

let part2 = NSMutableAttributedString(string: (type == .comment) ? " commented" : " \(notification.notificationText)", attributes: [NSForegroundColorAttributeName:AppColour.DarkGray, NSFontAttributeName : UIFont(name: AppFont.Regular, size: Helper.universalFonrSizeFor(12.0))!])
        
        attributedString.append(part1)
        attributedString.append(part2)
        
        if type == .comment {
            usernameLabel.attributedText = attributedString
        }else{
            infoLabel.attributedText = attributedString
        }
        
    }
    
    func handleTopLabelTap(_ gesture:UITapGestureRecognizer){
        
        let touchX = gesture.location(in: usernameLabel).x
        let maxX = getWidthForUsername(usernameLabel)
        
        if touchX < maxX {
            self.usernameTapAction?(true)
        }else{
            self.usernameTapAction?(false)
        }
        
    }
    
    func handleBottomLabelTap(_ gesture:UITapGestureRecognizer){
        
        let touchX = gesture.location(in: infoLabel).x
        let maxX = getWidthForUsername(infoLabel)
        
        if touchX < maxX {
            self.usernameTapAction?(true)
        }else{
            self.usernameTapAction?(false)
        }
        
    }
    
    func getWidthForUsername(_ label:UILabel)->CGFloat{
        
        let placeholderLabel = UILabel(frame: label.bounds)
        placeholderLabel.font = label.font
        placeholderLabel.text = notification!.username
        return placeholderLabel.sizeThatFits(CGSize(width: Double.infinity, height: Double.infinity)).width
        
    }
    
    //MARK: Helpers
    
}
