//
//  LayerDrawingView.swift
//  OnixVectorDrawing
//
//  Created by Alexei on 11.05.16.
//  Copyright © 2016 Onix. All rights reserved.
//

import UIKit

class LayerDrawingView: UIView, UIScrollViewDelegate {
    
    var arrColorPoints = [CGPoint]()
    var drawingLayer: CALayer? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    private var fillColor = UIColor.red
    
    func setFillColor(color: UIColor) {
        fillColor = color
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    private func setup() {
        
        self.backgroundColor = UIColor.white
        self.clearsContextBeforeDrawing = true
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinchGesture))
        self.addGestureRecognizer(pinch)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(panGesture))
        pan.minimumNumberOfTouches = 1
        self.addGestureRecognizer(pan)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
        tap.numberOfTouchesRequired = 1
        self.addGestureRecognizer(tap)
        
    }

    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        if let layer = drawingLayer, let context = UIGraphicsGetCurrentContext() {
            
            context.scaleBy(x: scaleForCTM, y: scaleForCTM)
            context.translateBy(x: translateForCTM.x, y: translateForCTM.y)
            layer.render(in: context)
        }
        
    }
    
    var lastPinchScale: CGFloat = 1.0
    private var scaleForCTM: CGFloat = 1.0
    
    private var lastPanTranslate = CGPoint.zero
    private var translateForCTM = CGPoint.zero
    
    func pinchGesture(sender: UIPinchGestureRecognizer) {
        if (sender.state == .began) {
            lastPinchScale = 1.0
        }
        
        // Scale
        let scaleDiff = sender.scale - lastPinchScale
        scaleForCTM += (scaleDiff * scaleForCTM)
        lastPinchScale = sender.scale
        
        self.setNeedsDisplay()
    }
    
    func panGesture(sender: UIPanGestureRecognizer) {
        if (sender.state == .began) {
            lastPanTranslate = CGPoint.zero
        }
        
        let translation = sender.translation(in: self)
                
        let translateDiff = CGPoint.init(x: translation.x - lastPanTranslate.x, y: translation.y - lastPanTranslate.y)
        translateForCTM = CGPoint.init(x: translateForCTM.x + (translateDiff.x / scaleForCTM), y: translateForCTM.y + (translateDiff.y / scaleForCTM))
        lastPanTranslate = translation
        
        self.setNeedsDisplay()
    }
    

    func tapGesture(sender: UITapGestureRecognizer) {
        
        let point = sender.location(in: self)
        let scaleTransform = CGAffineTransform(scaleX: 1 / scaleForCTM, y: 1 / scaleForCTM)
        let scaledPoint = point.applying(scaleTransform)
        let translateTransform = CGAffineTransform(translationX: -translateForCTM.x, y: -translateForCTM.y)
        let translatedPoint = scaledPoint.applying(translateTransform)
        
        if let layer = self.drawingLayer?.hitTest(translatedPoint) as? CAShapeLayer {
            layer.fillColor = fillColor.cgColor
            arrColorPoints.append(translatedPoint)
        }
        
        self.setNeedsDisplay()
    }
    
    func undoAction(){
        guard arrColorPoints.count > 0 else {
            return
        }
        if let layer = self.drawingLayer?.hitTest(arrColorPoints.last!) as? CAShapeLayer {
            layer.fillColor = UIColor.white.cgColor
            arrColorPoints.removeLast()
        }
        self.setNeedsDisplay()
    }
}
